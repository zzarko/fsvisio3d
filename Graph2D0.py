#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTab import *
from FileData import *
from FSVisioGraph import *

import numpy as np
from math import sqrt, pi
import tables as tb
import yaml
import re

def frange(start, stop, step):
     i = start
     while i < stop:
         yield i
         i += step

class Graph2D0(FSVisioTab):
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(Graph2D0, self).__init__(title, description, filePrefix, parent)

        self.graph = Graph(self.setStatusBarXY)
        self.contentLayout.addWidget(self.graph)
        self.markers = []

        self.offset1range = None
        self.offset2range = None

        # load data
        cfgdir = "./barbero_creep-iterations/"
        self.filelist = glob.glob( os.path.join(cfgdir, '*.hdf5') )
        self.filelist.sort()
        self.files = []
        for f in self.filelist:
            fname = os.path.basename(f)
            print "Opening file:",fname
            self.files.append(tb.open_file(f))
        self.shortfile = [ re.search(r'(barbero-local-to-global_iteration-[0-9][0-9]-)(.*)(\.hdf5)', os.path.basename(name)).group(2) for name in self.filelist ]
        self.desc = yaml.load(self.files[0].root.parameter_sweep.modal_composites.attrs.column_descriptions_as_yaml)
        self.units = yaml.load(self.files[0].root.parameter_sweep.modal_composites.attrs.column_units_as_yaml)
        self.hdfvars = self.units.keys()
#        for d in self.desc:
#            print d,':',self.desc[d]

#        a : strip length
#        sigma_cr_approx : critical buckling stress approximated from natural frequency
#        sigma_cr : critical buckling stress
#        t_b : base strip thickness
#        omega : natural frequency
#        omega_approx : natural frequency approximated from critical buckling stress
#        sigma_cr_rel_err : critical buckling stress relative approximation error
#        omega_rel_err : natural frequency relative approximation error
#        m_dominant : dominant mode, modal composite via sigma_cr
        self.thickvalues = [ row['t_b'] for row in self.files[0].root.parameter_sweep.modal_composites.where('(a>149.9)&(a<150.1)') ]
        self.lenvalues = [ row['a'] for row in self.files[0].root.parameter_sweep.modal_composites.where('(t_b>1.99)&(t_b<2.01)') ]
        self.varnames = ['sigma_cr','sigma_cr_approx','omega','omega_approx','m_dominant']
        self.addVar('damage','damage','')
        self.addVar('effstress','effective stress','MPa')
#    >>> passvalues = [ row['col3'] for row in
#    ...                table.where('(col1 > 0) & (col2 <= 20)', step=5)
#    ...                if your_function(row['col2']) ]
#    >>> print("Values that pass the cuts:", passvalues)

        self.addRadioControl("whichone", "Graph for", self.varnames, colrows=2, byColumns=False,callback=self.gTitle1)
        self.addLabelEdit("gtitle1","Graph title")
        self.addLabelSlider("thickness", "Thickness", sync=True, pageStep = 10, singleStep = 1, callback = self.thicknessChanged)

        self.addLabelDoubleSpinBox("scale1","Scaling", 0.1, 100, 0.01, 2, callback = self.scale1Changed)
        self.addLabelDoubleSpinBox("offset1","Offset", -99999, 99999, callback = self.offset1Changed)
        self.addButton("reset1","Reset offset/scale", callback = self.resetOS1)

        self.addSeparator()
        self.addLabelCheckList("fileselect", "Select iterations", self.shortfile)

        self.nameControlsTab('Graph 1')

        self.setDefaultParent(self.addControlsTab('Graph 2'))
        self.addCheckBox('graph2','Show graph 2',False)
        self.addCheckBox("twiny","On separate axis",True)
        self.addRadioControl("whichone2", "Graph for", self.varnames, colrows=2, byColumns=False,callback=self.gTitle2)
        self.addLabelEdit("gtitle2","Graph title")
        self.addLabelSlider("thickness2", "Thickness", sync=True, pageStep = 10, singleStep = 1, callback = self.thickness2Changed)

        self.addLabelDoubleSpinBox("scale2","Scaling", 0.1, 100, 0.01, 2, callback = self.scale2Changed)
        self.addLabelDoubleSpinBox("offset2","Offset", -99999, 99999, callback = self.offset2Changed)
        self.addButton("reset2","Reset offset/scale", callback = self.resetOS2)

        self.addSeparator()
        self.addLabelCheckList("fileselect2", "Select iterations", self.shortfile)

        self.setDefaultParent(self.addControlsTab('GraphControls'))
        self.addCheckBox('autoupdate','Auto update graph',True)
        self.addCheckBox('showlegend','Show legend',True)
        self.addLabelEdit("exportname","Export name", callback=self.dummyCallback)
        self.addButton("export","Export graph",self.exportGraph)
        self.addLabelEdit("suptitle","Suptitle")
        
        self.addSeparator()
        self.addGraphControls(callback=self.clearMarkers)
        #zasivljenje:
        self.graphSmoothLayout.checkbox.setEnabled(False)
        self.graphColorLinLagVkLayout.checkbox.setVisible(False)
        self.graphSmoothLayout.checkbox.setEnabled(False)
#        self.graphMarkerLayout.setEnabled(False)

        self.addSeparator()
        markgrid = QGridLayout()
        self.defaultParent.addLayout(markgrid)
        #plotSymbols1 = ['^','o','s','v','D','<','d','>','h','p','H']
        #plotSymbols2 = ['+','x','1','2','3','4',]
        self.marknames = ["O","*",u"⎕",u"◇",u"△",u"▽"]
        self.markdata  = ["o","*","s","D","^","v","<","d",">","h","p","H"]
        markgrid.addLayout(self.addLabel("markerslab","Markers",parent=False),0,0)
        markgrid.addLayout(self.addCheckBox("marksecond","Mark for second graph",parent=False,callback=self.dummyCallback),0,1,1,2)
        markgrid.addLayout(self.addLabelEdit("markname","Name",parent=False),1,0,1,2)
        markgrid.addLayout(self.addLabelDoubleSpinBox("markat","at",self.lenvalues[0],self.lenvalues[-1],0.5,callback=self.dummyCallback,parent=False),1,2)
        markgrid.addLayout(self.addLabelComboBox("marksym","",self.marknames,callback=self.dummyCallback,parent=False),2,0)
        self.marksymLayout.combo.setFixedWidth(50)
        markgrid.addLayout(self.addButton("addmark","Add marker", callback = self.addMarker,parent=False),2,1)
        markgrid.addLayout(self.addButton("clearmark","Clear markers", callback = self.clearMarkers,parent=False),2,2)

        self.scale1 = 1
        self.scale2 = 1

        self.syncTab = False
        self.graphSymbolSize = 10
        self.graphNumberSize = 20
        self.graphTitleSize = 20
        self.graphPenSize = 2
        self.graphLegendScale = 120
        self.graphWidth = 90


#        self.file1 = fdata.files[0]
#        self.file2 = fdata.files[1]
        self.init()

    def addVar(self, name, description, unit):
        self.varnames.append(name)
        self.desc[name] = description
        self.units[name] = unit

    def thickness2Changed(self, value):
        if value == -1: return
        if not hasattr(self, 'thickness2Layout'): return
        thickness = self.thickvalues[value]
        self.thickness2Layout.label.setText(self.thickness2Layout.labelText+": "+str(thickness)+self.units['t_b'])
        self.update()

    def thicknessChanged(self, value):
        if value == -1: return
        if not hasattr(self, 'thicknessLayout'): return
        thickness = self.thickvalues[value]
        self.thicknessLayout.label.setText(self.thicknessLayout.labelText+": "+str(thickness)+self.units['t_b'])
        self.update()

    def gTitle1(self, value):
        if self.units[self.varnames[value]] != "":
            self.gtitle1 = '%s [%s]' % (self.desc[self.varnames[value]],self.units[self.varnames[value]])
        else:
            self.gtitle1 = self.desc[self.varnames[value]]
        self.update()

    def gTitle2(self, value):
        if self.units[self.varnames[value]] != "":
            self.gtitle2 = '%s [%s]' % (self.desc[self.varnames[value]],self.units[self.varnames[value]])
        else:
            self.gtitle2 = self.desc[self.varnames[value]]
        self.update()

    def addMarker(self):
        self.markers.append([self.marksecond,self.markat])
        self.update()
        return
#        if self.marksecond and self.secondg:
#            var = self.var2
#            pfile = fdata.files[self.file2]
#        else:
#            var = self.var1
#            pfile = fdata.files[self.file1]
#        nterm = self.nterm1 - 1
#        try:
#            index = pfile.length.index(self.markat)
#        except:
#            return
#        if var < 8:
#            y = pfile.data[index][nterm].selfvalue[var]
#        else:
#            y = pfile.composite[var-8][index]
#        if abs(y) >= 0.01:
#            name = "%0.1f mm: %0.2f %s" % (self.markat,y,self.varUnits[var])
#        else:
#            name = "%0.1f mm: %0.2E %s" % (self.markat,y,self.varUnits[var])
#        if self.markname != "": name = self.markname + ": " + name
#        self.markers.append([self.markat, y, name, self.markdata[self.marksym], self.marksecond])
#        self.update()

    def offset1Changed(self, value):
        if value == -1: return
        if (value != 0):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.scale1 == 1.0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

    def offset2Changed(self, value):
        if (value == -1) or (self.graph.canvas.axes2 == None): return
        if (value != 0):
            if self.offset2range == None:
                self.offset2range = self.graph.canvas.axes2.get_ylim()
        elif self.scale2 == 1.0:
            self.offset2range = None
        #print "OFFSET2",self.offset2,self.offset2range
        self.update()

    def scale1Changed(self, value):
        if value == -1: return
        if (value != 1):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.offset1 == 0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

    def scale2Changed(self, value):
        if (value == -1) or (self.graph.canvas.axes2 == None): return
        if (value != 1):
            if self.offset2range == None:
                self.offset2range = self.graph.canvas.axes2.get_ylim()
        elif self.offset2 == 0:
            self.offset2range = None
        #print "OFFSET2",self.offset2,self.offset2range
        self.update()

    def resetOS1(self):
        self.updateOff()
        self.offset1 = 0.0
        self.scale1 = 1.0
        self.updateOn()

    def resetOS2(self):
        self.updateOff()
        self.offset2 = 0.0
        self.scale2 = 1.0
        self.updateOn()

    def init(self):
        self.thicknessSetMinMax(0,len(self.thickvalues)-1)
        self.thickness2SetMinMax(0,len(self.thickvalues)-1)
        self.thickness = self.thickvalues.index(6.30)+1
        self.thickness2 = self.thickness
        self.thicknessChanged(self.thickness)
        self.whichone = 0
        self.whichone2 = 0
        return

        self.lengthn1SetMinMax(0,self.file1.lengths-1)
        self.lengthn1 = 0
        self.lengthn1Changed(0)
        self.lengthn2SetMinMax(0,self.file1.lengths-1)
        self.lengthn2 = self.file1.lengths-1
        self.lengthn2Changed(0)
        self.phi = 2
        self.omegaq = 1
        w = self.file1.composite[1][self.lengthn1]  # freq elasticno
        wd = self.file2.composite[1][self.lengthn1] # freq visko
        
        wmin = min(self.file1.composite[1])
        wmax = max(self.file1.composite[1])*100
        #self.wminSetMinMaxStepDec(wmin,wmax,0.1,2)
        #self.wmaxSetMinMaxStepDec(wmin,wmax,0.1,2)
        self.wminSetMinMaxStepDec(1,wmax,0.1,2)
        self.wmaxSetMinMaxStepDec(1,wmax,0.1,2)
        self.wmin = wmin
        self.wmax = wmax

    def exportGraph(self):
        if self.exportnameLayout.edit.text() != "":
            title = self.exportnameLayout.edit.text()
        else:
            title = '%s_%s[%f]' % (self.filePrefix, self.varnames[self.whichone], self.thickvalues[self.thickness])
        if self.offset1 != 0:
            title += " (offset %d)" % self.offset1
        self.graph.export("./export/",title)

    def clearMarkers(self):
        self.markers = []
#        markers2 = [x for x in self.markers if x[0] != self.markat]
#        if len(markers2) == len(self.markers):
#            self.markers = []
#        else:
#            self.markers = markers2
        self.update()

    def plot(self):
        if not self.autoupdate: return
        self.setStatusBar("Accessing HDF5 data, please wait...")
        self.graph.titleX = 'length [%s]' % self.units['a']

#        if self.units[self.varnames[self.whichone]] != "":
#            self.graph.titleY = '%s [%s]' % (self.desc[self.varnames[self.whichone]],self.units[self.varnames[self.whichone]])
#        else:
#            self.graph.titleY = self.desc[self.varnames[self.whichone]]
#        if self.units[self.varnames[self.whichone2]] != "":
#            self.graph.titleY2 = '%s [%s]' % (self.desc[self.varnames[self.whichone2]],self.units[self.varnames[self.whichone2]])
#        else:
#            self.graph.titleY2 = self.desc[self.varnames[self.whichone2]]
        self.graph.titleY = self.gtitle1Layout.edit.text()
        self.graph.titleY2 = self.gtitle2Layout.edit.text()


        self.setGraphAttributes(self.graph)
        self.graph.resetGraph()
        self.graph.showLegend = self.showlegend
        self.graph.logY = False

        varname = self.varnames[self.whichone]
        selected = self.fileselectGetCheckStates()
        for f in range(len(self.filelist)):
            if not selected[f]: continue
            x = self.lenvalues
            name = self.shortfile[f]
            if self.graph2: name = "%s-%s" % (varname,name)
            if varname in self.hdfvars:
                condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                #print "INDEXING:",self.files[f].root.parameter_sweep.modal_composites.will_query_use_indexing(condition)
                y = [ row[varname] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ]
                #print "GRAPH:",self.thickness,self.thickvalues[self.thickness]
                if len(y) == 0:
                    print "Something wrong with thickness:",self.thickvalues[self.thickness]
                    return
            elif varname == 'damage':
                condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                w = np.array([ row['omega'] for row in self.files[0].root.parameter_sweep.modal_composites.where(condition) ])    # elastično
                wd = np.array([ row['omega'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
                y = 1-(wd/w)**2
            elif varname == 'effstress':
                condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                w = np.array([ row['omega'] for row in self.files[0].root.parameter_sweep.modal_composites.where(condition) ])    # elastično
                wd = np.array([ row['omega'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
                s = np.array([ row['sigma_cr'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
                y = s*(w/wd)**2
            else:
                return
            color = self.graph.addCurve(x,y,name,symbol=True,showInLegend=True)
            #self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)

            #def addMarker(self, x, y, color='k', name="", symbol='o', secondg=False, size=10):
            for i,marker in enumerate(self.markers):
                if marker[0]: continue
                idx = (np.abs(np.array(x) - marker[1])).argmin()
                mname = "%.1f %s: %.3f %s" % (x[idx],self.units['a'],y[idx],self.units[self.varnames[self.whichone]])
                self.graph.addMarker(x[idx],y[idx],color,mname,self.markdata[i],False,self.graphSymbolSize)

        if self.graph2:
            varname = self.varnames[self.whichone2]
            selected = self.fileselect2GetCheckStates()
            for f in range(len(self.filelist)):
                if not selected[f]: continue
                x = self.lenvalues
                name = self.shortfile[f]
                name = "%s-%s" % (varname,name)
                if varname in self.hdfvars:
                    condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                    #print "INDEXING:",self.files[f].root.parameter_sweep.modal_composites.will_query_use_indexing(condition)
                    y = [ row[varname] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ]
                    #print "GRAPH:",self.thickness,self.thickvalues[self.thickness]
                    if len(y) == 0:
                        print "Something wrong with thickness:",self.thickvalues[self.thickness]
                        return
                elif varname == 'damage':
                    condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                    w = np.array([ row['omega'] for row in self.files[0].root.parameter_sweep.modal_composites.where(condition) ])    # elastično
                    wd = np.array([ row['omega'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
                    y = 1-(wd/w)**2
                elif varname == 'effstress':
                    condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                    w = np.array([ row['omega'] for row in self.files[0].root.parameter_sweep.modal_composites.where(condition) ])    # elastično
                    wd = np.array([ row['omega'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
                    s = np.array([ row['sigma_cr'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
                    y = s*(w/wd)**2
                else:
                    return
                color = self.graph.addCurve(x,y,name,symbol=True,showInLegend=True,secondAxe=self.twiny)
                #self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)

                #def addMarker(self, x, y, color='k', name="", symbol='o', secondg=False, size=10):
                for i,marker in enumerate(self.markers):
                    if not marker[0]: continue
                    idx = (np.abs(np.array(x) - marker[1])).argmin()
                    mname = "%.1f %s: %.2f %s" % (x[idx],self.units['a'],y[idx],self.units[self.varnames[self.whichone2]])
                    self.graph.addMarker(x[idx],y[idx],color,mname,self.markdata[i],True,self.graphSymbolSize)

        #self.graph.canvas.axes.autoscale_view(True,True,True)
        if ((self.offset1 != 0.0) or (self.scale1 != 1.0)) and (self.offset1range != None):
            print "GOFFSET1",self.offset1,self.scale1,self.offset1range
            self.graph.canvas.axes.autoscale_view(True,True,False)
            top = (self.offset1range[1] - self.offset1range[0])*self.scale1 + self.offset1range[0] + self.offset1
            self.graph.canvas.axes.set_ylim(self.offset1range[0] + self.offset1, top)
        else:
            self.graph.canvas.axes.autoscale_view(True,True,True)
        if self.graph.canvas.axes2:
            if ((self.offset2 != 0.0) or (self.scale2 != 1.0)) and (self.offset2range != None):
                print "GOFFSET2",self.offset2,self.scale2,self.offset2range
                self.graph.canvas.axes2.autoscale_view(True,True,False)
                top = (self.offset2range[1] - self.offset2range[0])*self.scale2 + self.offset2range[0] + self.offset2
                self.graph.canvas.axes2.set_ylim(self.offset2range[0] + self.offset2, top)
            else:
                self.graph.canvas.axes2.autoscale_view(True,True,True)

        self.graph.canvas.figure.suptitle(self.suptitleLayout.edit.text(),fontsize=self.graphTitleSize)

        #self.graph.canvas.axes.set_xscale('log')
        #if self.graph2:
        #    self.graph.canvas.axes2.set_ylabel()
        self.graph.plotGraph()

        self.setStatusBar("Graph shown")
        return

