#!/usr/bin/python
# -*- coding: UTF-8 -*-

# paketi
# apt install python-tables python-qt4-dev python-scipy python-numpy python-matplotlib python-yaml

# cetvrtak 112 08:30 - 17:00

#from FileData import *
#fdata.Load("./data/")

import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

#from Graph3D1 import *
from Graph2D0 import *
from Graph2D0SC import *
#from Graph2D2 import *
#from Graph2D3 import *
#from Graph2D4 import *
#from Graph2D5 import *
#from Graph2D6 import *
#from Graph3D2 import *

class FSVisioMain(QMainWindow):
    def __init__(self, parent=None): 
        super(FSVisioMain, self).__init__(parent)

        self.setWindowTitle("Finite Strip Visualization") 
        self.setGeometry(50,50,400,200)

        self.centralWidget = QWidget()
        self.centralWidgetLayout = QVBoxLayout()
        self.centralWidget.setLayout(self.centralWidgetLayout)
        self.setCentralWidget(self.centralWidget)

        #central tab widget
        self.tabWidget = QTabWidget()
        self.centralWidgetLayout.addWidget(self.tabWidget)
        self.connect(self.tabWidget, SIGNAL("currentChanged(int)"), self.tabChanged)
        self.lastTab = -1
        #sakriven tab bar
        #self.tabWidget.tabBar().setVisible(False)

        FSVisioTab.syncing = True
#        tab = Graph3D2("3D Graph 1","3D representation of structure", "3D1")
#        self.addTab(tab)
#        self.pandaTab = Graph3D1("3D graph 2","3D representation of movements and values", "3DG")
#        self.pandaTab.panda.bindToWindow(int(self.winId()))
#        self.addTab(self.pandaTab)

#        tab = Graph3D1("Damage","Damage graph", "DAM")
#        self.addTab(tab)
        tab = Graph2D0("Creep","Creep iterations", "CRP")
        self.addTab(tab)
        tab = Graph2D0SC("Scatter","Scatter for all iterations", "SCT")
        self.addTab(tab)
#        tab = Graph2D1("Creep","Creep iterations", "CRP")
#        self.addTab(tab)
#        tab = Graph2D2("Stress","Stress/frequency graphs", "STR")
#        self.addTab(tab)
#        tab = Graph2D3("EqFreq","Equivalent frequency", "EQF")
#        self.addTab(tab)
#        tab = Graph2D4("Prigus-2D","Koeficijent prigusenja 2D", "KSI2")
#        self.addTab(tab)
#        tab = Graph3D2("Prigus-3D","Koeficijent prigusenja 3D", "KSI3")
#        self.addTab(tab)
#        tab = Graph2D5("Stability","Stability graphs", "STB")
#        self.addTab(tab)
#        tab = Graph2D6("Stability2","Stability graphs 2", "ST2")
#        self.addTab(tab)

#        tab = Graph2D4("2D Graph NTERM/All","2D graph by NTERM, all files", "2NA")
#        self.addTab(tab)
#        tab = Graph2D5("2D Graph Load/All","2D graph by Load, all files", "2LA")
#        self.addTab(tab)

        #options
#        self.options = Options("Options", "FSVisio options")
#        self.addTab(self.options)
        self.setStatusBar(FSVisioTab.statusBar)
        FSVisioTab.syncing = False
        #set index to Panda3D tab
#        self.tabWidget.setCurrentIndex(1)
#        self.tabWidget.setCurrentIndex(0)

    def addTab(self, tab):
        """Adds tab widget to main window"""
        self.tabWidget.addTab(tab,tab.title)
        tab.updateOn()

    def tabChanged(self, tab):
#        print "T,L,P",tab,self.lastTab,self.pandaTab.number
        if self.lastTab != -1:
            FSVisioTab.tabs[self.lastTab].setActive(False)
        FSVisioTab.tabs[tab].setActive(True)

#        if tab == self.pandaTab.number:
#            self.pandaTab.setActive(True)
#            print "true"
##            self.tabs[0].panda3D.timer.start(0)
##            #ovo ispod je da izazove resize, pa samim tim i repaint
##            margins = self.tabs[0].panda3D.getContentsMargins()
##            self.tabs[0].panda3D.setContentsMargins(margins[0]+1, margins[1], margins[2], margins[3])
##            self.tabs[0].panda3D.setContentsMargins(margins[0], margins[1], margins[2], margins[3])
##            pass
#        elif self.lastTab == self.pandaTab.number:
#            self.pandaTab.setActive(False)
#            print "false"
##            wp.setSize(1,1)
##            wp.setOrigin(0,0)
##            base.win.requestProperties(wp)
##            base.graphicsEngine.renderFrame()
##            self.tabs[0].panda3D.timer.stop()
##            pass
        self.lastTab = tab
        FSVisioTab.currentTab = tab
#        FSVisioTab.tabs[tab].update()
        FSVisioTab.statusBar.showMessage(FSVisioTab.tabs[tab].statusMessage)

if __name__ == '__main__':
#    if os.path.exists("./data/dat/big.pickle"):
#        fdata = pickle.load(open("./data/dat/big.pickle"))
#    else:
#        fdata.Load("./data/dat/")
#    fdata.Load("./data/dat/")
#    pickle.dump(fdata, open("./data/dat/big.pickle","w"),pickle.HIGHEST_PROTOCOL)
#    FSVisioTab.iterableGlobal["All files"] = fdata.fileIterator
#    FSVisioTab.iterableGlobal["All files grouped"] = fdata.fileIteratorGrouped
    app = QApplication(sys.argv)
    form = FSVisioMain()
    form.show()
    app.exec_()

