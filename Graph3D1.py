#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTab import *
from FileData import *
from FSVisioGraph3D import *

from math import pi,sin,cos
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import operator

import numpy as np
import tables as tb

import matplotlib.colors as mcolors

# https://stackoverflow.com/questions/16834861/create-own-colormap-using-matplotlib-and-plot-color-scale
def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)

class Graph3D1(FSVisioTab):
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(Graph3D1, self).__init__(title, description, filePrefix, parent)
        self.varNames = ["Min critical force of mode","Min natural frequency of mode",
                         "Min normalized critical stress of mode","Min normalized natural frequency of mode",
                         "Min approximation critical stress of mode","Min natural frequency approximation",
                         "Critical stress relative error of mode","Natural frequency relative error of mode",
                         "Min critical stress of all modes", "Min natural frequency of all modes"]
        self.varUnits = ["MPa","rad/s","MPa","rad/s","MPa","rad/s","","","MPa","rad/s","MPa","rad/s"]
#        self.varNames = Vars.varForcesList + Vars.varCoordsList + Vars.varUserList + Vars.varSpecialList
        self.varDamNames = ["u","v",'w','f']

        self.graph = Graph3D(self.setStatusBarXY)
        self.contentLayout.addWidget(self.graph)

#        self.offset1range = None
#        self.offset2range = None

        self.addLabelComboBox("file1", "File", sync=True, callback = self.file1Changed)
#        self.addLabelComboBox("var1", "Variable", self.varNames, sync=True)
        self.addLabelComboBox("damagefor", "Damage for", self.varDamNames, sync=True)
#        self.addLabelComboBox("damageby", "Damage by", self.varDamNames, sync=True)
        self.addLabelSlider("lengthn", "Length", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthnChanged)
        self.addLabelSlider("nterm1", "NTerm", sync=True, pageStep = 1, singleStep = 1)
        self.addLabelDoubleSpinBox("movescale","Movement scaling", 0.1, 10000, 0.1, 1, sync=True)
        #self.addLabelDoubleSpinBox("scale1","Scaling", 0.1, 100, 0.01, 2, callback = self.scale1Changed)
        self.addLabelDoubleSpinBox("grscale","Graph scale", 0.1, 10, 0.1, 1)
#        self.addLabelDoubleSpinBox("offset1","Offset", -9999, 9999, callback = self.offset1Changed)
#        self.addButton("reset1","Reset offset/scale", callback = self.resetOS1)
        self.addCheckBox("coldamage","Color by damage")
        self.addCheckBox("showgrid","Show axes and grid")
        self.addLabelEdit("suptitle","Suptitle")
        self.addLabelDoubleSpinBox("mandamage","Manual damage", 0, 10000, 0.1, 2, sync=True)
        self.addSeparator()
#        varNames = [Vars.varRichName[i] for i in self.varNames]
#        self.addLabelComboBox("varX", "Variable X", varNames)
#        self.addLabelComboBox("varY", "Variable Y", varNames)
        self.addGraphControls(callback=self.clearMarkers)
        #zasivljenje:
        self.graphSmoothLayout.checkbox.setEnabled(False)
        self.graphColorLinLagVkLayout.checkbox.setVisible(False)
        self.graphSmoothLayout.checkbox.setEnabled(False)
#        self.graphMarkerLayout.setEnabled(False)

#        self.scale1 = 1
        self.scale2 = 1

        self.addButton("export","Export graph",self.exportGraph)
        self.syncTab = True
        self.init()
        self.graphSymbolSize = 12
        self.graphNumberSize = 14
        self.graphTitleSize = 18
        self.colorbar = None
        self.movescale = 1000
        self.figtext = None
        self.showgrid = True

    def init(self):
        self.file1SetValues([i.fileName for i in fdata.files])
        self.grscale = 1.5

    def file1Changed(self, value):
        if value == -1: return
        self.nterm1SetMinMax(1,fdata.files[self.file1].nterm)
        self.lengthnSetMinMax(0,fdata.files[self.file1].lengths-1)
        self.lengthnChanged(0)

    def lengthnChanged(self, value):
        if value == -1: return
        if not hasattr(self, 'lengthnLayout'): return
        self.lengthnLayout.label.setText(self.lengthnLayout.labelText+": "+str(fdata.files[self.file1].length[value]))
        self.update()

#    def offset1Changed(self, value):
#        if value == -1: return
#        if (value != 0):
#            if self.offset1range == None:
#                self.offset1range = self.graph.canvas.axes.get_ylim()
#        elif self.scale1 == 1.0:
#            self.offset1range = None
#        #print "OFFSET1",self.offset1,self.offset1range
#        self.update()

#    def scale1Changed(self, value):
#        if value == -1: return
#        if (value != 1):
#            if self.offset1range == None:
#                self.offset1range = self.graph.canvas.axes.get_ylim()
#        elif self.offset1 == 0:
#            self.offset1range = None
#        #print "OFFSET1",self.offset1,self.offset1range
#        self.update()

#    def resetOS1(self):
#        self.offset1 = 0.0
#        self.scale1 = 1.0

    def exportGraph(self):
        title = self.filePrefix + "_" + self.file1Text + "_Dam-" + self.damageforText + "_Mode-" + str(self.nterm1)
        title = title + "_Len-" + "%.1f" % fdata.files[self.file1].length[self.lengthn] + "m"
        self.graph.export("./export/",title)

    def clearMarkers(self):
        self.graph.markers = []
        self.plot()

    def frange(self, start, stop, n):
        L = [0.0] * n
        nm1 = n - 1
        nm1inv = 1.0 / nm1
        for i in range(n):
            L[i] = nm1inv * (start*(nm1 - i) + stop*i)
        return L

    def plot(self):
        self.graph.azim = self.graph.canvas.axes.azim
        self.graph.elev = self.graph.canvas.axes.elev

        self.graph.clear()
        self.setGraphAttributes(self.graph)
        self.graph.resetGraph()

        #self.graph.titleX = "Length [mm]"
        #self.graph.titleY = "Damage:"
        self.graph.titleX = ""
        self.graph.titleY = ""
        symbols = False
        pfile = fdata.files[self.file1]
        nterm = self.nterm1 - 1

        length = self.lengthn
        a = pfile.length[length]
        movscale = self.movescale
        scale = 1
        load = 0
        forcefreq = 0
        sections = 50
        grscale = self.grscale
        coldamage = self.coldamage

        #izdvoji damages tabelu
        condition = "(a1 == %f) & (ii == %d)" % (a, self.nterm1)
        #print condition
        damages = np.asmatrix(fdata.damages.read_where(condition)[0]['fatigue_damage_matrix'])
        #print "DAMAGES\n",damages
        damagefor = self.damagefor   # 0-u, 1-v, 2-w, 3-f
#        damageby  = self.damageby

        data = pfile.data[length][nterm].coord[forcefreq]
        data0 = pfile.basecoord
        coords = []
        xyzcol = []
        xc = []
        yc = []
        zc = []

#        dam_kk = 24099000000*0.002815
#        dam_mm = 10000*0.002815*0.493
#        dam_k = dam_kk/a*1000
#        dam_m = dam_mm*a/1000
#        dam_wd = pfile.composite[1][length]
#        dam_rd = -dam_k+dam_wd*dam_wd*dam_m
#        dam_rdk = -dam_rd/dam_k

        dam_kk = 48*24099000000*0.000003718
        dam_mm = 0.493*10000*0.002815
        dam_k = dam_kk/a*1000/a*1000/a*1000
        dam_m = dam_mm*a/1000
        dam_wd = pfile.data[length][nterm].selfvalue[3]
#        dam_wd = pfile.composite[1][length]
        dam_rd = -dam_k+dam_wd*dam_wd*dam_m
        dam_rdk = -dam_rd/dam_k

        for lin in range(pfile.lines):
            x1 = data[lin].x*movscale*scale
            y1 = data[lin].y*movscale*scale
            z1 = data[lin].z*movscale*scale
            for y in self.frange(0,a,sections):
                arg = (nterm+1)*pi*y/a
                sarg = sin(arg)
                carg = cos(arg)
                coords.append((x1*sarg+data0[lin].x*scale,y1*carg+y*scale,z1*sarg+data0[lin].z*scale))
                row = lin*4+damagefor
#                damage = 0
#                for l in range(pfile.lines*4):
#                    dam = damages[row,l]
#                    if np.isfinite(dam):
#                        #if dam > 0: damage += dam
#                        damage += abs(dam)
#                #xyzcol.append(abs(x1*sarg)+abs(y1*cos(arg))+abs(z1*sarg))
#                if damagefor == 1:
#                    damage = damage * carg
#                else:
#                    damage = damage * sarg
                if damagefor == 1:
                    damarg = carg
                else:
                    damarg = sarg
#                if y == 0:
#                    damage = 0
#                else:
#                    k = kk/a*1000
#                    m = mm*a/1000
#                    wd = pfile.composite[1][length]
#                    rd = -k*wd*wd*m
#                    damage = -rd/k * damarg
                if self.mandamage == 0:
                    damage = dam_rdk*damarg
                else:
                    damage = self.mandamage*damarg
                xyzcol.append(abs(damage))
                xc.append(coords[-1][0])
                yc.append(coords[-1][1])
                zc.append(coords[-1][2])
        xyzmaxcol = max(xyzcol)
        maxdam = xyzmaxcol
        print "Max damage:",xyzmaxcol
        if xyzmaxcol < 1: xyzmaxcol = 1
        triangles = []
        colors = []
        #colnum = 0
        #colrgb = [(0, 0, 0, 1),(0, 0, 1, 1),(0, 1, 0, 1),(1, 0, 0, 1),(0, 1, 1, 1),(1, 0, 1, 1),(1, 1, 0, 1),(1, 1, 1, 1)]
        for cs in range(sections-1):
            for s in range(pfile.strips):
                l1 = pfile.strip[s].line[0]-1
                l2 = pfile.strip[s].line[1]-1
                p1 = l1*sections+cs
                p2 = l2*sections+cs
                p3 = l1*sections+cs+1
                p4 = l2*sections+cs+1
                if coldamage:
                    pc = tuple(map(operator.add, coords[p1], coords[p2]))
                    pc = tuple(map(operator.add, pc, coords[p3]))
                    pc = tuple(map(operator.add, pc, coords[p4]))
                    pc = tuple(map(operator.div, pc, (4,4,4)))
                    colc = (xyzcol[p1] + xyzcol[p2] + xyzcol[p3] + xyzcol[p4])/4

                    tri1 = [coords[p1],coords[p2],pc]
                    tri2 = [coords[p3],coords[p4],pc]
                    tri3 = [coords[p1],coords[p3],pc]
                    tri4 = [coords[p2],coords[p4],pc]
                    triangles.append(tri1)
                    triangles.append(tri2)
                    triangles.append(tri3)
                    triangles.append(tri4)

                    col = (xyzcol[p1] + xyzcol[p2] + colc)/xyzmaxcol/3
                    #colors.append((col*0.8+0.1,col*0.8+0.1,col*0.8+0.1,1))
                    colors.append((col*0.8+0.1,0.9-col*0.8,0.1,1))
                    col = (xyzcol[p3] + xyzcol[p4] + colc)/xyzmaxcol/3
                    #colors.append((col*0.8+0.1,col*0.8+0.1,col*0.8+0.1,1))
                    colors.append((col*0.8+0.1,0.9-col*0.8,0.1,1))
                    col = (xyzcol[p1] + xyzcol[p3] + colc)/xyzmaxcol/3
                    #colors.append((col*0.8+0.1,col*0.8+0.1,col*0.8+0.1,1))
                    colors.append((col*0.8+0.1,0.9-col*0.8,0.1,1))
                    col = (xyzcol[p2] + xyzcol[p4] + colc)/xyzmaxcol/3
                    #colors.append((col*0.8+0.1,col*0.8+0.1,col*0.8+0.1,1))
                    colors.append((col*0.8+0.1,0.9-col*0.8,0.1,1))
                else:
#                    col = (1.0,1.0,1.0,1.0)
#                    colors.append(col)
#                    colors.append(col)
#                    colors.append(col)
#                    colors.append(col)

                    square = [coords[p1],coords[p2],coords[p4],coords[p3]]
                    triangles.append(square)
                    #col = (xyzcol[p1] + xyzcol[p2] + xyzcol[p3] + xyzcol[p4])/xyzmaxcol/4
                    #colors.append((col,0,1-col,1))
                    #colors.append((col*0.8+0.1,col*0.8+0.1,col*0.8+0.1,1))
                    #colors.append(colrgb[colnum % 8])
                    #colnum += 1

#        # colormap
#        x = np.array(xyzcol)
#        y = np.array(xyzcol)
#        x, y = np.meshgrid(x,y)
#        c = mcolors.ColorConverter().to_rgb
#        cmap = make_colormap([c('green'), c('red')])
#        plot = self.graph.canvas.axes.pcolormesh(y,x,x,cmap=cmap)
#        if self.colorbar:
#             self.graph.canvas.figure.delaxes(self.graph.canvas.figure.axes[-1])
#             self.graph.canvas.figure.subplots_adjust(right=0.90)
##            self.graph.canvas.axes.clear()
##            self.colorbar.ax.clear()
##            cbar = self.graph.canvas.figure.colorbar(plot,cax=self.colorbar.ax)
#        cbar = self.graph.canvas.figure.colorbar(plot,pad=0.2)
#        cbar.ax.set_yticklabels([])
#        cbar.set_label('Max damage: '+str(xyzmaxcol))
#        self.colorbar = cbar

        #collection = Poly3DCollection(triangles, facecolors=['r', 'b', 'g'], edgecolors=['r', 'b', 'g'])
        #collection = Poly3DCollection(triangles,facecolors='w',color=(0,0,0,0), edgecolor='Gray',alpha=1.0)
        #collection = Poly3DCollection(triangles,facecolors=colors,color=colors, edgecolor=(0,0,0,1), linewidth=0.1)
        if coldamage:
            collection = Poly3DCollection(triangles,facecolors=colors,color=colors)
        else:
            collection = Poly3DCollection(triangles,facecolors='white',edgecolors='black')

        #grid je iz nekog razloga iznad grafa, ovo ne radi:
#        self.graph.canvas.axes.set_xlim([min(xc), max(xc)])
#        self.graph.canvas.axes.set_ylim([min(yc), max(yc)])
#        self.graph.canvas.axes.set_zlim([min(zc), max(zc)])
#        self.graph.canvas.axes.set_axisbelow(True)
#        self.graph.canvas.axes.grid(zorder=0)

#        #dodavanje jos jednog seta tacaka (sa nevidljivim markerom) iz nekog razloga radi:
        self.graph.canvas.axes.scatter(xc,yc,zc,marker='')
        self.graph.canvas.axes.set_axisbelow(True)

        minxyz = min(xc+yc+zc)
        maxxyz = max(xc+yc+zc)
#        self.graph.canvas.axes.set_xlim([minxyz, maxxyz])
#        self.graph.canvas.axes.set_ylim([minxyz, maxxyz])
#        self.graph.canvas.axes.set_zlim([minxyz, maxxyz])
        scale_x = (max(xc)-min(xc))/(maxxyz-minxyz)*grscale
        scale_y = (max(yc)-min(yc))/(maxxyz-minxyz)*grscale
        scale_z = (max(zc)-min(zc))/(maxxyz-minxyz)*grscale
        self.graph.canvas.axes.get_proj = lambda: np.dot(Axes3D.get_proj(self.graph.canvas.axes), np.diag([scale_x, scale_y, scale_z, 1]))

        self.graph.canvas.axes.add_collection3d(collection)

#        x = [0, 2, 1, 1]
#        y = [0, 0, 1, 0]
#        z = [0, 0, 0, 1]

#        vertices = [[0, 1, 2], [0, 1, 3], [0, 2, 3], [1, 2, 3]]

#        tupleList = zip(x, y, z)

#        poly3d = [[tupleList[vertices[ix][iy]] for iy in range(len(vertices[0]))] for ix in range(len(vertices))]
#        print "POLY3D:",poly3d
#        
#        #self.graph.canvas.axes.scatter(x,y,z)

        self.graph.canvas.axes.set_xticks([min(xc),max(xc)])
        #self.graph.canvas.axes.set_yticks([min(yc),(min(yc)+max(yc)/2),max(yc)])
        self.graph.canvas.axes.set_zticks([min(zc),max(zc)])
#        self.graph.canvas.axes.set_xlabel("Length "+str(a))
#        self.graph.canvas.axes.set_ylabel("Max damage "+str(maxdam))
#        self.graph.canvas.axes.set_xlabel("Length %.1f, damage %.2f, wd %.2f" % (a,maxdam,dam_wd))

        if self.showgrid:
            self.graph.canvas.axes.grid(which='major',markevery=0.1)
            self.graph.canvas.axes.set_axis_on()
        else:
            self.graph.canvas.axes.set_axis_off()
        if coldamage:
            #text = u"Length %.1f, damage %.2f, ω %.2f, scaling %.1f" % (a,maxdam,dam_wd,movscale)
            text = u"Length %.1fmm, Damage %.2f, Modal vector %d, Scaling %.0f" % (a,maxdam,nterm+1,movscale)
        else:
            text = u"Length %.1fmm, Modal vector %d, Scaling %.0f" % (a,nterm+1,movscale)
        self.graph.canvas.draw()

#        if self.figtext:
#            self.figtext.set_text(text)
#            self.figtext.set_size(self.graphTitleSize)
#        else:
#            self.figtext = self.graph.canvas.figure.text(0.5, 0.02, text, ha='center', size=self.graphTitleSize)

#        self.figtext = self.graph.canvas.figure.text(0.5, 0.02, text, ha='center', size=self.graphTitleSize)
#        self.figtext.set_text("AAAAAAAAAAAAAAAAAAAAAAAA")
#        self.figtext.set_size(self.graphTitleSize)

        self.graph.canvas.axes.set_title(text,fontsize=self.graphTitleSize, x=0.5, y=-0.04)

        #dodavanje suptitle-a
        self.graph.canvas.figure.suptitle(self.suptitle,fontsize=self.graphTitleSize)

        return













        x = [0.0]*pfile.lengths
        y = [0.0]*pfile.lengths
        #factor = 1/(2*6.35) #ovim treba množiti sile!
        if var < 8:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.data[i][nterm].selfvalue[var]
        else:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.composite[var-8][i]
        name = self.varNames[var]
        if name[-4:] == "mode": name += " %s" % (nterm+1)
        if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
        self.graph.addCurve(x,y,name,symbol=symbols)

        if ((self.offset1 != 0.0) or (self.scale1 != 1.0)) and (self.offset1range != None):
            #print "GOFFSET1",self.offset1,self.offset1range
            self.graph.canvas.axes.autoscale_view(True,True,False)
            top = (self.offset1range[1] - self.offset1range[0])*self.scale1 + self.offset1range[0] + self.offset1
            self.graph.canvas.axes.set_ylim(self.offset1range[0] + self.offset1, top)
        else:
            self.graph.canvas.axes.autoscale_view(True,True,True)
        self.graph.plotGraph()
        

        return
        lFile = self.file1
        lCross = self.cross
        lStrip = self.strip
        lLine = self.line
        lRange1 = self.range1
        lRange2 = self.range2
        lVarX = self.varNames[self.varX]
        lVarY = self.varNames[self.varY]
        
        loads = lRange2-lRange1+1
        x = [0.0]*loads
        y = [0.0]*loads
        for load in range(lRange1,lRange2+1,1):
            x[load-lRange1] = fdata.get(lVarX,lFile,lCross,load,lStrip,lLine)
            y[load-lRange1] = fdata.get(lVarY,lFile,lCross,load,lStrip,lLine)
        self.setGraphAttributes(self.graph)
        self.graph.titleX = Vars.varRichName[lVarX]
        self.graph.titleY = Vars.varRichName[lVarY]
        self.graph.resetGraph()
        segs = [Qt.SolidLine]*loads
        for i in range(loads):
            eig = fdata.get("eig",lFile,vLoad=i+lRange1)
            if eig < 0:
                segs[i] = Qt.CustomDashLine
            elif eig == 0:
                segs[i] = Qt.DashDotLine

        for i in range(len(segs)):
            if fdata.get("eig",lFile,vLoad=i) < 0:
                segs[i] = Qt.DotLine
            elif fdata.get("eig",lFile,vLoad=i) == 0:
                segs[i] = Qt.DashDotLine
#        print segs
        self.graph.addCurve(x,y,symbol=True,segments=segs)
        self.graph.plotGraph()

