#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTab import *
from FileData import *
from FSVisioGraph import *

from math import pi,sin,cos
import numpy as np
import tables as tb

class Graph2D1(FSVisioTab):
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(Graph2D1, self).__init__(title, description, filePrefix, parent)
        self.varNames = ["Min critical force of mode","Min natural frequency of mode",
                         "Min normalized critical stress of mode","Min normalized natural frequency of mode",
                         "Min approximation critical stress of mode","Min natural frequency approximation",
                         "Critical stress relative error of mode","Natural frequency relative error of mode",
                         "Min critical stress of all modes", "Min natural frequency of all modes"]
        self.varUnits = ["MPa","rad/s","MPa","rad/s","MPa","rad/s","","","MPa","rad/s","MPa","rad/s"]
#        self.varNames = Vars.varForcesList + Vars.varCoordsList + Vars.varUserList + Vars.varSpecialList
        self.varDamNames = ["u","v",'w','f']

        self.graph = Graph(self.setStatusBarXY)
        self.contentLayout.addWidget(self.graph)

        self.offset1range = None
        self.offset2range = None

        self.addLabelComboBox("file1", "File", sync=True, callback = self.file1Changed)
        self.addLabelComboBox("var1", "Variable", self.varNames, sync=True)
#        self.addLabelComboBox("damagefor", "Damage for", self.varDamNames, sync=True, callback = self.damforChanged)
        self.addLabelComboBox("damagefor", "Damage for", self.varDamNames)
        self.addLabelSlider("lengthn", "Length", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthnChanged)
        self.addLabelSlider("cross", "Crossection", 0, 100, sync=True, pageStep = 10, singleStep = 1, callback = self.crossChanged)
        self.addLabelSlider("nterm1", "NTerm", sync=True, pageStep = 1, singleStep = 1)
        self.addLabelDoubleSpinBox("movescale","Movement scaling", 0.1, 10000, 0.1, 1, sync=True)
        self.addLabelDoubleSpinBox("scale1","Scaling", 0.1, 100, 0.01, 2, callback = self.scale1Changed)
        self.addLabelDoubleSpinBox("offset1","Offset", -9999, 9999, callback = self.offset1Changed)
        self.addButton("reset1","Reset offset/scale", callback = self.resetOS1)
#        self.addSeparator()
#        varNames = [Vars.varRichName[i] for i in self.varNames]
#        self.addLabelComboBox("varX", "Variable X", varNames)
#        self.addLabelComboBox("varY", "Variable Y", varNames)
        self.addCheckBox("secondg","Show undeformed crosssection")
#        self.addLabelComboBox("file2", "File", sync=True, callback = self.file2Changed)
#        self.addLabelComboBox("var2", "Variable", self.varNames, sync=True)
#        self.addLabelSlider("nterm2", "NTerm", sync=True, pageStep = 1, singleStep = 1)
#        self.addLabelDoubleSpinBox("scale2","Scaling", 0.1, 100, 0.01, 2, callback = self.scale2Changed)
#        self.addLabelDoubleSpinBox("offset2","Offset", -9999, 9999, callback = self.offset2Changed)
#        self.addButton("reset2","Reset offset/scale", callback = self.resetOS2)
        self.addLabelEdit("suptitle","Suptitle")
        self.addLabelDoubleSpinBox("mandamage","Manual damage", 0, 10000, 0.1, 2, sync=True)
        self.addSeparator()
        self.addGraphControls(callback=self.clearMarkers)
        #zasivljenje:
        self.graphSmoothLayout.checkbox.setEnabled(False)
        self.graphColorLinLagVkLayout.checkbox.setVisible(False)
        self.graphSmoothLayout.checkbox.setEnabled(False)
#        self.graphMarkerLayout.setEnabled(False)

        self.scale1 = 1
        self.scale2 = 1

        self.addButton("export","Export graph",self.exportGraph)
        self.syncTab = True
        self.init()
        self.graphSymbolSize = 12
        self.graphNumberSize = 14
        self.graphTitleSize = 14
        self.graphPenSize = 4
        self.movescale = 1000

    def init(self):
        self.file1SetValues([i.fileName for i in fdata.files])
#        self.file2SetValues([i.fileName for i in fdata.files])

    def file1Changed(self, value):
        if value == -1: return
        self.nterm1SetMinMax(1,fdata.files[self.file1].nterm)
        self.lengthnSetMinMax(0,fdata.files[self.file1].lengths-1)
        self.lengthn = 0
        self.lengthnChanged(0)

    def lengthnChanged(self, value):
        if value == -1: return
        if not hasattr(self, 'lengthnLayout'): return
        length = fdata.files[self.file1].length[value]
        self.lengthnLayout.label.setText(self.lengthnLayout.labelText+": "+str(length)+"m")
        if self.damagefor == 1:
            cross = 0
        else:
            cross = 50
        self.cross = cross
        self.crossChanged(cross)

    def damforChanged(self, value):
        print "DFC1"
        if value == -1: return
        if value == 1:
            cross = 56
        else:
            cross = 78
        self.cross = cross
        self.crossChanged(cross)
        print "DFC2"

    def crossChanged(self, value):
        if value == -1: return
        if not hasattr(self, 'crossLayout'): return
        length = fdata.files[self.file1].length[self.lengthn]
        cross = length * value / 100
        self.crossLayout.label.setText(self.crossLayout.labelText+": "+str(cross)+"mm")
        self.update()

#    def file2Changed(self, value):
#        if value == -1: return
#        self.nterm2SetMinMax(1,fdata.files[self.file2].nterm)

    def offset1Changed(self, value):
        if value == -1: return
        if (value != 0):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.scale1 == 1.0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

#    def offset2Changed(self, value):
#        if (value == -1) or (self.graph.canvas.axes2 == None): return
#        if (value != 0):
#            if self.offset2range == None:
#                self.offset2range = self.graph.canvas.axes2.get_ylim()
#        elif self.scale2 == 1.0:
#            self.offset2range = None
#        #print "OFFSET2",self.offset2,self.offset2range
#        self.update()

    def scale1Changed(self, value):
        if value == -1: return
        if (value != 1):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.offset1 == 0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

#    def scale2Changed(self, value):
#        if (value == -1) or (self.graph.canvas.axes2 == None): return
#        if (value != 1):
#            if self.offset2range == None:
#                self.offset2range = self.graph.canvas.axes2.get_ylim()
#        elif self.offset2 == 0:
#            self.offset2range = None
#        #print "OFFSET2",self.offset2,self.offset2range
#        self.update()

    def resetOS1(self):
        self.offset1 = 0.0
        self.scale1 = 1.0

#    def resetOS2(self):
#        self.offset2 = 0.0
#        self.scale2 = 1.0

    def exportGraph(self):
        title = self.filePrefix + "_" + self.file1Text + "_" + self.var1Text
        if self.var1 < 8: title += " " + str(self.nterm1)
        if self.secondg:
            title += "_" + self.var2Text
            if self.var2 < 8: title += " " + str(self.nterm2)
        self.graph.export("./export/",title)

    def clearMarkers(self):
        self.graph.markers = []
        self.plot()

    def frange(self, start, stop, n):
        L = [0.0] * n
        nm1 = n - 1
        nm1inv = 1.0 / nm1
        for i in range(n):
            L[i] = nm1inv * (start*(nm1 - i) + stop*i)
        return L

    def plot(self):
        self.setGraphAttributes(self.graph)
        self.graph.resetGraph()
        self.graph.titleY = ""

        symbols = self.secondg
        pfile = fdata.files[self.file1]
        nterm = self.nterm1 - 1

        length = self.lengthn
        a = pfile.length[length]
        y = a * self.cross / 100
        movscale = self.movescale
        scale = 1
        load = 0
        forcefreq = 0
        sections = 50

        #self.graph.titleX = "Crosssection deformation at y=" + str(y) + "m"
        self.graph.titleX = "Length %.1fmm, Crosssection at %.1fmm, Scaling %.0f" % (a,y,movscale)

        #izdvoji damages tabelu
        condition = "(a1 == %f) & (ii == %d)" % (a, self.nterm1)
        #print condition
        damages = np.asmatrix(fdata.damages.read_where(condition)[0]['fatigue_damage_matrix'])
        #print "DAMAGES\n",damages
        damagefor = self.damagefor   # 0-u, 1-v, 2-w, 3-f

        data = pfile.data[length][nterm].coord[forcefreq]
        data0 = pfile.basecoord
        coords = []
        xyzcol = []
        xc = []
        yc = []
        zc = []

        dam_kk = 48*24099000000*0.000003718
        dam_mm = 0.493*10000*0.002815
        dam_k = dam_kk/a*1000/a*1000/a*1000
        dam_m = dam_mm*a/1000
        dam_wd = pfile.data[length][nterm].selfvalue[3]
#        dam_wd = pfile.composite[1][length]
        dam_rd = -dam_k+dam_wd*dam_wd*dam_m
        dam_rdk = -dam_rd/dam_k

        for lin in range(pfile.lines):
            x1 = data[lin].x*movscale*scale
            y1 = data[lin].y*movscale*scale
            z1 = data[lin].z*movscale*scale

            arg = (nterm+1)*pi*y/a
            sarg = sin(arg)
            carg = cos(arg)
            coords.append((x1*sarg+data0[lin].x*scale,y1*carg+y*scale,z1*sarg+data0[lin].z*scale))
#            row = lin*4+damagefor
#            damage = 0
#            for l in range(pfile.lines*4):
#                dam = damages[row,l]
#                if np.isfinite(dam):
#                    #if dam > 0: damage += dam
#                    damage += abs(dam)
#            #xyzcol.append(abs(x1*sarg)+abs(y1*cos(arg))+abs(z1*sarg))
            if damagefor == 1:
                damarg = carg
            else:
                damarg = sarg
            if self.mandamage == 0:
                damage = dam_rdk*damarg
            else:
                damage = self.mandamage*damarg
            xyzcol.append(abs(damage))
            xc.append(coords[-1][0])
            yc.append(coords[-1][1])
            zc.append(coords[-1][2])

        print "Damage",damage,"Cross",y
        xyzmaxcol = max(xyzcol)
        if xyzmaxcol < 1: xyzmaxcol = 1

        #nedeformisani presek
        if self.secondg:
            for s in range(pfile.strips):
                l1 = pfile.strip[s].line[0]-1
                l2 = pfile.strip[s].line[1]-1
                x = [data0[l1].x*scale,data0[l2].x*scale]
                y = [data0[l1].z*scale,data0[l2].z*scale]
                col = (xyzcol[l1] + xyzcol[l2])/xyzmaxcol/2
                #color=(col*0.8+0.1,0,0.9-col*0.8,1)
                color=(0.5,0.5,0.5,1)
                self.graph.addCurve(x,y,"",symbol=False, color=color)

        #deformisani presek
        divide = 8
        for s in range(pfile.strips):
            l1 = pfile.strip[s].line[0]-1
            l2 = pfile.strip[s].line[1]-1
            xr = self.frange(coords[l1][0],coords[l2][0],divide)
            yr = self.frange(coords[l1][2],coords[l2][2],divide)
            cr = self.frange(xyzcol[l1],xyzcol[l2],divide)
            colors = []
            for i in range(divide-1):
                col = (cr[i] + cr[i+1])/xyzmaxcol/2
                color=(col*0.8+0.1,0.9-col*0.8,0.1,1)
                x = [xr[i], xr[i+1]]
                y = [yr[i], yr[i+1]]
                self.graph.addCurve(x,y,"",symbol=False, color=color)

        #skaliranje da bude malo manje od površine grafika
        xr = max(xc)-min(xc)
        zr = max(zc)-min(zc)
        space = 0.05
        self.graph.canvas.axes.autoscale_view(True,True,False)
        self.graph.canvas.axes.set_xlim(min(xc) - space*xr, max(xc) + space*xr)
        self.graph.canvas.axes.set_ylim(min(zc) - space*zr, max(zc) + space*zr)

        #dodavanje tačaka linija
        markx = []
        marky = []
        markc = []
        for lin in range(pfile.lines):
            col = xyzcol[lin]/xyzmaxcol
            color=(col*0.8+0.1,0.9-col*0.8,0.1,1)
            self.graph.addMarker(xc[lin],zc[lin],color)

        #dodavanje suptitle-a
        self.graph.canvas.figure.suptitle(self.suptitle,fontsize=self.graphTitleSize)

        self.graph.plotGraph()


        return

        var = self.var1
        x = [0.0]*pfile.lengths
        y = [0.0]*pfile.lengths
        #factor = 1/(2*6.35) #ovim treba množiti sile!
        if var < 8:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.data[i][nterm].selfvalue[var]
        else:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.composite[var-8][i]
        name = self.varNames[var]
        if name[-4:] == "mode": name += " %s" % (nterm+1)
        if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
        self.graph.addCurve(x,y,name,symbol=symbols)

        if self.secondg:
            pfile = fdata.files[self.file2]
            nterm = self.nterm2 - 1
            var = self.var2
            x = [0.0]*pfile.lengths
            y = [0.0]*pfile.lengths
            if var < 8:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = pfile.data[i][nterm].selfvalue[var]
            else:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = pfile.composite[var-8][i]
            name = self.varNames[var]
            if name[-4:] == "mode": name += " %s" % (nterm+1)
            if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
            self.graph.addCurve(x,y,name,symbol=symbols,secondAxe=True)
        if ((self.offset1 != 0.0) or (self.scale1 != 1.0)) and (self.offset1range != None):
            #print "GOFFSET1",self.offset1,self.offset1range
            self.graph.canvas.axes.autoscale_view(True,True,False)
            top = (self.offset1range[1] - self.offset1range[0])*self.scale1 + self.offset1range[0] + self.offset1
            self.graph.canvas.axes.set_ylim(self.offset1range[0] + self.offset1, top)
        else:
            self.graph.canvas.axes.autoscale_view(True,True,True)
        if self.graph.canvas.axes2:
            if ((self.offset2 != 0.0) or (self.scale2 != 1.0)) and (self.offset2range != None):
                #print "GOFFSET2",self.offset2,self.offset2range
                self.graph.canvas.axes2.autoscale_view(True,True,False)
                top = (self.offset2range[1] - self.offset2range[0])*self.scale2 + self.offset2range[0] + self.offset2
                self.graph.canvas.axes2.set_ylim(self.offset2range[0] + self.offset2, top)
            else:
                self.graph.canvas.axes2.autoscale_view(True,True,True)
        self.graph.plotGraph()

        return
        lFile = self.file1
        lCross = self.cross
        lStrip = self.strip
        lLine = self.line
        lRange1 = self.range1
        lRange2 = self.range2
        lVarX = self.varNames[self.varX]
        lVarY = self.varNames[self.varY]
        
        loads = lRange2-lRange1+1
        x = [0.0]*loads
        y = [0.0]*loads
        for load in range(lRange1,lRange2+1,1):
            x[load-lRange1] = fdata.get(lVarX,lFile,lCross,load,lStrip,lLine)
            y[load-lRange1] = fdata.get(lVarY,lFile,lCross,load,lStrip,lLine)
        self.setGraphAttributes(self.graph)
        self.graph.titleX = Vars.varRichName[lVarX]
        self.graph.titleY = Vars.varRichName[lVarY]
        self.graph.resetGraph()
        segs = [Qt.SolidLine]*loads
        for i in range(loads):
            eig = fdata.get("eig",lFile,vLoad=i+lRange1)
            if eig < 0:
                segs[i] = Qt.CustomDashLine
            elif eig == 0:
                segs[i] = Qt.DashDotLine

        for i in range(len(segs)):
            if fdata.get("eig",lFile,vLoad=i) < 0:
                segs[i] = Qt.DotLine
            elif fdata.get("eig",lFile,vLoad=i) == 0:
                segs[i] = Qt.DashDotLine
#        print segs
        self.graph.addCurve(x,y,symbol=True,segments=segs)
        self.graph.plotGraph()

