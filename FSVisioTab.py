#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTabControls import *
#from FSVisioGraph3D import *

class FSVisioTab(QWidget):
    """Main class for one tab"""
    #tab counter
    tabCounter = 0
    #tab list
    tabs = []
    currentTab = -1
    #synchronization values
    syncValues = {}
    #statusbar
    statusBar = None
    #syncing in progress
    syncing = False
    #globally iterable values
    iterableGlobal = {}
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(FSVisioTab, self).__init__(parent)
        #tab number
        FSVisioTab.tabCounter += 1
        self.number = FSVisioTab.tabCounter-1
        #this is used as an indicator that changes are in place (update is done only after all changes)
        self.updateCounter = 0
        #dictionary of variable names and their get methods
        self.get = {}
        #dictionary of variable names and their set methods
        self.set = {}
        #list with above variables (nedded for their order of initialization)
        self.vars = []
        #synchronization for variables
        self.syncTo = {}
        self.syncFrom = {}
        #iterable values
        self.iterable = {}
        #is this tab synchronized
        self.syncTab = False
        #title of the tab
        self.title = title
        self.filePrefix = filePrefix
        #statusbar messages
        self.statusMessage = description
        self.defaultStatus = description
        #default parent for new widgets
        self.defaultParent = None
        #array of controls tabs
        self.controls = []

        #tab layout
        self.layout = QHBoxLayout()
        self.setLayout(self.layout)
        #self.widget.setMaximumWidth(AppMaxWidth)
        #self.widget.setMaximumHeight(AppMaxHeight)

        #tab content
        self.content = QWidget()
        self.layout.addWidget(self.content)
        self.contentLayout = QVBoxLayout()
        self.content.setLayout(self.contentLayout)

        #tab controls
        self.controlsTab = QTabWidget()
        self.layout.addWidget(self.controlsTab)
        self.controlsTab.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        self.connect(self.controlsTab, SIGNAL("currentChanged(int)"), self.controlsTabChanged)
        self.controlsLayout = self.addControlsTab("Controls")
        self.setDefaultParent()

        self.updateOff()

        if FSVisioTab.statusBar == None:
            FSVisioTab.statusBar = QStatusBar(self)
            FSVisioTab.statusBar.setObjectName("statusbar")
            FSVisioTab.statusBar.xyLabel = QLabel()
            FSVisioTab.statusBar.addPermanentWidget(FSVisioTab.statusBar.xyLabel)
        FSVisioTab.tabs.append(self)

    def __setattr__(self, name, value):
        if ("set" in self.__dict__) and (name in self.set):
            self.set[name](value)
        else:
            self.__dict__[name] = value

    def __getattr__(self, name):
        if ("get" in self.__dict__) and (name in self.get):
            return self.get[name]()
        else:
            print "Unknown attribute:", name
#            print self.__dict__
            raise AttributeError

    def setStatusBar(self, status):
        self.statusMessage = status
        if FSVisioTab.currentTab == self.number:
            FSVisioTab.statusBar.showMessage(status)
            QApplication.processEvents()

    def setStatusBarXY(self, xy):
        FSVisioTab.statusBar.xyLabel.setText(xy)

    def defaultStatusBar(self):
        self.setStatusBar(self.defaultStatus)

    def updateOff(self):
        """Turns off update method; can be nested"""
        self.updateCounter += 1

    def updateOn(self):
        """Turns update method on (if this is the last nested On statement, that is)"""
        self.updateCounter -= 1
        self.update()

    def setActive(self, active):
        print self.title, "Active:", active
        if active:
            self.updateOff()
            self.syncSelf()
            self.updateOn()

    def init(self):
        """Default initialization function"""
        pass

    def plot(self):
        """Default plot function"""
        pass

    def update(self, value=None):
        """Default update function"""
        if self.updateCounter > 0: return
        #print "Update",self.title
        self.updateCounter += 1
        self.plot()
        self.syncOthers()
        self.updateCounter -= 1

    def dummySetValue(self,value):
        pass

    def dummyCallback(self,value):
        pass

    def syncOthers(self):
        if FSVisioTab.syncing: return
        FSVisioTab.syncing = True
#        print self.title, "SyncOthers"
        for var in self.syncTo:
            value = getattr(self,var)
#            print var, self.syncTo[var], value
            if value != None:
                FSVisioTab.syncValues[self.syncTo[var]] = value
#        for tab in FSVisioTab.tabs:
#            if tab.syncTab and (tab != self): tab.syncSelf()
        FSVisioTab.syncing = False

    def syncSelf(self):
#        self.updateOff()
#        print self.title, "SyncSelf"
        for var in self.syncFrom:
            value = FSVisioTab.syncValues[var]
#            print var, self.syncFrom[var], value
            if value != None:
                setattr(self, self.syncFrom[var], value)
#        self.updateOn()

    def exportGraph(self):
        pass

    def exportGraphBy(self, name=None):
        if name == None: name = str(self.exportByText)
        if name in self.iterable:
            iterator = self.iterable[name]
            iteratorLocal = True
        else:
            iterator = self.iterableGlobal[name]
            iteratorLocal = False
        graphIterator = iterator()
        if iteratorLocal: currentValue = getattr(self,name)
        for graph in graphIterator:
            self.updateOff()
            setattr(self,name,graph)
            if iteratorLocal: self.update()
            else: self.init()
            self.updateOn()
            self.exportGraph()
        self.updateOff()
        if iteratorLocal: setattr(self,name, currentValue)
        if not iteratorLocal: self.init()
        self.updateOn()

    def addAttribute(self, name, getter, setter, addToVars=False, sync=None):
        if name in self.get: raise Exception("Name "+name+" alredy defined!")
        self.get[name] = getter
        self.set[name] = setter
        if addToVars:
            self.vars.append(name)
            if sync != None:
                syncTo = sync if isinstance(sync, basestring) else name
                self.syncTo[name] = syncTo
                self.syncFrom[syncTo] = name
                if syncTo not in FSVisioTab.syncValues:
                    FSVisioTab.syncValues[syncTo] = None

    def setDefaultParent(self, parent=None):
        if parent == None:
            self.defaultParent = self.controlsLayout
        else:
            self.defaultParent = parent

    def addControlsTab(self, name):
        """Adds a new tab in controls widget"""

#        self.scrollArea = QtGui.QScrollArea(self)
#        self.scrollArea.setWidgetResizable(True)
#        self.scrollAreaWidgetContents = QtGui.QWidget(self.scrollArea)
#        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 380, 247))
#        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

#        self.verticalLayout = QtGui.QVBoxLayout(self)
#        self.verticalLayout.addWidget(self.pushButton)
#        self.verticalLayout.addWidget(self.scrollArea)

#        self.verticalLayoutScroll = QtGui.QVBoxLayout(self.scrollAreaWidgetContents)

        controls = QScrollArea(self)
        controls.setWidgetResizable(True)
        self.controls.append(controls)
        self.controlsTab.addTab(controls, name)
        scrollContents = QWidget(controls)
        controls.setWidget(scrollContents)
        #scrollContents.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        scrollContents.setMinimumWidth(260)

        controlsLayout = QVBoxLayout(scrollContents)
        controlsLayout.setAlignment(Qt.AlignTop)
        #controls.setLayout(controlsLayout)

        #controls.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        #controls.setMinimumWidth(260)
        if self.controlsTab.count() > 1:
            self.controlsTab.setTabPosition(QTabWidget.North)
            self.controlsTab.tabBar().show()
        else:
            self.controlsTab.setTabPosition(QTabWidget.South)
            self.controlsTab.tabBar().hide()
        controls.update = None
        return controlsLayout

    def nameControlsTab(self, name):
        """Set name for first tab in controls widget"""
        #idx = self.controlsTab.indexOf(self.defaultParent.parent())
        self.controlsTab.setTabText(0,name)

    def controlsTabChanged(self, tab):
        if self.controls[tab].update != None:
            self.controls[tab].update()

    def addSeparator(self, parent=True):
        """Adds a separator line to controls"""
        layout = QHBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.separator = QFrame()
        layout.addWidget(layout.separator)
        layout.separator.setFrameShape(QFrame.HLine)
        layout.separator.setFrameShadow(QFrame.Sunken)
        return layout

    def addLabelComboBox(self, name, label, values=None, data=None, callback=None, sync=None, parent=True):
        """Adds a combobox with a label to controls
            Also, automatically are created attributes name (current data of the combobox) and nameLayout
            (containing the whole layout), and method nameSetValues (for setting combobox's values and data)"""
        if callback == None: callback = self.update
        layout = ComboControl(label, values, data, callback)
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        #adding an attribute representing the whole layout
        setattr(self,name+"Layout",layout)
        #adding a method for setting combobox's values and data
        setattr(self,name+"SetValues",layout.setValues)
        #adding a method for iterating through combobox's values
        setattr(self,name+"Values",layout.valuesGenerator)
        self.iterable[name] = layout.valuesGenerator
        if values != None:
            layout.setValues(values, data)
        #get and set methods for combobox
        self.addAttribute(name, layout.combo.currentData, layout.combo.setCurrentData, True, sync)
        self.addAttribute(name+"Text", layout.getText, self.dummySetValue)
        return layout

    def addLabelCheckList(self, name, label, values=None, data=None, callback=None, sync=None, parent=True):
        """Adds a list with checkable items and a label to controls
            Also, automatically are created attributes name (to access list's elements) and nameLayout
            (containing the whole layout), and method nameSetValues (for setting list's values)"""
        if callback == None: callback = self.update
        layout = CheckListControl(label, values, data, callback)
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        #adding an attribute representing the whole layout
        setattr(self,name+"Layout",layout)
        #adding a method for setting list's values and data
        setattr(self,name+"SetValues",layout.setValues)
        if values != None:
            layout.setValues(values, data)
        #adding a method for getting list's check states
        setattr(self,name+"GetCheckStates",layout.getCheckStates)
        return layout

    def addFromToRange(self, name, label, minvalue=0, maxvalue=100, callback=None, sync=None, parent=True):
        """Adds two spinboxes with a label for a range
            Also, automatically are created attributes name1, name2 (current top and bottom range) and nameLayout
            (containing the whole layout), and method nameSetMinMax (for setting range's min and max values)"""
        if callback == None: callback = self.update
        layout = RangeControl(label, minvalue, maxvalue, callback)
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)

        setattr(self, name+"Layout", layout)
        setattr(self, name+"SetMinMax", self.__setFromToRangeMinMax(name))
        name1 = name+"1"
        name2 = name+"2"
        self.addAttribute(name1, layout.getRange1, layout.setRange1, True, sync)
        self.addAttribute(name2, layout.getRange2, layout.setRange2, True, sync)
        return layout

    def __setFromToRangeMinMax(self, name):
        """Function with closure for setting range's min and max values"""
        def setFunction(minvalue, maxvalue):
            self.updateOff()
            layout = getattr(self, name+"Layout")
            layout.setMinMax(minvalue, maxvalue)
            self.updateOn()
        return setFunction

    def addLabelSpinBox(self, name, label, minvalue=0, maxvalue=100, step=1, callback=None, sync=None, parent=True):
        """Adds a spinbox with a label
            Also, automatically are created attributes name (current value of spinbox) and nameLayout
            (containing the whole layout), and method nameSetMinMaxStep (for setting spinbox's min, max and step values)"""
        if parent == None: parent = self.controlsLayout
        layout = QHBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.label = QLabel(label)
        layout.addWidget(layout.label, 0, Qt.AlignRight)
        layout.spinbox = QSpinBox()
        layout.addWidget(layout.spinbox, 1)
        layout.spinbox.setAlignment(Qt.AlignRight)
        minmaxstep = self.__setSpinBoxMinMaxStep(name)
        if callback == None: callback = self.update
        self.connect(layout.spinbox, SIGNAL("valueChanged(int)"), callback)
        setattr(self, name+"Layout", layout)
        setattr(self, name+"SetMinMaxStep", minmaxstep)
        minmaxstep(minvalue, maxvalue, step)
        self.addAttribute(name, layout.spinbox.value, layout.spinbox.setValue, True, sync)
        return layout

    def __setSpinBoxMinMaxStep(self, name):
        """Function with closure for setting spinbox's min, max and step values"""
        def setFunction(minvalue, maxvalue, step=1):
            self.updateOff()
            layout = getattr(self, name+"Layout")
            if (minvalue != None) and (maxvalue != None):
                layout.spinbox.setRange(minvalue, maxvalue)
            layout.spinbox.setSingleStep(step)
            self.updateOn()
        return setFunction

    def addLabelDoubleSpinBox(self, name, label, minvalue=0, maxvalue=100, step=0.1, decimals=1, callback=None, sync=None, parent=True):
        """Adds a spinbox with a label
            Also, automatically are created attributes name (current value of spinbox) and nameLayout
            (containing the whole layout), and method nameSetMinMaxStep (for setting spinbox's min, max and step values)"""
        layout = QHBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.label = QLabel(label)
        layout.addWidget(layout.label, 0, Qt.AlignRight)
        layout.spinbox = QDoubleSpinBox()
        layout.addWidget(layout.spinbox, 1)
        layout.spinbox.setAlignment(Qt.AlignRight)
        minmaxstepdec = self.__setDoubleSpinBoxMinMaxStepDec(name)
        if callback == None: callback = self.update
        self.connect(layout.spinbox, SIGNAL("valueChanged(double)"),callback)
        setattr(self, name+"Layout", layout)
        setattr(self, name+"SetMinMaxStepDec", minmaxstepdec)
        minmaxstepdec(minvalue, maxvalue, step, decimals)
        self.addAttribute(name, layout.spinbox.value, layout.spinbox.setValue, True, sync)
        return layout

    def __setDoubleSpinBoxMinMaxStepDec(self, name):
        """Function with closure for setting spinbox's min, max, step and decimals values"""
        def setFunction(minvalue, maxvalue, step=0.1, decimals=1):
            self.updateOff()
            layout = getattr(self, name+"Layout")
            if (minvalue != None) and (maxvalue != None):
                layout.spinbox.setRange(minvalue, maxvalue)
            layout.spinbox.setSingleStep(step)
            layout.spinbox.setDecimals(decimals)
            self.updateOn()
        return setFunction

    def addAngleDial(self, name, label, callback=None, sync=None, parent=True):
        """Adds a dial and spinbox for an angle
            Also, automatically are created attributes name (current angle value) and nameLayout
            (containing the whole layout)"""
        if callback == None: callback = self.update
        layout = AngleControl(label, callback)
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        setattr(self, name+"Layout", layout)
        self.addAttribute(name, layout.getAngle, layout.setAngle, True, sync)
        return layout

    def addCheckBox(self, name, label, checked=False, callback=None, sync=None, parent=True):
        """Adds a CheckBox
            Also, automatically are created attributes name (current checkbox value) and nameLayout
            (containing the whole layout)"""
        if callback == None: callback = self.update
        layout = QHBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.checkbox = QCheckBox(label)
        layout.addWidget(layout.checkbox)
        state = Qt.Checked if checked else Qt.Unchecked
        layout.checkbox.setCheckState(state)
        self.connect(layout.checkbox, SIGNAL("stateChanged(int)"), callback)
        setattr(self, name+"Layout", layout)
        self.addAttribute(name, self.__getCheckBox(name), self.__setCheckBox(name), True, sync)
        return layout

    def __getCheckBox(self, name):
        """Function with closure for getting checkbox's status"""
        def getFunction():
            layout = getattr(self, name+"Layout")
            return layout.checkbox.checkState() == Qt.Checked
        return getFunction

    def __setCheckBox(self, name):
        """Function with closure for setting checkbox's status"""
        def setFunction(checked):
            self.updateOff()
            layout = getattr(self, name+"Layout")
            state = Qt.Checked if checked else Qt.Unchecked
            layout.checkbox.setCheckState(state)
            self.updateOn()
        return setFunction

    def addButton(self, name, label, callback=None, parent=True):
        """Adds a Button
            Also, automatically are created attributes name (current checkbox value) and nameLayout
            (containing the whole layout)"""
        if callback == None: callback = self.update
        layout = QHBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.button = QPushButton(label)
        layout.addWidget(layout.button)
        self.connect(layout.button, SIGNAL("clicked()"), callback)
        setattr(self, name+"Layout", layout)
        return layout

    def addLabel(self, name, label, parent=True, sync=False):
        """Adds a Label
            Also, automatically are created attributes name (current label text) and nameLayout
            (containing the whole layout)"""
        layout = QHBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.label = QLabel(label)
        layout.addWidget(layout.label)
        setattr(self, name+"Layout", layout)
        self.addAttribute(name, layout.label.text, layout.label.setText, True, sync)
        return layout

    def addLabelEdit(self, name, label, parent=True, callback=None):
        """Adds an edit box
            Also, automatically are created attributes name (current label text) and nameLayout
            (containing the whole layout)"""
        if callback == None: callback = self.update
        layout = QHBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.label = QLabel(label)
        layout.addWidget(layout.label)
        layout.edit = QLineEdit("")
        layout.addWidget(layout.edit)
        layout.button = QPushButton(u"✓")
        layout.addWidget(layout.button)
        size = 30
        layout.button.setMaximumWidth(size)
        layout.button.setMaximumHeight(size)
        self.connect(layout.button, SIGNAL("clicked()"), callback)
        setattr(self, name+"Layout", layout)
        self.addAttribute(name, layout.edit.text, layout.edit.setText)
        return layout

    def addRadioControl(self, name, label, buttons, data=None, colrows=1, byColumns=False, topTitle=True, callback=None, sync=None, parent=True):
        """Adds a radio button group
            Also, automatically are created attributes name (current checkbox value) and nameLayout
            (containing the whole layout)"""
        if callback == None: callback = self.update
        layout = RadioControl(label, buttons, data, colrows, byColumns, topTitle, callback)
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        setattr(self, name+"Layout", layout)
        setattr(self, name+"SetTextId", self.__radioSetTextId(name))
        self.addAttribute(name, layout.getButtonId, layout.setButtonId, True, sync)
        self.addAttribute(name+"Text", self.__getRadioText(name), self.dummySetValue)
        return layout

    def __radioSetTextId(self, name):
        def setFunction(buttons, data=None):
            self.updateOff()
            layout = getattr(self, name+"Layout")
            layout.setButtonsTextId(buttons, data)
            self.updateOn()
        return setFunction

    def __getRadioText(self, name):
        """Function with closure for getting checkbox's status"""
        def getFunction():
            layout = getattr(self, name+"Layout")
            return layout.getButtonText()
        return getFunction

    def addLabelSlider(self, name, label, minvalue=0, maxvalue=100, pageStep=45, singleStep=5, callback=None, sync=None, parent=True):
        """Adds a radio button group
            Also, automatically are created attributes name (current checkbox value) and nameLayout
            (containing the whole layout)"""
        if callback == None: callback = self.update
        layout = SliderControl(label, minvalue, maxvalue, pageStep, singleStep, callback)
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        setattr(self, name+"Layout", layout)
        setattr(self, name+"SetMinMax", self.__sliderSetMinMax(name))
        self.addAttribute(name, layout.getSlider, layout.setSlider, True, sync)
        return layout

    def __sliderSetMinMax(self, name):
        def setFunction(minvalue=0, maxvalue=100):
            self.updateOff()
            layout = getattr(self, name+"Layout")
            layout.setMinMax(minvalue, maxvalue)
            self.updateOn()
        return setFunction

    def addExportIterator(self, name="exportBy", label="Export by", callback=None, sync=None, parent=True):
        """Adds a combobox with a label to controls
            Also, automatically are created attributes name (current data of the combobox) and nameLayout
            (containing the whole layout), and method nameSetValues (for setting combobox's values and data)"""
        if callback == None: callback = self.exportGraphBy
        values = self.iterable.keys()
        values.extend(FSVisioTab.iterableGlobal.keys())
        values.sort()
        data = None
        layout = ComboControl(label, values, data, callback, button=True)
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        #adding an attribute representing the whole layout
        setattr(self,name+"Layout",layout)
        #adding a method for setting combobox's values and data
        setattr(self,name+"SetValues",layout.setValues)
        if values != None:
            layout.setValues(values, data)
        #get and set methods for combobox
        self.addAttribute(name, layout.combo.currentData, layout.combo.setCurrentData, True, sync)
        self.addAttribute(name+"Text", layout.getText, self.dummySetValue)
        return layout

    def addGraphControls(self, scale=False, callback=None, parent=True):
        if callback == None: callback = self.update
        layout = QVBoxLayout()
        if parent == True:
            self.defaultParent.addLayout(layout)
        elif parent != False:
            parent.addLayout(layout)
        layout.label1 = QLabel("Graph controls")
        layout.addWidget(layout.label1)
        layout.grid1 = QGridLayout()
        layout.addLayout(layout.grid1)
        layout.grid1.addLayout(self.addCheckBox("graphGrid","Show grid", checked=True, parent=False),0,0)
        layout.grid1.addLayout(self.addCheckBox("graphSmooth","Smooth graph", checked=False, parent=False),0,1)
        layout.grid1.addLayout(self.addCheckBox("graphInvert","Invert Y", checked=False, parent=False),1,0)
        layout.grid1.addLayout(self.addCheckBox("graphFlip","Flip axes", checked=False, parent=False),1,1)
        layout.grid1.addLayout(self.addCheckBox("graphColor","Color Graphs", checked=True, parent=False),2,0)
        layout.grid1.addLayout(self.addCheckBox("graphShowSymbols","Show Symbols", checked=False, parent=False),2,1)
        layout.addLayout(self.addCheckBox("graphColorLinLagVk","Color LIN/LAG/VK", checked=False, parent=False))
        #layout.addLayout(self.addCheckBox("graphColor","Color Graphs", checked=False, parent=False))
        #layout.addLayout(self.addCheckBox("graphShowSymbols","Show Symbols", checked=True, parent=False))
        if scale:
            layout.addLayout(self.addCheckBox("scale","Scale all graph values", checked=False, parent=False))
        layout.addLayout(self.addSeparator(parent=False))
        layout.angle = self.addAngleDial("graphAngle","Bottom numbers angle", parent=False)
        layout.addLayout(layout.angle)
        layout.addLayout(self.addSeparator(parent=False))
        layout.label2 = QLabel("Graph sizes")
        layout.addWidget(layout.label2)
        layout.grid2 = QGridLayout()
        layout.addLayout(layout.grid2)
        layout.grid2.addLayout(self.addLabelSpinBox("graphPenSize","Line", 1, 10, parent=False),0,0)
        layout.grid2.addLayout(self.addLabelSpinBox("graphSymbolSize","Symbol", 1, 50, parent=False),0,1)
        layout.grid2.addLayout(self.addLabelSpinBox("graphNumberSize","Number", 1, 50, parent=False),1,0)
        layout.grid2.addLayout(self.addLabelSpinBox("graphTitleSize","Title", 1, 50, parent=False),1,1)
        layout.grid2.addLayout(self.addLabelSpinBox("graphWidth","Width %", 50, 200, parent=False),2,0)
        layout.grid2.addLayout(self.addLabelSpinBox("graphHeight","Height %", 50, 200, parent=False),2,1)
        layout.grid2.addLayout(self.addLabelSpinBox("graphLegendScale","Legend %", 50, 200, parent=False),3,0)
        layout.grid2.addLayout(self.addLabelSpinBox("graphLegendColumns","Columns", 1, 10, parent=False),3,1)
        self.graphPenSize = 2
        self.graphSymbolSize = 8
        self.graphNumberSize = 10
        self.graphTitleSize = 10
        self.graphWidth = 100
        self.graphHeight = 100
        self.graphLegendScale = 100
        self.graphLegendColumns = 1
#        layout.addLayout(self.addSeparator(parent=False))
#        layout.marker = self.addRadioControl("graphMarker", "Marker type", ["Horizontal","Vertical", "Point"],
#                                             byColumns=True, parent=False)
#        layout.addLayout(layout.marker)
#        layout.markerBtn = self.addButton("graphClear", "Clear markers", callback, parent=False)
#        layout.marker.labelLayout.addLayout(layout.markerBtn)
#        layout.alignLR = self.addRadioControl("alignLR", "Align", ["Left/Down","Right/Up"],
#                                              byColumns=True, topTitle=False, parent=False)
#        self.alignLR = 1
#        layout.addLayout(layout.alignLR)

#        layout.addLayout(self.addButton("graphCustom","Add custom graph", self.addCustomGraph, parent=False))

        return layout

    def setGraphAttributes(self, graph):
        graph.showGrid = self.graphGrid
        graph.showSymbols = self.graphShowSymbols
        graph.smooth = self.graphSmooth
        graph.invertY = self.graphInvert
        graph.flipAxes = self.graphFlip
        graph.curvePenSize = self.graphPenSize
        graph.symbolSize = self.graphSymbolSize
        graph.axisFontSize = self.graphNumberSize
        graph.titleFontSize = self.graphTitleSize
        graph.graphWidth = self.graphWidth
        graph.graphHeight = self.graphHeight
        graph.graphLegendScale = self.graphLegendScale
        graph.graphLegendColumns = self.graphLegendColumns
        graph.colorLinLagVk = self.graphColorLinLagVk
        graph.color = self.graphColor
        graph.angle = self.graphAngle
        #graph.marker = self.graphMarker
        #graph.alignLR = self.alignLR

    def addCustomGraph(self):
        #Ny
        y = [0,0.04,0.08,0.12,0.16,0.2,0.24,0.28,0.32,0.36,0.4,0.44,0.48,0.52,0.56,0.6,0.64,0.68,0.72,0.76,0.8,0.84,0.88,0.92,0.96,1]
        x = [0,83.1165,166.166,249.15,332.067,414.918,497.704,580.424,663.08,745.67,828.196,910.657,993.055,1075.39,1157.66,1239.87,1322.01,1404.09,1486.11,1568.07,1649.96,1731.79,1813.57,1895.28,1976.93,2058.51]
        name = "Abaqus"
        #w
        y = [0,0.04,0.08,0.12,0.16,0.2,0.24,0.28,0.32,0.36,0.4,0.44,0.48,0.52,0.56,0.6,0.64,0.68,0.72,0.76,0.8,0.84,0.88,0.92,0.96,1]
        x = [0,-0.00011634,-0.000232821,-0.000349445,-0.000466212,-0.000583123,-0.000700177,-0.000817377,-0.000934722,-0.00105221,-0.00116985,-0.00128764,-0.00140557,-0.00152366,-0.00164189,-0.00176027,-0.00187881,-0.0019975,-0.00211634,-0.00223533,-0.00235447,-0.00247377,-0.00259323,-0.00271284,-0.00283261,-0.00295254]
        name = "Abaqus"
        #segs = [Qt.SolidLine]*loads 
        #self.graph.addCurve(x,y,name,symbol=True,segments=segs)
        self.graph.addCurve(x,y,name,symbol=True)
        self.graph.plotGraph()

