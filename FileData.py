# -*- coding: UTF-8 -*-

import ConfigParser
import os, glob, json
from sys import stdout

import numpy as np
import tables as tb

#opis 3D tačke (za kompletnu 3D strukturu)
class Coord3D(object):
    def __init__(self,cx=0,cy=0,cz=0,cf=0,cc=0):
        self.x = cx
        self.y = cy
        self.z = cz
        self.f = cf
#        self.c = cc

#opis 2D tačke (za grafike na pojedinim segmentima, tj. pod-grafike)
class Coord2D(object):
    def __init__(self,cx=0,cy=0):
        self.x = cx
        self.y = cy

#recnici sa promenljivima
class VarDict(object):
    def __init__(self):
        self.code = {'nterm':10, 'var':11, 'load':12, 'Mx':0, 'My':1, 'Mxy':2, 'Nx':3, 'Ny':4, 'Nxy':5, 'u':20, 'v':21, 'w':22, 'f':23}
        self.invCode = {10:'nterm', 11:'var', 12:'load', 0:'Mx', 1:'My', 2:'Mxy', 3:'Nx', 4:'Ny', 5:'Nxy', 20:'u', 21:'v', 22:'w', 23:'f'}
        self.text = {'nterm':'Series term', 'var':'', 'load':'Load', 'Mx':'Mx', 'My':'My', 'Mxy':'Mxy', 'Nx':'Nx', 'Ny':'Ny', 'Nxy':'Nxy', 'u':'u', 'v':'v', 'w':'w', 'f':u'\u03c6'}
        self.unit = {'nterm':[''], 'var':[''],'load':[''], 'Mx':['N'], 'My':['N'], 'Mxy':['N'], 'Nx':['N','N/m','N/m2'], 'Ny':['N','N/m','N/m2'], 'Nxy':['N','N/m','N/m2'], 'u':['m'], 'v':['m'], 'w':['m'], 'f':['']}
        self.unitText = {'nterm':[''], 'var':[''], 'load':[''], 'Mx':['N'], 'My':['N'], 'Mxy':['N'], 'Nx':['N','N/m','N/m<sup>2</sup>'], 'Ny':['N','N/m','N/m<sup>2</sup>'], 'Nxy':['N','N/m','N/m<sup>2</sup>'], 'u':['m'], 'v':['m'], 'w':['m'], 'f':[u'\u00b0']}
        self.g2D1 = ['Mx', 'My', 'Mxy', 'Nx', 'Ny', 'Nxy']
        self.g2D2 = ['u', 'v', 'w', 'f']
        self.g2D3X = ['Mx', 'My', 'Mxy']
        self.g2D3Y = ['u', 'v', 'w', 'f']
        self.g2D4 = ['Mx', 'My', 'Mxy', 'Nx', 'Ny', 'Nxy', 'u', 'v', 'w', 'f', 'var', 'nterm']
        self.g3D1 = ['Mx', 'My', 'Mxy', 'Nx', 'Ny', 'Nxy', 'u', 'v', 'w', 'f']
        self.varname = [u'U', u'V', u'W', u'\u03c6']

#----------------------------------------------------------------------
# struktura za čuvanje podataka iz cfg fajlova
#----------------------------------------------------------------------

#pristup podacima ide ovako (promenljiva za sve fajlove se zove fdata):
#    - podaci o brojevima linija u traci:
#        fdata.files[0~files-1].strip[0~strips-1].line[0~1]
#    - podaci o koordinatama tačaka (load=0 je inicijalno stanje):
#        fdata.files[0~files-1].data[0~crosssections-1][0~loads].coord[0~lines-1].<x,y,z>
#    - podaci o vrednostima promenljivih (za load=0 nema promenljivih):            [promenljiva][linija]
#        fdata.files[0~files-1].data[0~crosssections-1][1~loads].values[0~strips-1][0~5][0~1]

#klasa za listu fajlova
class FFilesData(object):
    def __init__(self):
        self.files = []
        self.varCfg = None
        self.sameParams = True
        self.damagefile = None
        self.damages = None

    def LoadVarCfg(self, cfgfile, cfgdir="./"):
        cfg = ConfigParser.SafeConfigParser()
        if cfgdir[-1] <> "/": cfgdir = cfgdir+"/"
        cfg.read(cfgdir+cfgfile)
        varDict.text['var'] = cfg.get('Var','Name')
        varDict.unit['var'] = cfg.get('Var','Unit')
        varDict.unitText['var'] = cfg.get('Var','Unit')
        self.varCfg = cfg

    def Load(self,cfgdir="./"):
        del self.files[:]
        varDict.text['var'] = ''
        varDict.unit['var'] = ''
        varDict.unitText['var'] = ''
        self.sameParams = True
        flist = glob.glob( os.path.join(cfgdir, 'var.cfg') )
        nvars = 0
        for f in flist:
            fname = os.path.basename(f)
            print "Loading:", fname
            self.LoadVarCfg(fname,cfgdir)
            nvars = -1
        flist = glob.glob( os.path.join(cfgdir, '*.json') )
        flist.sort()
        for f in flist:
            fname = os.path.basename(f)
            nvars = nvars + 1
            if fname != "var.cfg":
                print "Loading:", fname
                self.files.append(FFileData())
                if (self.varCfg != None) and (self.varCfg.has_option("Var",fname[:-4])):
                    self.files[-1].var = self.varCfg.getfloat("Var",fname[:-4])
                    #print fname,self.files[-1].var
                self.files[-1].Load(os.path.basename(f),cfgdir)
                if (self.files[-1].loads != self.files[0].loads) or (self.files[-1].cross != self.files[0].cross) or (self.files[-1].strips != self.files[0].strips) or (self.files[-1].lines != self.files[0].lines):
                    self.sameParams = False
        if (nvars > 0) and (nvars != len(self.files)):
            #options.showMessageText(6,"var.cfg doesn't have all the values!")
            print "var.cfg doesn't have all the values!",nvars
            self.var.name = ""
        if nvars == 0: self.var.name = ""
        #self.Print()
#        min1 = fdata.files[0].min_nterm_self
#        min2 = fdata.files[1].min_nterm_self
#        for i in range(len(fdata.files[0].data[min1][1].coord)):
#            c1 = fdata.files[0].data[min1][1].coord[i].x
#            c2 = fdata.files[1].data[min2][1].coord[i].x
#            print "{0:2} X Abs:{1:10f} Rel:{2:10.2f}%".format(i+1, abs(c1-c2), abs((c1-c2)/0.4)*100)
#            c1 = fdata.files[0].data[min1][1].coord[i].y
#            c2 = fdata.files[1].data[min2][1].coord[i].y
#            print "{0:2} Y Abs:{1:10f} Rel:{2:10.2f}%".format(i+1, abs(c1-c2), abs((c1-c2)/0.4)*100)
#            c1 = fdata.files[0].data[min1][1].coord[i].z
#            c2 = fdata.files[1].data[min2][1].coord[i].z
#            print "{0:2} Z Abs:{1:10f} Rel:{2:10.2f}%".format(i+1, abs(c1-c2), abs((c1-c2)/0.4)*100)
        flist = glob.glob( os.path.join(cfgdir, '*.hdf5') )
        for f in flist:
            fname = os.path.basename(f)
            print "Opening damage file:",fname
            self.damagefile = tb.open_file(f)
            self.damages = self.damagefile.root.damages
            break

    def getValue(self,vFile,vCross,vLoad,vStrip,vLine,vVar,vNT1=False):
        if type(vVar)==type(1): vr = vVar
        else: vr = varDict.code[str(vVar)]
        if vr <= 5:
            if vLoad == 0: return 0
            else: return self.files[vFile].data[vCross][vLoad].values[vStrip][vr][vLine]
        elif vr == 10:
            return self.files[vFile].nterm
        elif vr == 11:
            return self.files[vFile].var
        else:
            if vLoad == 0: return 0
            #print vFile, vStrip,vLine
            l = fdata.files[vFile].strip[vStrip].line[vLine]-1
            if vNT1: l = l+fdata.files[0].lines
            if vr == 20:
                return self.files[vFile].data[vCross][vLoad].coord[l].x
            elif vr == 21:
                return self.files[vFile].data[vCross][vLoad].coord[l].y
            elif vr == 22:
                return self.files[vFile].data[vCross][vLoad].coord[l].z
            elif vr == 23:
                if vNT1: return 0
                else: return self.files[vFile].data[vCross][vLoad].coord[l].f

    def getUnitText(self,vVar,exp10,NUnit=0):
        if type(vVar)==type(1): units = varDict.unitText[varDict.invCode[vVar]]
        else: units = varDict.unitText[str(vVar)]
        if NUnit >= len(units): unit = units[0]
        else: unit = units[NUnit]
        if unit == "": return ""
        if exp10 != 0: unit = unit+u"\u00b710<sup>"+str(exp10)+"</sup>"
        return " ["+unit+"]"

    def Print(self):
        for fl in range(len(self.files)):
            for cs in range(self.files[fl].cross):
                for ld in range(self.files[fl].loads):
                    for ln in range(self.files[fl].lines):
                        print "fl:",fl,"cs:",cs,"ld:",ld,"ln:",ln,"xyz:",self.files[fl].data[cs][ld+1].coord[ln].x,self.files[fl].data[cs][ld+1].coord[ln].y,self.files[fl].data[cs][ld+1].coord[ln].z

#klasa za jedan fajl
class FFileData(object):
    def __init__(self):
        self.fileName = ""
        self.cross = 0
        self.loads = 0
        self.lines = 0
        self.strips = 0
        self.nterm = 0
        self.var = 0
        self.length = []
        self.lengths = 0
        self.min_nterm_self = 0
        self.strip = []            #FStripData    [strips]
        self.data = []            #FLoadData    [length][nterms].FSelfData.coord[0/1].x
        self.loadFactors = [0]
        self.basecoord = []

        """
        print j.keys()
        [u'input', u'results']

        print j["input"]["a1"]
        {u'stop': 2310.0, u'resolution': 201, u'start': 231.0}

        print j["input"]["finite_strips"][0]
        {u'gu': 1.0, u'g': 1805287.39, u'h': 6.35, u'px': 0.38, u'py': 0.15, u'ee1': 20928.75, u'ee2': 8032.99, u'strip': 1, u'node1': 1, u'node2': 2}

        print j["input"]["nodal_lines"][0]
        {u'node': 1, u'x': 0.0, u'z': 0.0}

        print j["results"][0]["a1"]
        231.0

        print j["results"][0]["min_natural_freq"]
        0.265618357575

        print j["results"][0]["ii"]
        1

        print j["results"][0]["min_natural_freq_mode_shapes"][0]
        0.00066838791069

        print j["results"][0]["min_crit_force"]
        -4831.04805011

        print j["results"][0]["min_crit_force_mode_shapes"][0]
        0.000703522099267
        """

    def Load(self, cfgfile, cfgdir="./"):
        self.fileName = cfgfile[:-5]
        if cfgdir[-1] <> "/": cfgdir = cfgdir+"/"
        cfg = json.load(open(cfgdir+cfgfile))
        self.lines = len(cfg["input"]["nodal_lines"])
        for i in range(self.lines):
            self.basecoord.append(Coord3D(cfg["input"]["nodal_lines"][i]["x"],0,cfg["input"]["nodal_lines"][i]["z"],0))
        print "ln",self.lines
        self.strips = len(cfg["input"]["finite_strips"])
        print "st",self.strips
        self.nterm = len(cfg["results"])/cfg["input"]["a1"]["resolution"]
        print "nt",self.nterm
        self.length = [ cfg["results"][x]["a1"] for x in range(0, len(cfg["results"]), self.nterm) ]
        self.lengths = len(self.length)
        self.loadFactors.append(1)
        for i in range(self.strips):
            self.strip.append( FStripData(cfg["input"]["finite_strips"][i]["node1"], cfg["input"]["finite_strips"][i]["node2"]) )
        self.min_nterm_self = 0
#        self.composite = [[],[]]
        self.composite = [[0.0]*self.lengths,[0.0]*self.lengths,[0.0]*self.lengths,[0.0]*self.lengths,[0.0]*self.lengths,[0.0]*self.lengths]
#        print "Calculating composite..."
#        self.composite[0] = list( min( list( a['min_crit_force_normalized'] for a in cfg['results'] if a['a1'] == l)) for l in self.length )
#        self.composite[1] = list( min( list( a['min_natural_freq_normalized'] for a in cfg['results'] if a['a1'] == l)) for l in self.length )
        #for i,l in enumerate(self.length):
        #    composite[0][i] = min( list( a['min_crit_force_normalized'] for a in cfg['results'] if a['a1'] == l) )
        idx = 0
        composite1 = [0.0]*self.nterm
        composite2 = [0.0]*self.nterm
        composite3 = [0.0]*self.nterm
        composite4 = [0.0]*self.nterm
        composite5 = [0.0]*self.nterm
        composite6 = [0.0]*self.nterm
        for l in range(self.lengths):
            stdout.write("\rlength: %.2f/%.2f" % (self.length[l],self.length[-1]))
            stdout.flush()
            newlen = []
            self.data.append(newlen)
            for n in range(self.nterm):
                newlen.append(FSelfData())
                newlen[-1].selfvalue[0] = cfg["results"][idx]["min_crit_force"]
                newlen[-1].selfvalue[1] = cfg["results"][idx]["min_natural_freq"]
                newlen[-1].selfvalue[2] = cfg["results"][idx]["min_crit_stress_normalized"]
                newlen[-1].selfvalue[3] = cfg["results"][idx]["min_natural_freq_normalized"]
                newlen[-1].selfvalue[4] = cfg["results"][idx]["min_crit_stress_approximation"]
                newlen[-1].selfvalue[5] = cfg["results"][idx]["min_natural_freq_approximation"]
                newlen[-1].selfvalue[6] = cfg["results"][idx]["min_crit_stress_relative_error"]
                newlen[-1].selfvalue[7] = cfg["results"][idx]["min_natural_freq_relative_error"]
                composite1[n] = newlen[-1].selfvalue[2]
                composite2[n] = newlen[-1].selfvalue[3]
                composite3[n] = newlen[-1].selfvalue[4]
                composite4[n] = newlen[-1].selfvalue[5]
                composite5[n] = newlen[-1].selfvalue[6]
                composite6[n] = newlen[-1].selfvalue[7]
                newlen[-1].coord.append([])
                for i in range(self.lines):
                    newlen[-1].coord[-1].append(Coord3D(cfg["results"][idx]["min_crit_force_mode_shapes"][0+i*4],
                                                        cfg["results"][idx]["min_crit_force_mode_shapes"][1+i*4],
                                                        cfg["results"][idx]["min_crit_force_mode_shapes"][2+i*4],
                                                        cfg["results"][idx]["min_crit_force_mode_shapes"][3+i*4]))
                newlen[-1].coord.append([])
                for i in range(self.lines):
                    newlen[-1].coord[-1].append(Coord3D(cfg["results"][idx]["min_natural_freq_mode_shapes"][0+i*4],
                                                        cfg["results"][idx]["min_natural_freq_mode_shapes"][1+i*4],
                                                        cfg["results"][idx]["min_natural_freq_mode_shapes"][2+i*4],
                                                        cfg["results"][idx]["min_natural_freq_mode_shapes"][3+i*4]))
                idx += 1
#            print "\n\nC2:",l,min(composite2),composite2
            cmin = min(composite1)
            self.composite[0][l] = cmin              #Critical stress across all modes
            index = composite1.index(cmin)
            self.composite[1][l] = composite2[index] #Natural frequency across all modes
            self.composite[2][l] = composite3[index] #Approximation critical stress across all modes
            self.composite[3][l] = composite4[index] #Approximation natural frequency across all modes
            self.composite[4][l] = composite5[index] #Critical stress relative error across all modes
            self.composite[5][l] = composite6[index] #Natural frequency relative error across all modes
        stdout.write("\n")

class FStripData(object):
    def __init__(self,l1=-1,l2=-1):
        self.line = [l1,l2]
        self.show = True
        self.flip = False
        self.showVal = True

class FSelfData(object):
    def __init__(self):
        self.coord = []            #Coord3D    [selfvalue][lines]
        self.selfvalue = [0]*8
        self.nodal_line = 0
        self.varname = ""

def floatText(number):
    if ((abs(number) < 100000) and (abs(number) >= 0.01)) or (number == 0): return str(round(number,2))
    else: return "%.2e" % number

varDict = VarDict()
fdata = FFilesData()
form = None

