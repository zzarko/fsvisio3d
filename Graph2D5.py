#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTab import *
from FileData import *
from FSVisioGraph import *

import numpy as np
from math import sqrt

def frange(start, stop, step):
     i = start
     while i < stop:
         yield i
         i += step

class Graph2D5(FSVisioTab):
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(Graph2D5, self).__init__(title, description, filePrefix, parent)
        self.varNames = ["Min critical force of mode","Min natural frequency of mode",
                         "Min normalized critical stress of mode","Min normalized natural frequency of mode",
                         "Min approximation critical stress of mode","Min natural frequency approximation",
                         "Critical stress relative error of mode","Natural frequency relative error of mode",
                         "Critical stress across all modes", "Natural frequency across all modes",
                         "Approximation critical stress across all modes", "Approximation natural frequency across all modes",
                         "Critical stress relative error across all modes","Natural frequency relative error across all modes",
                         "Time of retardation","Effective stress across all modes", "Damage variable across all modes",
                         "Creep coeficient"]
        self.varUnits = ["MPa","rad/s","MPa","rad/s","MPa","rad/s","%","%","MPa","rad/s","MPa","rad/s","%","%","s","MPa","",""]
#        self.varNames = Vars.varForcesList + Vars.varCoordsList + Vars.varUserList + Vars.varSpecialList

        self.graph = Graph(self.setStatusBarXY)
        self.contentLayout.addWidget(self.graph)
        self.markers = []

        self.diagnames = ["Ksi","D"]
        self.addRadioControl("whichone", "Graph for", self.diagnames, colrows=2, byColumns=False)
        self.addLabelSlider("lengthn", "Length", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthnChanged)
        self.addLabelDoubleSpinBox("phi","Phi", 0.1, 100, 0.01, 2)
        self.addLabelDoubleSpinBox("wmin","Omega min", 0.1, 100, 0.01, 2)
        self.addLabelDoubleSpinBox("wmax","Omega max", 0.1, 100, 0.01, 2)

        self.addSeparator()
        self.addGraphControls(callback=self.clearMarkers)
        #zasivljenje:
        self.graphSmoothLayout.checkbox.setEnabled(False)
        self.graphColorLinLagVkLayout.checkbox.setVisible(False)
        self.graphSmoothLayout.checkbox.setEnabled(False)
#        self.graphMarkerLayout.setEnabled(False)

        self.addButton("export","Export graph",self.exportGraph)
        self.syncTab = True
        self.graphSymbolSize = 12
        self.graphNumberSize = 14
        self.graphTitleSize = 18

        self.file1 = fdata.files[0]
        self.file2 = fdata.files[1]
        self.init()


    def lengthnChanged(self, value):
        if value == -1: return
        if not hasattr(self, 'lengthnLayout'): return
        length = self.file1.length[value]
        self.lengthnLayout.label.setText(self.lengthnLayout.labelText+": "+str(length)+"mm")
        self.update()

    def addMarker(self):
        if self.marksecond and self.secondg:
            var = self.var2
            pfile = fdata.files[self.file2]
        else:
            var = self.var1
            pfile = fdata.files[self.file1]
        nterm = self.nterm1 - 1
        try:
            index = pfile.length.index(self.markat)
        except:
            return
        if var < 8:
            y = pfile.data[index][nterm].selfvalue[var]
        else:
            y = pfile.composite[var-8][index]
        if abs(y) >= 0.01:
            name = "%0.1f mm: %0.2f %s" % (self.markat,y,self.varUnits[var])
        else:
            name = "%0.1f mm: %0.2E %s" % (self.markat,y,self.varUnits[var])
        if self.markname != "": name = self.markname + ": " + name
        self.markers.append([self.markat, y, name, self.markdata[self.marksym], self.marksecond])
        self.update()

    def init(self):
        self.lengthnSetMinMax(0,self.file1.lengths-1)
        self.lengthn = 0
        self.lengthnChanged(0)
        self.phi = 2
        w = self.file1.composite[1][self.lengthn]  # freq elasticno
        wd = self.file2.composite[1][self.lengthn] # freq visko
        
        wmin = min(self.file1.composite[1])
        wmax = max(self.file1.composite[1])*100
        #self.wminSetMinMaxStepDec(wmin,wmax,0.1,2)
        #self.wmaxSetMinMaxStepDec(wmin,wmax,0.1,2)
        self.wminSetMinMaxStepDec(1,wmax,0.1,2)
        self.wmaxSetMinMaxStepDec(1,wmax,0.1,2)
        self.wmin = wmin
        self.wmax = wmax

    def exportGraph(self):
        title = self.filePrefix + self.diagnames[self.whichone]
        self.graph.export("./export/",title)

    def clearMarkers(self):
        markers2 = [x for x in self.markers if x[0] != self.markat]
        if len(markers2) == len(self.markers):
            self.markers = []
        else:
            self.markers = markers2
        self.plot()

    def plot(self):
        self.setGraphAttributes(self.graph)
        self.graph.resetGraph()
        self.graph.titleX = "Delta*"
#        self.graph.titleY = self.varNames[self.var1]   # SLIKA 1 i 2

        length = self.file1.length[self.lengthn]
        w = self.file1.composite[1][self.lengthn]  # freq elasticno
        wd = self.file2.composite[1][self.lengthn] # freq visko

        N = 800
        phi = 2 # self.phi
        step = (self.wmax-self.wmin)/(N+1)
        wq = np.linspace(self.wmin,self.wmax,N) #omegaQ
        wq = np.logspace(0.1,5,N) #omegaQ
        x = [0.0]*N
        y = [0.0]*N
        if self.whichone == 0:  # ksi
            for i in range(N):
                delta = wq[i]/w
                deltas = wq[i]/wd
                x[i] = deltas
                y[i] = delta*phi*(1-deltas*deltas)/(2*deltas*(1+delta*delta+phi))
            name = "Ksi"
        if self.whichone == 1:  # Dq
            for i in range(N):
                delta = wq[i]/w
                deltas = wq[i]/wd
                ksi = delta*phi*(1-deltas*deltas)/(2*deltas*(1+delta*delta+phi))
                s1 = (1-deltas*deltas)
                s2 = (2*ksi*deltas)
                x[i] = deltas
                y[i] = 1/sqrt(s1*s1+s2*s2)
            name = "Dq"
        else:
            pass
        self.graph.addCurve(x,y,name)
        #self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)
        self.graph.canvas.axes.autoscale_view(True,True,True)
        self.graph.canvas.axes.set_xscale('log')
        self.graph.plotGraph()
        return

        symbols = self.secondg
        pfile = fdata.files[self.file1]

        # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
        pfile1 = fdata.files[0]
        pfile2 = fdata.files[1]

        nterm = self.nterm1 - 1
        var = self.var1
        x = [0.0]*pfile.lengths
        y = [0.0]*pfile.lengths
        print "VAR:",var

#        a = pfile.length[length]
#        dam_kk = 48*24099000000*0.000003718
#        dam_mm = 0.493*10000*0.002815
#        dam_k = dam_kk/a*1000/a*1000/a*1000
#        dam_m = dam_mm*a/1000
#        dam_wd = pfile.data[length][nterm].selfvalue[3]
##        dam_wd = pfile.composite[1][length]
#        dam_rd = -dam_k+dam_wd*dam_wd*dam_m
#        dam_rdk = -dam_rd/dam_k

        showInLegend = self.secondg and (not self.twiny)
        print "showInLegend",showInLegend,self.secondg,self.twiny

        if var < 8:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.data[i][nterm].selfvalue[var]
        elif var < 14:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.composite[var-8][i]
        elif var == 14:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = 1/pfile.composite[1][i]
#                x[i] = pfile.length[i]
#                k = kk/x[i]*1000
#                m = mm*x[i]/1000
#                wd = pfile.composite[1][i]
#                rd = -k * wd*wd*m
#                d = -rd/k
#                y[i] = d
        # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
        elif var == 15:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                w = pfile1.composite[1][i]  # freq elasticno
                wd = pfile2.composite[1][i] # freq visko
                s = pfile2.composite[0][i]  # napon visko
                y[i] = s * (w/wd)**2
        elif var == 16:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                w = pfile1.composite[1][i]  # freq elasticno
                wd = pfile2.composite[1][i] # freq visko
                s = pfile2.composite[0][i]  # napon visko
                y[i] = 1-(wd/w)**2
        elif var == 17:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                w = pfile1.composite[1][i]  # freq elasticno
                wd = pfile2.composite[1][i] # freq visko
                s = pfile2.composite[0][i]  # napon visko
                D = 1-(wd/w)**2
                y[i] = D/(1-D)

        #procenti
        if var in [6,7,12,13]:
            self.graph.percent1 = True
        else:
            self.graph.percent1 = False

        name = self.varNames[var]
        if name[-4:] == "mode": name += " %s" % (nterm+1)
        if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
#        name = 'Elastic'   # SLIKA 1 i 2
        self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)

        if self.secondg:
            pfile = fdata.files[self.file2]
            nterm = self.nterm2 - 1
            var = self.var2
            x = [0.0]*pfile.lengths
            y = [0.0]*pfile.lengths
            if var < 8:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = pfile.data[i][nterm].selfvalue[var]
            elif var < 14:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = pfile.composite[var-8][i]
            elif var == 14:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = 1/pfile.composite[1][i]
            # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
            elif var == 15:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    w = pfile1.composite[1][i]  # freq elasticno
                    wd = pfile2.composite[1][i] # freq visko
                    s = pfile2.composite[0][i]  # napon visko
                    y[i] = s*w*w/wd/wd
            elif var == 16:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    w = pfile1.composite[1][i]  # freq elasticno
                    wd = pfile2.composite[1][i] # freq visko
                    s = pfile2.composite[0][i]  # napon visko
                    y[i] = 1-wd*wd/w/w


            name = self.varNames[var]
            if name[-4:] == "mode": name += " %s" % (nterm+1)
            if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
#            name = 'Viscoelastic'  # SLIKA 1 i 2
            #procenti
            if var in [6,7,12,13]:
                self.graph.percent2 = True
            else:
                self.graph.percent2 = False
            self.graph.addCurve(x,y,name,symbol=symbols,secondAxe=self.twiny,showInLegend=showInLegend)
        if ((self.offset1 != 0.0) or (self.scale1 != 1.0)) and (self.offset1range != None):
            #print "GOFFSET1",self.offset1,self.offset1range
            self.graph.canvas.axes.autoscale_view(True,True,False)
            top = (self.offset1range[1] - self.offset1range[0])*self.scale1 + self.offset1range[0] + self.offset1
            self.graph.canvas.axes.set_ylim(self.offset1range[0] + self.offset1, top)
        else:
            self.graph.canvas.axes.autoscale_view(True,True,True)
        if self.graph.canvas.axes2:
            if ((self.offset2 != 0.0) or (self.scale2 != 1.0)) and (self.offset2range != None):
                #print "GOFFSET2",self.offset2,self.offset2range
                self.graph.canvas.axes2.autoscale_view(True,True,False)
                top = (self.offset2range[1] - self.offset2range[0])*self.scale2 + self.offset2range[0] + self.offset2
                self.graph.canvas.axes2.set_ylim(self.offset2range[0] + self.offset2, top)
            else:
                self.graph.canvas.axes2.autoscale_view(True,True,True)

        #dodavanje markera
        for m in self.markers:
            col = 0
            color=(col*0.8+0.1,0.9-col*0.8,0.1,1)
            self.graph.addMarker(m[0],m[1],color,m[2],m[3],m[4])

        #dodavanje suptitle-a
        self.graph.canvas.figure.suptitle(self.suptitle,fontsize=self.graphTitleSize)

        self.graph.plotGraph()
        return

