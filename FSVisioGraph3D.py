#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

#import PyQt4.Qwt5 as Qwt
import PyQt4.QtSvg as QtSvg
from FileData import floatText

#MPL
#def floatText(number):
#    if ((abs(number) < 100000) and (abs(number) >= 0.01)) or (number == 0): return str(round(number,2))
#    else: return "%.2e" % number

from math import isnan

#MPL
import numpy as np
from scipy.interpolate import pchip,interp1d

#MPL
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.backend_bases import KeyEvent
from matplotlib.lines import Line2D
from mpl_toolkits.mplot3d import Axes3D

#MPL
class MplCanvas3D(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=1, height=1, dpi=100):
        self.figure = Figure(figsize=(width, height), dpi=dpi)
        #self.figure.subplots_adjust(top=0.95,right = 0.80)
        #self.figure.subplots_adjust(left=0.0, right=0.8, bottom=0.1, top=0.9)
        self.figure.subplots_adjust(left=0.0, right=1.0, bottom=0.04, top=0.96)
        self.figure.patch.set_facecolor('w')
        self.axes = self.figure.add_subplot(1,1,1,projection='3d')
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        FigureCanvas.__init__(self, self.figure)
        self.setParent(parent)
        self.setFocusPolicy(Qt.StrongFocus)

        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

#MPL
class Graph3D(QWidget):
    plotSymbols1 = ['^','o','s','v','D','<','d','>','h','p','H']
    plotSymbols2 = ['+','x','1','2','3','4',]
    plotSymbolColors = ['0.5', '1.0', '0.0']
    plotColors = ['r','b','g','k','m','y','c']
    #red, green, blue, black, magenta, yellow, darkRed, darkGreen, darkBlue, darkCyan, darkMagenta, darkYellow, cyan, gray, darkGray, lightGray
    globalMinX = 0
    globalMinY = 0
    globalMaxX = 0
    globalMaxY = 0
    exportVector = True

    def __init__(self, showCallback=None, parent=None):
        super(Graph3D, self).__init__(parent)
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.canvas = MplCanvas3D(self,width=1, height=1, dpi=100)
        self.layout.addWidget(self.canvas)
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        self.layout.addWidget(self.mpl_toolbar)
#        self.canvas.mpl_connect('key_press_event', self.on_key_press)
        self.canvasbox = self.canvas.axes.get_position()
#        self.canvas.mpl_connect('pick_event', self.on_pick)
#        self.canvas.mpl_connect('motion_notify_event', self.on_mouse_move)
#        self.canvas.mpl_connect('button_release_event', self.on_mouse_release)

        self.legend = None
#        self.legendLocation = None
        self.legendBox = None

        self.setMinimumHeight(100)
        self.setMinimumWidth(250)

        self.curves = []
        self.markers = []
        self.marker = 0
        self.alignLR = 1
        self.showGrid = True
        self.showLegend = False
        self.showSymbols = True
        self.smooth = False
        self.invertY = False
        self.flipAxes = False
        self.secondAxe = None
        self.titleX = ""
        self.titleY = ""
        self.titleZ = ""
        self.svgSize = 1024
        self.pngSize = 1024
        self.colorLinLagVk = False
        self.color = False

        self.curvePenSize = 2
        self.symbolSize = 10
        self.axisFontSize = 10
        self.titleFontSize = 10
        self.graphWidth = 100
        self.graphHeight = 100
        self.graphLegendScale = 100
        self.graphLegendColumns = 3
        self.angle = 0

        self.minX = self.maxX = 0
        self.minY = self.maxY = 0

        self.azim = 75.0
        self.elev = 10.0
        self.canvas.axes.view_init(elev=self.elev, azim=self.azim)

        self.showCallback = showCallback if showCallback != None else self.dummyCallback
#        self.connect(Spy(self.canvas()), SIGNAL("MouseMove"), self.showCoordinates)
#        self.connect(Spy(self.canvas()), SIGNAL("MouseDblClick"), self.mouseClick)

#        self.zoomer = Qwt.QwtPlotZoomer(Qwt.QwtPlot.xBottom,
#                                        Qwt.QwtPlot.yLeft,
#                                        Qwt.QwtPicker.DragSelection,
#                                        Qwt.QwtPicker.AlwaysOff,
#                                        self.canvas())
#        self.zoomer.setRubberBandPen(QPen(Qt.black))

#    def on_key_press(self, event):
#        print('you pressed', event.key)
#        # implement the default mpl key press events described at
#        # http://matplotlib.org/users/navigation_toolbar.html#navigation-keyboard-shortcuts
#        key_press_handler(event, self.canvas, self.mpl_toolbar)

#    def on_pick(self,event):
#        if self.showLegend and (event.artist == self.legend) and (event.mouseevent.button == 1):
#            self.legendLocation = (event.mouseevent.x, event.mouseevent.y)
#            print "LEGEND", self.legendLocation

#    def on_mouse_move(self, event):
#        if self.legendLocation != None:
#            height = float(self.canvas.figure.bbox.height)
#            width = float(self.canvas.figure.bbox.width)
#            dx = (event.x - self.legendLocation[0])/width
#            dy = (event.y - self.legendLocation[1])/height
#            location = self.legend.location
#            print location


#    def on_mouse_release(self, event):
#        self.legendLocation = None

#- On a FigureCanvas, use mpl_connect to hook into the
#'motion_notify_event'.  So, we have
#"self.mpl_connect('motion_notify_event', self._onMouseMove)"
#- onMouseMove checks to see if we've previously picked the legend and
#adds mplEvent.x and mplEvent.y to the original pick location and calls
#our self._moveLegend function
#- The _moveLegend routine is:

#   def _moveLegend(self, x, y, autoDraw=True):

#       height = float(self.figure.bbox.height())
#       width = float(self.figure.bbox.width())

#       dx = x/width
#       dy = y/height

#       self._legendLocation = (dx, dy)
#       self.plot.getLegend().setLocation(self._legendLocation)

#       self._legend._loc=(dx,dy)

#       if autoDraw:
#           self.draw()

#Now, you won't need most of this, but the important things here are
#the dx,dy calculations and the _legend._loc=(dx,dy) assignment.

    def getSymbol(self, n):
        l1 = len(Graph.plotSymbols1)
        l2 = len(Graph.plotSymbols2)
        l3 = len(Graph.plotSymbolColors)
        if n >= l3*l1 + l2:
            n = n % (l3*l1 + l2)
        if n < l3*l1:
            return (Graph.plotSymbols1[n % l1], Graph.plotSymbolColors[n / l1])
        else:
            return (Graph.plotSymbols2[n - l3*l1], Graph.plotSymbolColors[0])

    def clear(self):
#        self.canvas.axes.clear()
        self.canvas.figure.clf()
        self.canvas.figure.subplots_adjust(left=0.03, right=0.96, bottom=0.04, top=0.96)
        self.canvas.figure.patch.set_facecolor('w')
        self.canvas.axes = self.canvas.figure.add_subplot(1,1,1,projection='3d')

    def resetGraph(self):
        if self.legend != None:
            self.legendBox = self.legend.get_bbox_to_anchor()
#            print self.legendBox
        else:
            self.legendBox = None
        self.canvas.axes.cla()
        self.canvas.axes.mouse_init()

        self.colorCounter = 0
        self.symbolCounter = 0
        self.symbolCounterLag = 0
        self.symbolCounterLin = 0
        self.symbolCounterVk = 0
        self.curves=[]
        self.noLegend=[]
        self.showLegend = False
        labelX = self.titleX
        labelY = self.titleY
        if self.flipAxes:
            labelX, labelY = labelY, labelX
        self.canvas.axes.set_xlabel(labelX, fontsize=self.titleFontSize)
        self.canvas.axes.set_ylabel(labelY, fontsize=self.titleFontSize)
        if self.titleZ != "":
            self.canvas.axes.set_zlabel(self.titleZ, fontsize=self.titleFontSize)
        for label in self.canvas.axes.get_xticklabels():
            label.set_rotation(self.angle)
            label.set_fontsize(self.axisFontSize)
        for label in self.canvas.axes.get_yticklabels():
            label.set_rotation(self.angle)
            label.set_fontsize(self.axisFontSize)
        for label in self.canvas.axes.get_zticklabels():
            label.set_rotation(self.angle)
            label.set_fontsize(self.axisFontSize)
        self.canvas.axes.view_init(elev=self.elev, azim=self.azim)

    def addCurve(self, x, y, name="", symbol=False, startMinMax=False, secondAxe=False, segments=[]):
        if self.curves != []:
            self.showLegend = True
            self.minX = min(min(x),self.minX)
            self.minY = min(min(y),self.minY)
            self.maxX = max(max(x),self.maxX)
            self.maxY = max(max(y),self.maxY)
        else:
            self.minX = min(x)
            self.minY = min(y)
            self.maxX = max(x)
            self.maxY = max(y)
        if startMinMax:
            Graph.globalMinX = self.minX
            Graph.globalMinY = self.minY
            Graph.globalMaxX = self.maxX
            Graph.globalMaxY = self.maxY
        else:
            Graph.globalMinX = min(self.minX,Graph.globalMinX)
            Graph.globalMinY = min(self.minY,Graph.globalMinY)
            Graph.globalMaxX = max(self.maxX,Graph.globalMaxX)
            Graph.globalMaxY = max(self.maxY,Graph.globalMaxY)
        if segments == []:
            segments = [Qt.SolidLine]*(len(x)-1)
        segStart = 0

        if self.color:
            curveColor = Graph.plotColors[self.colorCounter]
            self.colorCounter += 1
            if self.colorCounter >= len(Graph.plotColors): self.colorCounter = 0
        else:
            curveColor = 'k'

        while segStart < len(segments):
            segEnd = segStart
            while (segEnd < len(segments)-1) and (segments[segStart] == segments[segEnd+1]): segEnd += 1

            datax = []
            datay = []
            for x1,y1 in zip(x[segStart:segEnd+2],y[segStart:segEnd+2]):
                if (not isnan(x1)) and (not isnan(y1)):
                    datax.append(x1)
                    datay.append(y1)

            if self.flipAxes:
                datax, datay = datay, datax

            if self.invertY:
                datay[:] = [x*-1 for x in datay]

            if symbol and self.showSymbols:
                if self.colorLinLagVk:
                    if name.count("LIN") > 0:
                        sym, col = self.getSymbol(self.symbolCounterLin)
                    elif name.count("LAG") > 0:
                        sym, col = self.getSymbol(self.symbolCounterLag)
                    elif name.count("VK") > 0:
                        sym, col = self.getSymbol(self.symbolCounterVk)
                    else:
                        sym, col = self.getSymbol(self.symbolCounter)
                else:
                    sym, col = self.getSymbol(self.symbolCounter)
            else:
                sym = col = None

            if self.colorLinLagVk:
                if name.count("LIN") > 0:
                    penColor = 'r'
                elif name.count("LAG") > 0:
                    penColor = 'g'
                elif name.count("VK") > 0:
                    penColor = 'b'
                else:
                    penColor = 'k'
            else:
                penColor = curveColor

            if segments[segStart] == Qt.CustomDashLine:
                penStyle = "dashed"
            else:
                penStyle = "solid"


            axes = self.canvas.axes

            markevery=len(datax) / 10

            axes.set_ylabel(name, color=penColor)
            axes.tick_params("y", colors=penColor)

            if segStart == 0 and (name != ""):
                curve = Line2D(datax, datay, label=name, color = penColor, linewidth = self.curvePenSize, linestyle=penStyle,
                               markeredgecolor = penColor, markerfacecolor = col, marker = sym, markersize = self.symbolSize,
                               markevery = markevery)
                #self.canvas.axes.add_line(curve)
                axes.add_line(curve)
                self.curves.append(curve)
            else:
                curve = Line2D(datax, datay, label="", color = penColor, linewidth = self.curvePenSize, linestyle=penStyle,
                               markeredgecolor = penColor, markerfacecolor = col, marker = sym, markersize = self.symbolSize,
                               markevery = markevery)
                #self.canvas.axes.add_line(curve)
                axes.add_line(curve)
                self.noLegend.append(curve)

            #TODO: treba još dosta dorade...
            if self.smooth:
                x = np.array(datax)
                y = np.array(datay)
                interp = interp1d(x, y,bounds_error=False,fill_value=0.,kind='cubic')
                # Dense x for the smooth curve.
                xx = np.linspace(min(datax), max(datax), 101)
                # Plot it all.
                curve, = self.canvas.axes.plot(xx, interp(xx))
                self.noLegend.append(curve)

            segStart = segEnd + 1

        if symbol:
            if self.colorLinLagVk:
                if name.count("LIN") > 0:
                    self.symbolCounterLin += 1
                elif name.count("LAG") > 0:
                    self.symbolCounterLag += 1
                elif name.count("VK") > 0:
                    self.symbolCounterVk += 1
                else:
                    self.symbolCounter += 1
            else:
                self.symbolCounter += 1
#            if self.symbolCounter >= len(GraphOld.plotSymbols): self.symbolCounter = 0

    def plotGraph(self, scaleX=False, scaleY=False, scaleGlobal=False):
        self.canvas.axes.tick_params(which='both',axis='both',direction='out')
        self.canvas.axes.grid(self.showGrid)
#        self.canvas.axes.autoscale_view(True,True,True)
        lines, labels = self.canvas.axes.get_legend_handles_labels()
        lines2 = labels2 = []
        for curve in self.curves:
            curve.linewidth = self.curvePenSize
        for curve in self.noLegend:
            curve.linewidth = self.curvePenSize
        width = self.graphWidth/100.0
        height = self.graphHeight/100.0
        if self.showLegend:
#            if self.legendBox != None:
#                legendBox = self.legendBox
#                self.legendBox = None
#            else:
#                legendBox = (0.5, -0.1)
#            legendBox = (0.5, -0.1)
#            width -= 0.00
#            height -= 0.05
#            self.legend = self.canvas.axes.legend(loc='upper center', bbox_to_anchor=legendBox, ncol=self.graphLegendColumns,
#                    prop={'size':10.0*self.graphLegendScale/100}, markerscale=0.8*self.graphLegendScale/100) #, shadow=True, fancybox=True, mode="expand"
            self.legend = self.canvas.axes.legend(lines + lines2, labels + labels2, loc='best', ncol=self.graphLegendColumns, prop={'size':10.0*self.graphLegendScale/100},
                          markerscale=0.8*self.graphLegendScale/100) #, shadow=True, fancybox=True, mode="expand"
            self.legend.draggable(True)
#        else:
#            self.legend = None
        self.canvas.axes.set_position([self.canvasbox.x0 + self.canvasbox.width * (1-width), self.canvasbox.y0 + self.canvasbox.height * (1-height),
                                       self.canvasbox.width * width, self.canvasbox.height * height])
#        if self.showLegend: self.canvas.axes.legend(loc="best", ncol=3) #, mode="expand"
#        self.figure.tight_layout()
        self.canvas.draw()


    def export(self, path, name):
        self.canvas.figure.savefig(str(path+"/svg/"+name+".svg"), transparent = True)
        self.canvas.figure.savefig(str(path+"/ps/"+name+".ps"), transparent = True)
        self.canvas.figure.savefig(str(path+"/png/"+name+".png"), transparent = True, dpi=300)

    def dummyCallback(self,text):
        pass

    def showCoordinates(self, position):
        pass

    def addMarker(self, x, y, t, a):
        pass

    def mouseClick(self, position):
        pass

    def leaveGraph(self):
        pass

