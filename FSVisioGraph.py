#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

#import PyQt4.Qwt5 as Qwt
import PyQt4.QtSvg as QtSvg
from FileData import floatText

#MPL
#def floatText(number):
#    if ((abs(number) < 100000) and (abs(number) >= 0.01)) or (number == 0): return str(round(number,2))
#    else: return "%.2e" % number

from math import isnan

#MPL
import numpy as np
from scipy.interpolate import pchip,interp1d

#MPL
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.backend_bases import KeyEvent
from matplotlib.lines import Line2D

from matplotlib import ticker
def percentlab(x,p):
    return "%0.1f%%" % (x*100)

#MPL
class MplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=1, height=1, dpi=100):
        self.figure = Figure(figsize=(width, height), dpi=dpi)
        #self.figure.subplots_adjust(top=0.95,right = 0.80)
        self.figure.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)
        self.figure.patch.set_facecolor('w')
        self.axes = self.figure.add_subplot(1,1,1)
        self.axes2 = None
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        FigureCanvas.__init__(self, self.figure)
        self.setParent(parent)
        self.setFocusPolicy(Qt.StrongFocus)

        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

#MPL
class Graph(QWidget):
    plotSymbols1 = ['^','o','s','v','D','<','d','>','h','p','H']
    plotSymbols2 = ['+','x','1','2','3','4',]
    plotSymbolColors = ['0.5', '1.0', '0.0']
    plotColors = ['r','b','g','k','m','y','c']
    #red, green, blue, black, magenta, yellow, darkRed, darkGreen, darkBlue, darkCyan, darkMagenta, darkYellow, cyan, gray, darkGray, lightGray
    globalMinX = 0
    globalMinY = 0
    globalMaxX = 0
    globalMaxY = 0
    exportVector = True

    def __init__(self, showCallback=None, parent=None):
        super(Graph, self).__init__(parent)
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.canvas = MplCanvas(self,width=1, height=1, dpi=100)
        self.layout.addWidget(self.canvas)
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        self.layout.addWidget(self.mpl_toolbar)
#        self.canvas.mpl_connect('key_press_event', self.on_key_press)
        self.canvasbox = self.canvas.axes.get_position()
#        self.canvas.mpl_connect('pick_event', self.on_pick)
#        self.canvas.mpl_connect('motion_notify_event', self.on_mouse_move)
#        self.canvas.mpl_connect('button_release_event', self.on_mouse_release)

        self.legend = None
#        self.legendLocation = None
        self.legendBox = None

        self.setMinimumHeight(100)
        self.setMinimumWidth(250)

        self.curves = []
        self.markers = []
        self.marker = 0
        self.alignLR = 1
        self.showGrid = True
        self.showLegend = False
        self.showSymbols = True
        self.smooth = False
        self.invertY = False
        self.flipAxes = False
        self.secondAxe = None
        self.titleX = ""
        self.titleY = ""
        self.titleY2 = ""
        self.svgSize = 1024
        self.pngSize = 1024
        self.colorLinLagVk = False
        self.color = False

        self.curvePenSize = 2
        self.symbolSize = 10
        self.axisFontSize = 10
        self.titleFontSize = 10
        self.graphWidth = 100
        self.graphHeight = 100
        self.graphLegendScale = 100
        self.graphLegendColumns = 3
        self.angle = 0

        self.minX = self.maxX = 0
        self.minY = self.maxY = 0

        self.percent1 = False
        self.percent2 = False

        self.logX = False
        self.logY = False

        self.showCallback = showCallback if showCallback != None else self.dummyCallback
#        self.connect(Spy(self.canvas()), SIGNAL("MouseMove"), self.showCoordinates)
#        self.connect(Spy(self.canvas()), SIGNAL("MouseDblClick"), self.mouseClick)

#        self.zoomer = Qwt.QwtPlotZoomer(Qwt.QwtPlot.xBottom,
#                                        Qwt.QwtPlot.yLeft,
#                                        Qwt.QwtPicker.DragSelection,
#                                        Qwt.QwtPicker.AlwaysOff,
#                                        self.canvas())
#        self.zoomer.setRubberBandPen(QPen(Qt.black))

#    def on_key_press(self, event):
#        print('you pressed', event.key)
#        # implement the default mpl key press events described at
#        # http://matplotlib.org/users/navigation_toolbar.html#navigation-keyboard-shortcuts
#        key_press_handler(event, self.canvas, self.mpl_toolbar)

#    def on_pick(self,event):
#        if self.showLegend and (event.artist == self.legend) and (event.mouseevent.button == 1):
#            self.legendLocation = (event.mouseevent.x, event.mouseevent.y)
#            print "LEGEND", self.legendLocation

#    def on_mouse_move(self, event):
#        if self.legendLocation != None:
#            height = float(self.canvas.figure.bbox.height)
#            width = float(self.canvas.figure.bbox.width)
#            dx = (event.x - self.legendLocation[0])/width
#            dy = (event.y - self.legendLocation[1])/height
#            location = self.legend.location
#            print location


#    def on_mouse_release(self, event):
#        self.legendLocation = None

#- On a FigureCanvas, use mpl_connect to hook into the
#'motion_notify_event'.  So, we have
#"self.mpl_connect('motion_notify_event', self._onMouseMove)"
#- onMouseMove checks to see if we've previously picked the legend and
#adds mplEvent.x and mplEvent.y to the original pick location and calls
#our self._moveLegend function
#- The _moveLegend routine is:

#   def _moveLegend(self, x, y, autoDraw=True):

#       height = float(self.figure.bbox.height())
#       width = float(self.figure.bbox.width())

#       dx = x/width
#       dy = y/height

#       self._legendLocation = (dx, dy)
#       self.plot.getLegend().setLocation(self._legendLocation)

#       self._legend._loc=(dx,dy)

#       if autoDraw:
#           self.draw()

#Now, you won't need most of this, but the important things here are
#the dx,dy calculations and the _legend._loc=(dx,dy) assignment.

    def clear(self):
#        self.canvas.axes.clear()
        self.canvas.figure.clf()
        self.canvas.figure.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)
        self.canvas.figure.patch.set_facecolor('w')
        self.canvas.axes = self.canvas.figure.add_subplot(1,1,1)
        self.axes2 = None

    def getSymbol(self, n):
        l1 = len(Graph.plotSymbols1)
        l2 = len(Graph.plotSymbols2)
        l3 = len(Graph.plotSymbolColors)
        if n >= l3*l1 + l2:
            n = n % (l3*l1 + l2)
        if n < l3*l1:
            return (Graph.plotSymbols1[n % l1], Graph.plotSymbolColors[n / l1])
        else:
            return (Graph.plotSymbols2[n - l3*l1], Graph.plotSymbolColors[0])

    def resetGraph(self):
        if self.legend != None:
            self.legendBox = self.legend.get_bbox_to_anchor()
#            print self.legendBox
        else:
            self.legendBox = None
        if self.canvas.axes2:
            self.canvas.axes2.cla()
            #self.canvas.axes2.get_yaxis().set_ticks([])
        self.canvas.axes.cla()

        self.colorCounter = 0
        self.symbolCounter = 0
        self.symbolCounterLag = 0
        self.symbolCounterLin = 0
        self.symbolCounterVk = 0
        self.curves=[]
        self.noLegend=[]
        self.showLegend = False
        labelX = self.titleX
        labelY = self.titleY
        labelY2 = self.titleY2
        labelY2 = "LALALA"
        if self.flipAxes:
            labelX, labelY = labelY, labelX
        self.canvas.axes.set_xlabel(labelX, fontsize=self.titleFontSize)
        self.canvas.axes.set_ylabel(labelY, fontsize=self.titleFontSize)
        for label in self.canvas.axes.get_xticklabels():
            label.set_rotation(self.angle)
            label.set_fontsize(self.axisFontSize)
        for label in self.canvas.axes.get_yticklabels():
            label.set_fontsize(self.axisFontSize)
        if self.canvas.axes2:
            #self.canvas.axes2.set_xlabel(labelX, fontsize=self.titleFontSize)
            self.canvas.axes2.set_ylabel(labelY2, fontsize=self.titleFontSize)
            for label in self.canvas.axes2.get_xticklabels():
                label.set_rotation(self.angle)
                label.set_fontsize(self.axisFontSize)
            for label in self.canvas.axes2.get_yticklabels():
                label.set_fontsize(self.axisFontSize)

    def addCurve(self, x, y, name="", symbol=False, startMinMax=False, secondAxe=False, segments=[], color=None, linewidth=None, showInLegend=False):
        #print "legend1",self.showLegend
        if self.curves != []:
            #if not secondAxe: self.showLegend = True   #XLEGEND
            #print "legend2",self.showLegend
            self.minX = min(min(x),self.minX)
            self.minY = min(min(y),self.minY)
            self.maxX = max(max(x),self.maxX)
            self.maxY = max(max(y),self.maxY)
        else:
            self.minX = min(x)
            self.minY = min(y)
            self.maxX = max(x)
            self.maxY = max(y)
        if startMinMax:
            Graph.globalMinX = self.minX
            Graph.globalMinY = self.minY
            Graph.globalMaxX = self.maxX
            Graph.globalMaxY = self.maxY
        else:
            Graph.globalMinX = min(self.minX,Graph.globalMinX)
            Graph.globalMinY = min(self.minY,Graph.globalMinY)
            Graph.globalMaxX = max(self.maxX,Graph.globalMaxX)
            Graph.globalMaxY = max(self.maxY,Graph.globalMaxY)
        if segments == []:
            segments = [Qt.SolidLine]*(len(x)-1)
        segStart = 0

        if color:
            curveColor = color
        elif self.color:
            curveColor = Graph.plotColors[self.colorCounter]
            self.colorCounter += 1
            if self.colorCounter >= len(Graph.plotColors): self.colorCounter = 0
        else:
            curveColor = 'k'

        while segStart < len(segments):
            segEnd = segStart
            while (segEnd < len(segments)-1) and (segments[segStart] == segments[segEnd+1]): segEnd += 1

            datax = []
            datay = []
            for x1,y1 in zip(x[segStart:segEnd+2],y[segStart:segEnd+2]):
                if (not isnan(x1)) and (not isnan(y1)):
                    datax.append(x1)
                    datay.append(y1)

            if self.flipAxes:
                datax, datay = datay, datax

            if self.invertY:
                datay[:] = [x*-1 for x in datay]

            if symbol and self.showSymbols:
                if self.colorLinLagVk:
                    if name.count("LIN") > 0:
                        sym, col = self.getSymbol(self.symbolCounterLin)
                    elif name.count("LAG") > 0:
                        sym, col = self.getSymbol(self.symbolCounterLag)
                    elif name.count("VK") > 0:
                        sym, col = self.getSymbol(self.symbolCounterVk)
                    else:
                        sym, col = self.getSymbol(self.symbolCounter)
                else:
                    sym, col = self.getSymbol(self.symbolCounter)
            else:
                sym = col = None

            if self.colorLinLagVk:
                if name.count("LIN") > 0:
                    penColor = 'r'
                elif name.count("LAG") > 0:
                    penColor = 'g'
                elif name.count("VK") > 0:
                    penColor = 'b'
                else:
                    penColor = 'k'
            else:
                penColor = curveColor

            if segments[segStart] == Qt.CustomDashLine:
                penStyle = "dashed"
            else:
                penStyle = "solid"


            if secondAxe:
                if not self.canvas.axes2:
                    self.canvas.axes2 = self.canvas.axes.twinx()
                    self.canvas.axes2.hold(False)
                axes = self.canvas.axes2
                self.canvas.axes2.set_visible(True)
            else:
                axes = self.canvas.axes
                if self.canvas.axes2:
                    self.canvas.axes2.set_visible(False)

            markevery=len(datax) / 10

            if segStart == 0 and (name != "") and showInLegend:
                print "curve1",name
                curve = Line2D(datax, datay, label=name, color = penColor, linewidth = self.curvePenSize, linestyle=penStyle,
                               markeredgecolor = penColor, markerfacecolor = col, marker = sym, markersize = self.symbolSize,
                               markevery = markevery)
                #self.canvas.axes.add_line(curve)
                axes.add_line(curve)
                self.curves.append(curve)
            else:
                #print "curve2",name
                curve = Line2D(datax, datay, label=name, color = penColor, linewidth = self.curvePenSize, linestyle=penStyle,
                               markeredgecolor = penColor, markerfacecolor = col, marker = sym, markersize = self.symbolSize,
                               markevery = markevery)
                #self.canvas.axes.add_line(curve)
                axes.add_line(curve)
                self.noLegend.append(curve)

            #TODO: treba još dosta dorade...
            if self.smooth:
                x = np.array(datax)
                y = np.array(datay)
                interp = interp1d(x, y,bounds_error=False,fill_value=0.,kind='cubic')
                # Dense x for the smooth curve.
                xx = np.linspace(min(datax), max(datax), 101)
                # Plot it all.
                curve, = self.canvas.axes.plot(xx, interp(xx))
                self.noLegend.append(curve)

            segStart = segEnd + 1

        if not showInLegend:
            if secondAxe:
                axes.set_ylabel(name, color=penColor)
                axes.tick_params("y", colors=penColor)
            else:
                axes.set_ylabel(name, color=penColor)
                axes.tick_params("y", colors=penColor)
        else:
            if secondAxe:
                axes.set_ylabel(self.titleY2, color=penColor)
                axes.tick_params("y", colors=penColor)
            else:
                axes.set_ylabel(self.titleY, color=penColor)
                axes.tick_params("y", colors=penColor)

        if symbol:
            if self.colorLinLagVk:
                if name.count("LIN") > 0:
                    self.symbolCounterLin += 1
                elif name.count("LAG") > 0:
                    self.symbolCounterLag += 1
                elif name.count("VK") > 0:
                    self.symbolCounterVk += 1
                else:
                    self.symbolCounter += 1
            else:
                self.symbolCounter += 1
#            if self.symbolCounter >= len(GraphOld.plotSymbols): self.symbolCounter = 0
        return curveColor

    def plotGraph(self, scaleX=False, scaleY=False, scaleGlobal=False):
        self.canvas.axes.tick_params(which='both',axis='both',direction='out')
        self.canvas.axes.grid(self.showGrid)
#        self.canvas.axes.autoscale_view(True,True,True)
        lines, labels = self.canvas.axes.get_legend_handles_labels()
        lines2 = labels2 = []
        axes = self.canvas.axes
        if self.percent1:
            axes.get_yaxis().set_major_formatter(ticker.FuncFormatter(percentlab))
        elif self.logY:
            pass
            axes.get_yaxis().set_major_formatter(ticker.ScalarFormatter())
            axes.get_yaxis().set_minor_formatter(ticker.ScalarFormatter())
            axes.ticklabel_format(axis='y', style='plain')
        else:
            #axes.ticklabel_format(axis='y', style='sci', scilimits=(-4,4))
            axes.ticklabel_format(axis='y', style='plain')
        if self.canvas.axes2:
            axes = self.canvas.axes2
            if self.percent2:
                axes.get_yaxis().set_major_formatter(ticker.FuncFormatter(percentlab))
            else:
                #axes.ticklabel_format(axis='y', style='sci', scilimits=(-4,4))
                axes.ticklabel_format(axis='y', style='plain')
            self.canvas.axes2.tick_params(which='both',axis='both',direction='out')
            #self.canvas.axes2.grid(self.showGrid)
            self.canvas.axes2.autoscale_view(True,True,True)
            lines2, labels2 = self.canvas.axes2.get_legend_handles_labels()
        for curve in self.curves:
            curve.linewidth = self.curvePenSize
        for curve in self.noLegend:
            curve.linewidth = self.curvePenSize
            if curve in lines:
                index = lines.index(curve)
                lines.remove(curve)
                labels.remove(labels[index])
            if curve in lines2:
                index = lines2.index(curve)
                lines2.remove(curve)
                labels2.remove(labels2[index])
        width = self.graphWidth/100.0
        height = self.graphHeight/100.0
        if self.showLegend:
            print "legend3"
#            if self.legendBox != None:
#                legendBox = self.legendBox
#                self.legendBox = None
#            else:
#                legendBox = (0.5, -0.1)
#            legendBox = (0.5, -0.1)
#            width -= 0.00
#            height -= 0.05
#            self.legend = self.canvas.axes.legend(loc='upper center', bbox_to_anchor=legendBox, ncol=self.graphLegendColumns,
#                    prop={'size':10.0*self.graphLegendScale/100}, markerscale=0.8*self.graphLegendScale/100) #, shadow=True, fancybox=True, mode="expand"
            self.legend = axes.legend(lines + lines2, labels + labels2, loc='best', ncol=self.graphLegendColumns, numpoints=1, prop={'size':10.0*self.graphLegendScale/100},
                          markerscale=0.8*self.graphLegendScale/100) #, shadow=True, fancybox=True, mode="expand"
            self.legend.draggable(True)

#        else:
#            self.legend = None
        self.canvas.axes.set_position([self.canvasbox.x0 + self.canvasbox.width * (1-width)/2, self.canvasbox.y0 + self.canvasbox.height * (1-height)/2,
                                       self.canvasbox.width * width, self.canvasbox.height * height])
        if self.canvas.axes2:
            self.canvas.axes2.set_position([self.canvasbox.x0 + self.canvasbox.width * (1-width)/2, self.canvasbox.y0 + self.canvasbox.height * (1-height)/2,
                                           self.canvasbox.width * width, self.canvasbox.height * height])
#        if self.showLegend: self.canvas.axes.legend(loc="best", ncol=3) #, mode="expand"
        self.canvas.draw()


    def export(self, path, name):
        self.canvas.figure.savefig(str(path+"/svg/"+name+".svg"), transparent = True)
        self.canvas.figure.savefig(str(path+"/ps/"+name+".ps"), transparent = True)
        self.canvas.figure.savefig(str(path+"/png/"+name+".png"), transparent = True, dpi=300)

    def dummyCallback(self,text):
        pass

    def showCoordinates(self, position):
        pass

    def addMarker(self, x, y, color='k', name="", symbol='o', secondg=False, size=10):
        curve = Line2D([x], [y], label=name, linewidth = 0, markeredgecolor = 'k',
            markerfacecolor = color, marker = symbol, markersize = size)
        if secondg and self.canvas.axes2:
            axes = self.canvas.axes2
        else:
            axes = self.canvas.axes
        axes.add_line(curve)
        if name == "":
            self.noLegend.append(curve)
        else:
            self.showLegend = True
        print "Marker:",name,x,y,secondg

    def mouseClick(self, position):
        pass

    def leaveGraph(self):
        pass



###################################################################################

#class GraphOld(Qwt.QwtPlot):
#    """QwtPlot with additional methods"""
#    plotSymbols1 = [Qwt.QwtSymbol.Diamond, Qwt.QwtSymbol.Ellipse, Qwt.QwtSymbol.DTriangle, Qwt.QwtSymbol.Rect, Qwt.QwtSymbol.UTriangle, Qwt.QwtSymbol.Star2]
#    plotSymbols2 = [Qwt.QwtSymbol.Cross, Qwt.QwtSymbol.XCross, Qwt.QwtSymbol.Star1]
#    plotSymbolColors = [Qt.lightGray, Qt.white, Qt.black]
##    plotColors = [Qt.darkRed, Qt.darkGreen, Qt.darkBlue, Qt.darkCyan, Qt.darkMagenta, Qt.darkYellow, Qt.black, Qt.gray, Qt.green, Qt.red, Qt.blue, Qt.cyan, Qt.magenta, Qt.yellow, Qt.darkGray, Qt.lightGray]
#    plotColors = [Qt.red, Qt.green, Qt.blue, Qt.black, Qt.magenta, Qt.yellow, Qt.darkRed, Qt.darkGreen, Qt.darkBlue, Qt.darkCyan, Qt.darkMagenta, Qt.darkYellow, Qt.cyan, Qt.gray, Qt.darkGray, Qt.lightGray]
#    globalMinX = 0
#    globalMinY = 0
#    globalMaxX = 0
#    globalMaxY = 0
#    exportVector = True

#    def __init__(self, showCallback=None, parent=None):
#        super(Graph, self).__init__(parent)
#        self.grid = Qwt.QwtPlotGrid()
#        self.setCanvasBackground(Qt.white)
#        self.setMinimumHeight(150)
#        self.setMinimumWidth(200)
#        self.axisFont = self.axisFont(Qwt.QwtPlot.xBottom)
#        self.titleFont = self.title().font()
#        self.titleFont.setBold(True)
#        self.curves = []
#        self.markers = []
#        self.marker = 0
#        self.alignLR = 1
#        self.showGrid = True
#        self.showLegend = False
#        self.showSymbols = True
#        self.smooth = False
#        self.invertY = False
#        self.flipAxes = False
#        self.titleX = ""
#        self.titleY = ""
#        self.svgSize = 1024
#        self.pngSize = 1024
#        self.colorLinLagVk = False
#        self.color = False

#        self.curvePenSize = 2
#        self.symbolSize = 10
#        self.axisFontSize = 10
#        self.titleFontSize = 10
#        self.angle = 0

#        self.minX = self.maxX = 0
#        self.minY = self.maxY = 0

#        self.showCallback = showCallback if showCallback != None else self.dummyCallback
#        self.connect(Spy(self.canvas()), SIGNAL("MouseMove"), self.showCoordinates)
#        self.connect(Spy(self.canvas()), SIGNAL("MouseDblClick"), self.mouseClick)

#        self.zoomer = Qwt.QwtPlotZoomer(Qwt.QwtPlot.xBottom,
#                                        Qwt.QwtPlot.yLeft,
#                                        Qwt.QwtPicker.DragSelection,
#                                        Qwt.QwtPicker.AlwaysOff,
#                                        self.canvas())
#        self.zoomer.setRubberBandPen(QPen(Qt.black))

#    def getSymbol(self, n):
#        l1 = len(GraphOld.plotSymbols1)
#        l2 = len(GraphOld.plotSymbols2)
#        l3 = len(GraphOld.plotSymbolColors)
#        if n >= l3*l1 + l2:
#            n = n % (l3*l1 + l2)
#        if n < l3*l1:
#            return (GraphOld.plotSymbols1[n % l1], GraphOld.plotSymbolColors[n / l1])
#        else:
#            return (GraphOld.plotSymbols2[n - l3*l1], GraphOld.plotSymbolColors[0])

#    def resetGraph(self):
#        self.clear()
#        self.colorCounter = 0
#        self.symbolCounter = 0
#        self.symbolCounterLag = 0
#        self.symbolCounterLin = 0
#        self.symbolCounterVk = 0
#        self.curves=[]
#        self.noLegend=[]
#        self.showLegend = False
#        if self.showGrid:
#            self.grid.attach(self)
#            self.grid.setPen(QPen(Qt.gray, 1))
#        else:
#            self.grid.detach()
#        self.setAxisLabelRotation(Qwt.QwtPlot.xBottom, 360-self.angle)
#        self.axisScaleEngine(Qwt.QwtPlot.yLeft).setAttribute(Qwt.QwtScaleEngine.Inverted, self.invertY)
#        self.titleFont.setPointSize(self.titleFontSize)
#        qwtX = Qwt.QwtText(self.titleX)
#        qwtX.setFont(self.titleFont)
#        qwtY = Qwt.QwtText(self.titleY)
#        qwtY.setFont(self.titleFont)
#        if self.flipAxes:
#            self.setAxisTitle(Qwt.QwtPlot.yLeft, qwtX)
#            self.setAxisTitle(Qwt.QwtPlot.xBottom, qwtY)
#        else:
#            self.setAxisTitle(Qwt.QwtPlot.yLeft, qwtY)
#            self.setAxisTitle(Qwt.QwtPlot.xBottom, qwtX)
#        self.axisFont.setPointSize(self.axisFontSize)
#        self.setAxisFont(Qwt.QwtPlot.yLeft,self.axisFont)
#        self.setAxisFont(Qwt.QwtPlot.xBottom,self.axisFont)

#    def addCurve(self, x, y, name="", symbol=False, startMinMax=False, segments=[]):
#        if self.curves != []:
##            self.showLegend = True
#            self.minX = min(min(x),self.minX)
#            self.minY = min(min(y),self.minY)
#            self.maxX = max(max(x),self.maxX)
#            self.maxY = max(max(y),self.maxY)
#        else:
#            self.minX = min(x)
#            self.minY = min(y)
#            self.maxX = max(x)
#            self.maxY = max(y)
#        if startMinMax:
#            GraphOld.globalMinX = self.minX
#            GraphOld.globalMinY = self.minY
#            GraphOld.globalMaxX = self.maxX
#            GraphOld.globalMaxY = self.maxY
#        else:
#            GraphOld.globalMinX = min(self.minX,GraphOld.globalMinX)
#            GraphOld.globalMinY = min(self.minY,GraphOld.globalMinY)
#            GraphOld.globalMaxX = max(self.maxX,GraphOld.globalMaxX)
#            GraphOld.globalMaxY = max(self.maxY,GraphOld.globalMaxY)
#        if segments == []:
#            segments = [Qt.SolidLine]*(len(x)-1)
#        segStart = 0
#        if self.color:
#            curveColor = GraphOld.plotColors[self.colorCounter]
#            self.colorCounter += 1
#            if self.colorCounter >= len(GraphOld.plotColors): self.colorCounter = 0
#        else:
#            curveColor = Qt.black
#        while segStart < len(segments):
#            segEnd = segStart
#            while (segEnd < len(segments)-1) and (segments[segStart] == segments[segEnd+1]): segEnd += 1
#            if segStart == 0 and (name != ""):
#                curve = Qwt.QwtPlotCurve(name)
#                self.curves.append(curve)
#            else:
#                curve = Qwt.QwtPlotCurve()
#                self.noLegend.append(curve)
#            if self.smooth:
#                self.fitter = Qwt.QwtSplineCurveFitter()
#                self.fitter.setFitMode(Qwt.QwtSplineCurveFitter.ParametricSpline)
#                self.fitter.setSplineSize(100)
#                curve.setCurveFitter(self.fitter)
#                curve.setCurveAttribute(Qwt.QwtPlotCurve.Fitted, True)
#            curve.attach(self)
#            if self.colorLinLagVk:
#                if name.count("LIN") > 0:
#                    penColor = Qt.red
#                elif name.count("LAG") > 0:
#                    penColor = Qt.green
#                elif name.count("VK") > 0:
#                    penColor = Qt.blue
#                else:
#                    penColor = Qt.black
#            else:
#                penColor = curveColor
#            if symbol and self.showSymbols:
#                if self.colorLinLagVk:
#                    if name.count("LIN") > 0:
#                        sym, col = self.getSymbol(self.symbolCounterLin)
#                    elif name.count("LAG") > 0:
#                        sym, col = self.getSymbol(self.symbolCounterLag)
#                    elif name.count("VK") > 0:
#                        sym, col = self.getSymbol(self.symbolCounterVk)
#                    else:
#                        sym, col = self.getSymbol(self.symbolCounter)
#                else:
#                    sym, col = self.getSymbol(self.symbolCounter)
#                curve.setSymbol(Qwt.QwtSymbol(sym, QBrush(col), QPen(penColor), QSize(self.symbolSize,self.symbolSize)))
#            if segments[segStart] == Qt.CustomDashLine:
#                pen = QPen(penColor, self.curvePenSize)
#            else:
#                pen = QPen(penColor, self.curvePenSize*2)
#            if segments[segStart] == Qt.CustomDashLine:
#                pen.setDashPattern([10,10])
#            else:
#                pen.setStyle(segments[segStart])
#            curve.setPen(pen)
##                self.symbolCounter += 1
##                if self.symbolCounter >= len(GraphOld.plotSymbols): self.symbolCounter = 0
#            datax = []
#            datay = []
#            for x1,y1 in zip(x[segStart:segEnd+2],y[segStart:segEnd+2]):
#                if (not isnan(x1)) and (not isnan(y1)):
#                    datax.append(x1)
#                    datay.append(y1)
#            if self.flipAxes:
##                curve.setData(y,x)
#                curve.setData(datay,datax)
#            else:
##                curve.setData(x,y)
#                curve.setData(datax,datay)
#            segStart = segEnd + 1
#        if symbol:
#            if self.colorLinLagVk:
#                if name.count("LIN") > 0:
#                    self.symbolCounterLin += 1
#                elif name.count("LAG") > 0:
#                    self.symbolCounterLag += 1
#                elif name.count("VK") > 0:
#                    self.symbolCounterVk += 1
#                else:
#                    self.symbolCounter += 1
#            else:
#                self.symbolCounter += 1
##            if self.symbolCounter >= len(GraphOld.plotSymbols): self.symbolCounter = 0

#    def plotGraph(self, scaleX=False, scaleY=False, scaleGlobal=False):
#        if self.showLegend:
#            self.legend = Qwt.QwtLegend()
#            self.legend.setFont(self.titleFont)
#            self.legend.setFrameStyle(QFrame.Box | QFrame.Sunken)
#            self.legend.setDisplayPolicy(Qwt.QwtLegend.FixedIdentifier, Qwt.QwtLegendItem.ShowSymbol | Qwt.QwtLegendItem.ShowText | Qwt.QwtLegendItem.ShowLine)
#            #self.legend.setItemMode(Qwt.QwtLegend.ClickableItem)
#            self.insertLegend(self.legend, Qwt.QwtPlot.BottomLegend)
#            for curve in self.noLegend:
#                self.legend.remove(curve)
#        else:
#            self.insertLegend(None)
#        if self.flipAxes:
#            scX = scaleY
#            scY = scaleX
#            if scaleGlobal:
#                minX, minY, maxX, maxY = GraphOld.globalMinY, GraphOld.globalMinX, GraphOld.globalMaxY, GraphOld.globalMaxX
#            else:
#                minX, minY, maxX, maxY = self.minY, self.minX, self.maxY, self.maxX
#        else:
#            scX = scaleX
#            scY = scaleY
#            if scaleGlobal:
#                minX, minY, maxX, maxY = GraphOld.globalMinX, GraphOld.globalMinY, GraphOld.globalMaxX, GraphOld.globalMaxY
#            else:
#                minX, minY, maxX, maxY = self.minX, self.minY, self.maxX, self.maxY
#        if scX and (minX != maxX):
#            self.setAxisScale(Qwt.QwtPlot.xBottom,minX,maxX)
#        else:
#            self.setAxisAutoScale(Qwt.QwtPlot.xBottom)
#        if scY and (minY != maxY):
#            self.setAxisScale(Qwt.QwtPlot.yLeft,minY,maxY)
#        else:
#            self.setAxisAutoScale(Qwt.QwtPlot.yLeft)
#        for marker in self.markers: self.addMarker(*marker)
#        self.replot()
#        self.zoomer.setZoomBase()

#    def export(self, path, name):
##        self.setCanvasBackground(Qt.transparent)
#        if GraphOld.exportVector:
#            gen = QtSvg.QSvgGenerator()
#            gen.setSize(QSize(self.svgSize,self.svgSize))
#            gen.setFileName(path+"/svg/"+name+".svg")
#            self.print_(gen)
#            printer = QPrinter()
#            printer.setOutputFormat(QPrinter.PostScriptFormat)
#            printer.setOutputFileName(path+"/ps/"+name+".ps")
#            self.print_(printer)
#        image = QImage(QSize(self.pngSize, self.pngSize), QImage.Format_ARGB32)
#        self.print_(image)
#        image.save(path+"/png/"+name+".png", 'PNG')
#        self.setCanvasBackground(Qt.white)

#    def dummyCallback(self,text):
#        pass

#    def showCoordinates(self, position):
#        if position == None:
#            self.showCallback('')
#        else:
#            self.showCallback('x = %+.6g, y = %.6g'
#                % (self.invTransform(Qwt.QwtPlot.xBottom, position.x()),
#                   self.invTransform(Qwt.QwtPlot.yLeft, position.y())))

#    def addMarker(self, x, y, t, a):
#        marker = Qwt.QwtPlotMarker()
#        if a == 0:
#            alignLR = Qt.AlignLeft | Qt.AlignBottom
#        else:
#            alignLR = Qt.AlignRight | Qt.AlignTop
#        if t == 0:
#            markerType = Qwt.QwtPlotMarker.HLine
#            markerFlipType = Qwt.QwtPlotMarker.VLine
#            text = floatText(y)
#            textF = floatText(y)
#        elif t == 1:
#            markerType = Qwt.QwtPlotMarker.VLine
#            markerFlipType = Qwt.QwtPlotMarker.HLine
#            text = floatText(x)
#            textF = floatText(x)
#        else:
#            markerType = Qwt.QwtPlotMarker.NoLine
#            markerFlipType = Qwt.QwtPlotMarker.NoLine
#            text = "("+floatText(x)+","+floatText(y)+")"
#            textF = "("+floatText(y)+","+floatText(x)+")"
#        if self.flipAxes:
#            marker.setValue(y,x)
#            marker.setLineStyle(markerFlipType)
#            marker.setLabel( Qwt.QwtText(textF) )
#        else:
#            marker.setValue(x,y)
#            marker.setLineStyle(markerType)
#            marker.setLabel( Qwt.QwtText(text) )
#        marker.setLabelAlignment(alignLR)
#        marker.setLinePen(QPen(Qt.darkGray, 2, Qt.DashLine))
#        marker.attach(self)

#    def mouseClick(self, position):
#        x = self.invTransform(Qwt.QwtPlot.xBottom, position.x())
#        y = self.invTransform(Qwt.QwtPlot.yLeft, position.y())
#        marker = self.marker
#        if self.flipAxes:
#            if marker < 2: marker = 1 - marker
#            self.markers.append([y,x,marker,self.alignLR])
#            self.addMarker(y,x,marker,self.alignLR)
#        else:
#            self.markers.append([x,y,marker,self.alignLR])
#            self.addMarker(x,y,marker,self.alignLR)
#        self.replot()

#    def leaveGraph(self):
#        self.showCallback('')

##taken from BarPlotDemo.py (PyQwt)
#class Spy(QObject):
#    def __init__(self, parent):
#        QObject.__init__(self, parent)
#        parent.setMouseTracking(True)
#        parent.installEventFilter(self)

#    def eventFilter(self, _, event):
#        if event.type() == QEvent.MouseMove:
#            self.emit(SIGNAL("MouseMove"), event.pos())
#        elif event.type() == QEvent.Leave:
#            self.emit(SIGNAL("MouseMove"), None)
#        elif event.type() == QEvent.MouseButtonDblClick:
#            self.emit(SIGNAL("MouseDblClick"), event.pos())
#        return False

