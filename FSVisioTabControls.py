#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class QComboBox2(QComboBox):
    """ComboBox with two additional metods for retrieving integer data and setting selection to integer data"""
    def __init__(self, parent=None):
        super(QComboBox, self).__init__(parent)
        self.setMinimumContentsLength(20)
        self.setSizeAdjustPolicy(QComboBox.AdjustToMinimumContentsLength)

    def currentData(self):
        return self.itemData(self.currentIndex()).toInt()[0]

    def setCurrentData(self, value):
        index = self.findData(value)
        self.setCurrentIndex(index)

class SliderControl(QVBoxLayout):
    """Control with slider and a label with it's numeric value"""
    def __init__(self, label, minvalue=0, maxvalue=100, pageStep=45, singleStep=5, callback=None, parent=None):
        super(SliderControl, self).__init__(parent)
        #label with text
        self.labelText = label
        self.label = QLabel(label)
        self.addWidget(self.label)
        #slider
        self.slider = QSlider(Qt.Horizontal)
        self.addWidget(self.slider)
        self.setMinMax(minvalue, maxvalue)
        self.slider.setPageStep(pageStep)
        self.slider.setSingleStep(singleStep)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.callback = callback if callback != None else self.dummyCallback
        self.connect(self.slider, SIGNAL("valueChanged(int)"), self.sliderChanged)
        self.setSlider(minvalue+1)
        self.setSlider(minvalue)

    def dummyCallback(self, value):
        pass

    def setMinMax(self, minvalue, maxvalue):
        if (minvalue != None) and (maxvalue != None):
            self.slider.setRange(minvalue, maxvalue)
            self.minvalue = minvalue
            self.maxvalue = maxvalue
            self.range = maxvalue-minvalue

    def sliderChanged(self, value):
        self.label.setText(self.labelText+": "+str(value))
        self.callback(value)

    def setSlider(self, value):
        while value < self.minvalue: value += self.range
        while value > self.maxvalue: value -= self.range
        self.slider.setSliderPosition(value)

    def getSlider(self):
        return self.slider.sliderPosition()

class RangeControl(QVBoxLayout):
    """Control with two spinboxes and a label representing non-overlapping range of values"""
    def __init__(self, label, minvalue=0, maxvalue=100, callback=None, parent=None):
        super(RangeControl, self).__init__(parent)
        #label with text
        self.label = QLabel(label)
        self.addWidget(self.label)
        #layout for two labels and two spinboxes
        rlayout = QHBoxLayout()
        self.addLayout(rlayout)
        #first label with fixed text
        self.label1 = QLabel("from")
        rlayout.addWidget(self.label1, 0, Qt.AlignRight)
        #bottom part of a range
        self.range1box = QSpinBox()
        rlayout.addWidget(self.range1box, 1)
        self.connect(self.range1box, SIGNAL("valueChanged(int)"),self.range1Changed)
        #second label with fixed text
        self.label2 = QLabel("to")
        rlayout.addWidget(self.label2, 0, Qt.AlignRight)
        #top part of a range
        self.range2box = QSpinBox()
        rlayout.addWidget(self.range2box, 1)
        self.connect(self.range2box, SIGNAL("valueChanged(int)"),self.range2Changed)
        #callback always exists
        self.callback = callback if callback != None else self.dummyCallback
        if (minvalue != None) and (maxvalue != None):
            self.setMinMax(minvalue, maxvalue)

    def dummyCallback(self):
        pass

    def setMinMax(self, minvalue, maxvalue):
        """This function is used outside for setting min and max"""
        self.range1box.setRange(minvalue, maxvalue)
        #self.range1box.setValue(minvalue+1)    #this may be needed if there is no valueChanged signal with next line only
        self.range1box.setValue(minvalue)
        self.range2box.setRange(minvalue, maxvalue)
        self.range2box.setValue(maxvalue)
        self.callback()

    def range1Changed(self, value):
        if value == -1: return
        if self.range2box.value() <= value:
            self.range2box.setValue(value)
        self.callback()

    def range2Changed(self, value):
        if value == -1: return
        if self.range1box.value() >= value:
            self.range1box.setValue(value)
        self.callback()

    def getRange1(self):
        return self.range1box.value()

    def setRange1(self, value):
        self.range1box.setValue(value)
        self.callback()

    def getRange2(self):
        return self.range2box.value()

    def setRange2(self, value):
        self.range2box.setValue(value)
        self.callback()

class AngleControl(QVBoxLayout):
    """Control with dial and spinbox representing an angle"""
    def __init__(self, label, callback=None, parent=None):
        super(AngleControl, self).__init__(parent)
        self.angle = 0
        self.updateCounter = 0
        #label with text
        self.label = QLabel(label)
        self.addWidget(self.label)
        #layout for dial and spinbox
        alayout = QHBoxLayout()
        self.addLayout(alayout)
        #dial
        self.dial = QDial()
        alayout.addWidget(self.dial)
        self.dial.setRange(0,359)
        self.dial.setTracking(False)
        self.dial.setWrapping(True)
        self.dial.setSingleStep(5)
        self.dial.setPageStep(45)
        self.dial.setNotchesVisible(True)
        self.dial.setMaximumWidth(50)
        self.dial.setMaximumHeight(50)
        self.connect(self.dial, SIGNAL("valueChanged(int)"),self.dialAngleChanged)
        #spinbox
        self.spinbox = QSpinBox()
        alayout.addWidget(self.spinbox)
        self.spinbox.setRange(0,360)
        self.spinbox.setWrapping(True)
        self.spinbox.setSingleStep(5)
        self.connect(self.spinbox, SIGNAL("valueChanged(int)"),self.spinAngleChanged)
        self.callback = callback if callback != None else self.dummyCallback
        self.dial.setValue(270)

    def dummyCallback(self, value=None):
        pass

    def dialAngleChanged(self, value):
        """Dial signal processing"""
        if value == -1: return
        self.updateCounter += 1
        if self.updateCounter == 1:
            self.angle = value-270
            if self.angle < 0: self.angle = self.angle+360
            self.spinbox.setValue((360-self.angle)%360)
        self.updateCounter -= 1
        self.callback(value)

    def spinAngleChanged(self, value):
        """SpinBox signal processing"""
        if value == -1: return
        self.updateCounter += 1
        if self.updateCounter == 1:
            self.angle = 360-value
            self.dial.setValue((360-value+270)%360)
        self.updateCounter -= 1
        self.callback(value)

    def getAngle(self):
        return self.spinbox.value()

    def setAngle(self, value):
        self.spinbox.setValue(value)
        self.callback(value)

class RadioControl(QVBoxLayout):
    """Control with radio buttons in one or more columns"""
    def __init__(self, label, buttons, data=None, colrows=1, byColumns=False, topTitle=True, callback=None, parent=None):
        super(RadioControl, self).__init__(parent)
        #label with text
        self.label = QLabel(label)
        if topTitle:
            self.labelLayout = QHBoxLayout()
            self.addLayout(self.labelLayout)
            self.labelLayout.addWidget(self.label)
        #radio buttons layout
        self.layout = QGridLayout()
        self.addLayout(self.layout)
        self.group = QButtonGroup()
        column = 0
        row = 0
        if not topTitle:
            self.layout.addWidget(self.label,row,column)
            if byColumns:
                column = 1
                row = 0
            else:
                column = 0
                row = 1
        self.buttons = {}
        self.buttonList = []
        if data == None:
            data = range(len(buttons))
        for text,number in zip(buttons, data):
            button = QRadioButton(text)
            self.layout.addWidget(button, row, column)
            self.group.addButton(button, number)
            self.buttons[number] = button
            self.buttonList.append(button)
            if byColumns:
                row += 1
                if row >= colrows:
                    row = 0
                    column += 1
            else:
                column +=1
                if column >= colrows:
                    column = 0
                    row +=1
        if callback == None: callback = self.dummyCallback
        self.connect(self.group, SIGNAL("buttonClicked(int)"), callback)
        self.buttons[data[0]].click()

    def dummyCallback(self, value):
        pass

    def getButtonId(self):
        return self.group.checkedId()

    def setButtonId(self, buttonId):
        self.buttons[buttonId].click()

    def getButtonText(self):
        return self.group.checkedButton().text()

    def setButtonsTextId(self, buttons, data=None):
        if len(buttons) != len(self.buttonList):
            raise Exception("Radio group has "+str(len(self.buttonList))+" buttons, but "+str(len(buttons))+" are/is provided")
        if data == None:
            data = range(len(buttons))
        buttons = [str(b) for b in buttons]
        for btnText, btnData, button in zip(buttons, data, self.buttonList):
            button.setText(btnText)
            self.group.setId(button, btnData)

class ComboControl(QHBoxLayout):
    """Control with label, ComboBox and +/- buttons"""
    def __init__(self, label, values=None, data=None, callback=None, parent=None, button=False):
        super(ComboControl, self).__init__(parent)

        self.setSpacing(0)
        if button:
            self.button = QPushButton(label)
            self.addWidget(self.button, 0, Qt.AlignRight)
        else:
            self.label = QLabel(label)
            self.addWidget(self.label, 0, Qt.AlignRight)
        self.addSpacing(5)
        self.combo = QComboBox2()
        self.addWidget(self.combo, 1)
        self.combo.setEditable(False)
        self.buttons = QVBoxLayout()
        self.buttons.setSpacing(0)
        self.addLayout(self.buttons)
        self.buttonP = QPushButton(u'\u25be')
        self.buttonM = QPushButton(u'\u25b4')
        self.buttons.addWidget(self.buttonM)
        self.buttons.addWidget(self.buttonP)
        size = 30
        self.buttonP.setMaximumWidth(size)
        self.buttonP.setMaximumHeight(size/2)
        self.buttonM.setMaximumWidth(size)
        self.buttonM.setMaximumHeight(size/2)
        self.connect(self.buttonP, SIGNAL("clicked()"), self.nextValue)
        self.connect(self.buttonM, SIGNAL("clicked()"), self.previousValue)

        if callback == None: callback = self.dummyCallback
        if button:
            self.connect(self.button, SIGNAL("clicked()"), callback)
        else:
            self.connect(self.combo, SIGNAL("currentIndexChanged(int)"), callback)

    def dummyCallback(self, value):
        pass

    def setValues(self, values, data=None):
        index = self.combo.currentIndex()
        self.combo.clear()
        #if there is no data specified, indexes will be used
        if data == None:
            data = range(len(values))
        if type(values[0]) != type(""):
            values = [str(v) for v in values]
        for val,dat in zip(values,data):
            self.combo.addItem(val,dat)
        if (index < self.combo.count) and (index > 0):
            self.combo.setCurrentIndex(index)
        else:
            self.combo.setCurrentIndex(0)

    def getText(self):
        return self.combo.currentText()

    def nextValue(self):
        index = self.combo.currentIndex()
        if index < self.combo.count()-1:
            self.combo.setCurrentIndex(index+1)

    def previousValue(self):
        index = self.combo.currentIndex()
        if index > 0:
            self.combo.setCurrentIndex(index-1)

    def valuesGenerator(self):
        counter = 0
        while True:
            if counter < self.combo.count():
                yield self.combo.itemData(counter).toInt()[0]
            else: return
            counter += 1

class CheckListControl(QVBoxLayout):
    """Control with label, ListWidget with checkbox items and check/uncheck/invert buttons"""
    def __init__(self, label, values=None, data=None, callback=None, parent=None):
        super(CheckListControl, self).__init__(parent)
        self.callback = callback

        self.label = QLabel(label)
        self.addWidget(self.label)
        self.list = QListWidget()
        self.addWidget(self.list)
        #self.list.setSizePolicy(QSizePolicy(QSizePolicy.Fixed,QSizePolicy.MinimumExpanding))
        self.list.setSelectionMode(QAbstractItemView.ExtendedSelection)
#        self.list.setMinimumWidth(120)
        #self.list.setMaximumWidth(130)
        self.connect(self.list, SIGNAL("itemClicked(QListWidgetItem *)"), self.listItemChanged)
        self.buttonCheck = QPushButton("Check Selected")
        self.addWidget(self.buttonCheck)
        self.connect(self.buttonCheck, SIGNAL("clicked()"), self.checkSelected)
        self.buttonUncheck = QPushButton("Uncheck Selected")
        self.addWidget(self.buttonUncheck)
        self.connect(self.buttonUncheck, SIGNAL("clicked()"), self.uncheckSelected)
        self.buttonInvert = QPushButton("Invert Selected")
        self.addWidget(self.buttonInvert)
        self.connect(self.buttonInvert, SIGNAL("clicked()"), self.invertSelected)
        self.setValues(values, data)
        self.applyChanges = True

    def listItemChanged(self):
        if self.applyChanges and self.callback:
            currentStates = self.getCheckStates()
            if self.lastStates != currentStates:
                self.lastStates = currentStates
                self.callback()

    def addItem(self, name, checked=True):
        item = QListWidgetItem(name)
        item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
        if checked:
            item.setCheckState(Qt.Checked)
        else:
            item.setCheckState(Qt.Unchecked)
        self.list.addItem(item)

    def setValues(self, values, data):
        if values:
            self.list.clear()
            #if there is no data specified, indexes will be used
            if data == None:
                data = [True]*len(values)
            for value,checked in zip(values,data):
                self.addItem(value, checked)
        self.lastStates = self.getCheckStates()

    def getCheckStates(self):
        count = self.list.count()
        data = [True]*count
        for i in range(count):
            data[i] = self.list.item(i).checkState() == Qt.Checked
        return data

    def itemCheck(self, i, checked=True):
        if checked:
            self.list.item(i).setCheckState(Qt.Checked)
        else:
            self.list.item(i).setCheckState(Qt.Unchecked)

    def itemState(self, i):
        return self.list.item(i).checkState() == Qt.Checked

    def checkSelected(self):
        self.applyChanges = False
        for i in self.list.selectedItems(): i.setCheckState(Qt.Checked)
        self.applyChanges = True
        self.listItemChanged()

    def uncheckSelected(self):
        self.applyChanges = False
        for i in self.list.selectedItems(): i.setCheckState(Qt.Unchecked)
        self.applyChanges = True
        self.listItemChanged()

    def invertSelected(self):
        self.applyChanges = False
        for i in self.list.selectedItems():
            if i.checkState() == Qt.Checked:
                i.setCheckState(Qt.Unchecked)
            else:
                i.setCheckState(Qt.Checked)
        self.applyChanges = True
        self.listItemChanged()

