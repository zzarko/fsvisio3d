#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTab import *
from FileData import *
from FSVisioGraph import *
from math import sqrt

class Graph2D3(FSVisioTab):
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(Graph2D3, self).__init__(title, description, filePrefix, parent)
        self.varNames = ["Min critical force of mode","Min natural frequency of mode",
                         "Min normalized critical stress of mode","Min normalized natural frequency of mode",
                         "Min approximation critical stress of mode","Min natural frequency approximation",
                         "Critical stress relative error of mode","Natural frequency relative error of mode",
                         "Critical stress across all modes", "Natural frequency across all modes",
                         "Approximation critical stress across all modes", "Approximation natural frequency across all modes",
                         "Critical stress relative error across all modes","Natural frequency relative error across all modes",
                         "Time of retardation","Effective stress across all modes", "Damage variable across all modes"]
        self.varUnits = ["MPa","rad/s","MPa","rad/s","MPa","rad/s","%","%","MPa","rad/s","MPa","rad/s","%","%","s","MPa",""]
#        self.varNames = Vars.varForcesList + Vars.varCoordsList + Vars.varUserList + Vars.varSpecialList

        self.graph = Graph(self.setStatusBarXY)
        self.contentLayout.addWidget(self.graph)

        self.offset1range = None
        self.offset2range = None

        self.markers = []

        self.addLabelComboBox("file1", "File", sync=True, callback = self.file1Changed)
        self.addLabelComboBox("var1", "Variable", self.varNames, sync=True)
        self.addLabelSlider("nterm1", "NTerm", sync=True, pageStep = 1, singleStep = 1)
        self.addLabelDoubleSpinBox("scale1","Scaling", 0.1, 100, 0.01, 2, callback = self.scale1Changed)
        self.addLabelDoubleSpinBox("offset1","Offset", -99999, 99999, callback = self.offset1Changed)
        self.addButton("reset1","Reset offset/scale", callback = self.resetOS1)
        self.addSeparator()
#        varNames = [Vars.varRichName[i] for i in self.varNames]
#        self.addLabelComboBox("varX", "Variable X", varNames)
#        self.addLabelComboBox("varY", "Variable Y", varNames)
        self.addLabelEdit("lengths","Lengths")
        self.lengthsLayout.edit.returnPressed.connect(self.update)


#        secondgrid = QGridLayout()
#        self.defaultParent.addLayout(secondgrid)
#        secondgrid.addLayout(self.addCheckBox("secondg","Show second graph",parent=False),0,0)
#        secondgrid.addLayout(self.addCheckBox("twiny","On separate axis",parent=False),0,1)
#        self.addLabelComboBox("file2", "File", sync=True, callback = self.file2Changed)
#        self.addLabelComboBox("var2", "Variable", self.varNames, sync=True)
#        self.addLabelSlider("nterm2", "NTerm", sync=True, pageStep = 1, singleStep = 1)
#        self.addLabelDoubleSpinBox("scale2","Scaling", 0.1, 100, 0.01, 2, callback = self.scale2Changed)
#        self.addLabelDoubleSpinBox("offset2","Offset", -99999, 99999, callback = self.offset2Changed)
#        self.addButton("reset2","Reset offset/scale", callback = self.resetOS2)

        self.addSeparator()
        markgrid = QGridLayout()
        self.defaultParent.addLayout(markgrid)
        self.marknames = ["O","*",u"⎕",u"◇",u"△"]
        self.markdata  = ["o","*","s","D","^"]
        markgrid.addLayout(self.addLabel("markerslab","Markers",parent=False),0,0)
        markgrid.addLayout(self.addCheckBox("marksecond","Mark for second graph",parent=False),0,1,1,2)
        markgrid.addLayout(self.addLabelEdit("markname","Name",parent=False),1,0,1,2)
        markgrid.addLayout(self.addLabelDoubleSpinBox("markat","at",0,100,0.5,callback=self.dummyCallback,parent=False),1,2)
        markgrid.addLayout(self.addLabelComboBox("marksym","",self.marknames,callback=self.dummyCallback,parent=False),2,0)
        self.marksymLayout.combo.setFixedWidth(50)
        markgrid.addLayout(self.addButton("addmark","Add marker", callback = self.addMarker,parent=False),2,1)
        markgrid.addLayout(self.addButton("clearmark","Clear markers", callback = self.clearMarkers,parent=False),2,2)
        markgrid.addLayout(self.addLabelEdit("suptitle","Suptitle",parent=False),3,0,1,3)

        self.addSeparator()
        self.addGraphControls(callback=self.clearMarkers)
        #zasivljenje:
        self.graphSmoothLayout.checkbox.setEnabled(False)
        self.graphColorLinLagVkLayout.checkbox.setVisible(False)
        self.graphSmoothLayout.checkbox.setEnabled(False)
#        self.graphMarkerLayout.setEnabled(False)

        self.scale1 = 1
#        self.scale2 = 1

        self.addButton("export","Export graph",self.exportGraph)
        self.syncTab = True
        self.init()
        self.graphSymbolSize = 12
        self.graphNumberSize = 14
        self.graphTitleSize = 18

    def addMarker(self):
#        if self.marksecond and self.secondg:
#            var = self.var2
#            pfile = fdata.files[self.file2]
#        else:
        var = self.var1
        pfile = fdata.files[self.file1]

        nterm = self.nterm1 - 1
        try:
            index = pfile.length.index(self.markat)
        except:
            return
        if var < 8:
            y = pfile.data[index][nterm].selfvalue[var]
        else:
            y = pfile.composite[var-8][index]
        if abs(y) >= 0.01:
            name = "%0.1f mm: %0.2f %s" % (self.markat,y,self.varUnits[var])
        else:
            name = "%0.1f mm: %0.2E %s" % (self.markat,y,self.varUnits[var])
        if self.markname != "": name = self.markname + ": " + name
        self.markers.append([self.markat, y, name, self.markdata[self.marksym], self.secondg])
        self.update()

    def init(self):
        self.file1SetValues([i.fileName for i in fdata.files])
#        self.file2SetValues([i.fileName for i in fdata.files])

    def file1Changed(self, value):
        if value == -1: return
        self.nterm1SetMinMax(1,fdata.files[self.file1].nterm)
        pfile = fdata.files[self.file1]
        self.markatSetMinMaxStepDec(pfile.length[0],pfile.length[-1])

#    def file2Changed(self, value):
#        if value == -1: return
#        self.nterm2SetMinMax(1,fdata.files[self.file2].nterm)

    def offset1Changed(self, value):
        if value == -1: return
        if (value != 0):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.scale1 == 1.0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

#    def offset2Changed(self, value):
#        if (value == -1) or (self.graph.canvas.axes2 == None): return
#        if (value != 0):
#            if self.offset2range == None:
#                self.offset2range = self.graph.canvas.axes2.get_ylim()
#        elif self.scale2 == 1.0:
#            self.offset2range = None
#        #print "OFFSET2",self.offset2,self.offset2range
#        self.update()

    def scale1Changed(self, value):
        if value == -1: return
        if (value != 1):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.offset1 == 0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

#    def scale2Changed(self, value):
#        if (value == -1) or (self.graph.canvas.axes2 == None): return
#        if (value != 1):
#            if self.offset2range == None:
#                self.offset2range = self.graph.canvas.axes2.get_ylim()
#        elif self.offset2 == 0:
#            self.offset2range = None
#        #print "OFFSET2",self.offset2,self.offset2range
#        self.update()

    def resetOS1(self):
        self.offset1 = 0.0
        self.scale1 = 1.0

#    def resetOS2(self):
#        self.offset2 = 0.0
#        self.scale2 = 1.0

    def exportGraph(self):
        title = self.filePrefix + "_" + self.file1Text + "_" + self.var1Text
        if self.var1 < 8: title += " " + str(self.nterm1)
        if self.secondg:
            title += "_" + self.var2Text
            if self.var2 < 8: title += " " + str(self.nterm2)
        self.graph.export("./export/",title)

    def clearMarkers(self):
        markers2 = [x for x in self.markers if x[0] != self.markat]
        if len(markers2) == len(self.markers):
            self.markers = []
        else:
            self.markers = markers2
        self.plot()

    def plot(self):
        self.setGraphAttributes(self.graph)
        self.graph.resetGraph()
        self.graph.titleX = u"δ [MPa]"
        self.graph.titleY = u"ω eq [rad/s]"
#        self.graph.titleY = self.varNames[self.var1]   # SLIKA 1 i 2

#        symbols = self.secondg
        symbols = True
        pfile = fdata.files[self.file1]

        # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
        pfile1 = fdata.files[0]
        pfile2 = fdata.files[1]

        lens=[]
        for l in str(self.lengths).split():
#            try:
            ll = float(l)
            try:
                li = pfile.length.index(ll)
                lens.append(li)
            except:
                pass
#            except:
#                pass
        if len(lens) == 0: return
        print "Lengths:",lens

        points = 200
        nterm = self.nterm1 - 1
        var = self.var1
        x = [0.0]*points
        y = [0.0]*points

        showInLegend = True

#        sigmaEMax = pfile.composite[0][lens[0]]
#        for l in lens:
#            if pfile.composite[0][l] > sigmaEMax: sigmaEMax = pfile.composite[0][l]

        for l in lens:
            w = pfile1.composite[1][l]  # freq elasticno
            wd = pfile2.composite[1][l] # freq visko
            s = pfile2.composite[0][l]  # napon visko
            D = 1-(wd/w)**2             # damage

            fi = D/(1-D)                # creep coefficient
            sigmaE = pfile.composite[0][l] # critical stress - elastic
            KE = fi/sigmaE
            omegaE = pfile.composite[1][l] # natural frequency - elastic

            for i in range(points):
                sigma = i*sigmaE*1.0/points
                fiSigma = sigma*KE
                omegaEQ = omegaE*sqrt(1/(1+fiSigma))
                x[i] = sigma
                y[i] = omegaEQ

#            name = "Length " + str(pfile.length[l]) + "mm"
            name = str(pfile.length[l]) + "mm"
#            if name[-4:] == "mode": name += " %s" % (nterm+1)
#            if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
            self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)

#        a = pfile.length[length]
#        dam_kk = 48*24099000000*0.000003718
#        dam_mm = 0.493*10000*0.002815
#        dam_k = dam_kk/a*1000/a*1000/a*1000
#        dam_m = dam_mm*a/1000
#        dam_wd = pfile.data[length][nterm].selfvalue[3]
##        dam_wd = pfile.composite[1][length]
#        dam_rd = -dam_k+dam_wd*dam_wd*dam_m
#        dam_rdk = -dam_rd/dam_k

#        showInLegend = self.secondg and (not self.twiny)

#        if var < 8:
#            for i in range(pfile.lengths):
#                x[i] = pfile.length[i]
#                y[i] = pfile.data[i][nterm].selfvalue[var]
#        elif var < 14:
#            for i in range(pfile.lengths):
#                x[i] = pfile.length[i]
#                y[i] = pfile.composite[var-8][i]
#        elif var == 14:
#            for i in range(pfile.lengths):
#                x[i] = pfile.length[i]
#                y[i] = 1/pfile.composite[1][i]
##                x[i] = pfile.length[i]
##                k = kk/x[i]*1000
##                m = mm*x[i]/1000
##                wd = pfile.composite[1][i]
##                rd = -k * wd*wd*m
##                d = -rd/k
##                y[i] = d
#        # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
#        elif var == 15:
#            for i in range(pfile.lengths):
#                x[i] = pfile.length[i]
#                w = pfile1.composite[1][i]  # freq elasticno
#                wd = pfile2.composite[1][i] # freq visko
#                s = pfile2.composite[0][i]  # napon visko
#                y[i] = s * (w/wd)**2
#        elif var == 16:
#            for i in range(pfile.lengths):
#                x[i] = pfile.length[i]
#                w = pfile1.composite[1][i]  # freq elasticno
#                wd = pfile2.composite[1][i] # freq visko
#                s = pfile2.composite[0][i]  # napon visko
#                y[i] = 1-(wd/w)**2

#        #procenti
#        if var in [6,7,12,13]:
#            self.graph.percent1 = True
#        else:
#            self.graph.percent1 = False

#        name = self.varNames[var]
#        if name[-4:] == "mode": name += " %s" % (nterm+1)
#        if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
##        name = 'Elastic'   # SLIKA 1 i 2
#        self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)

#        if self.secondg:
#            pfile = fdata.files[self.file2]
#            nterm = self.nterm2 - 1
#            var = self.var2
#            x = [0.0]*pfile.lengths
#            y = [0.0]*pfile.lengths
#            if var < 8:
#                for i in range(pfile.lengths):
#                    x[i] = pfile.length[i]
#                    y[i] = pfile.data[i][nterm].selfvalue[var]
#            elif var < 14:
#                for i in range(pfile.lengths):
#                    x[i] = pfile.length[i]
#                    y[i] = pfile.composite[var-8][i]
#            elif var == 14:
#                for i in range(pfile.lengths):
#                    x[i] = pfile.length[i]
#                    y[i] = 1/pfile.composite[1][i]
#            # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
#            elif var == 15:
#                for i in range(pfile.lengths):
#                    x[i] = pfile.length[i]
#                    w = pfile1.composite[1][i]  # freq elasticno
#                    wd = pfile2.composite[1][i] # freq visko
#                    s = pfile2.composite[0][i]  # napon visko
#                    y[i] = s*w*w/wd/wd
#            elif var == 16:
#                for i in range(pfile.lengths):
#                    x[i] = pfile.length[i]
#                    w = pfile1.composite[1][i]  # freq elasticno
#                    wd = pfile2.composite[1][i] # freq visko
#                    s = pfile2.composite[0][i]  # napon visko
#                    y[i] = 1-wd*wd/w/w


#            name = self.varNames[var]
#            if name[-4:] == "mode": name += " %s" % (nterm+1)
#            if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
##            name = 'Viscoelastic'  # SLIKA 1 i 2
#            #procenti
#            if var in [6,7,12,13]:
#                self.graph.percent2 = True
#            else:
#                self.graph.percent2 = False
#            self.graph.addCurve(x,y,name,symbol=symbols,secondAxe=self.twiny,showInLegend=showInLegend)
        if ((self.offset1 != 0.0) or (self.scale1 != 1.0)) and (self.offset1range != None):
            #print "GOFFSET1",self.offset1,self.offset1range
            self.graph.canvas.axes.autoscale_view(True,True,False)
            top = (self.offset1range[1] - self.offset1range[0])*self.scale1 + self.offset1range[0] + self.offset1
            self.graph.canvas.axes.set_ylim(self.offset1range[0] + self.offset1, top)
        else:
            self.graph.canvas.axes.autoscale_view(True,True,True)
#        if self.graph.canvas.axes2:
#            if ((self.offset2 != 0.0) or (self.scale2 != 1.0)) and (self.offset2range != None):
#                #print "GOFFSET2",self.offset2,self.offset2range
#                self.graph.canvas.axes2.autoscale_view(True,True,False)
#                top = (self.offset2range[1] - self.offset2range[0])*self.scale2 + self.offset2range[0] + self.offset2
#                self.graph.canvas.axes2.set_ylim(self.offset2range[0] + self.offset2, top)
#            else:
#                self.graph.canvas.axes2.autoscale_view(True,True,True)

        #dodavanje markera
        for m in self.markers:
            col = 0
            color=(col*0.8+0.1,0.9-col*0.8,0.1,1)
            self.graph.addMarker(m[0],m[1],color,m[2],m[3],m[4])

        #dodavanje suptitle-a
        self.graph.canvas.figure.suptitle(self.suptitle,fontsize=self.graphTitleSize)

        self.graph.plotGraph()
        return

