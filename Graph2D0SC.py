#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTab import *
from FileData import *
from FSVisioGraph import *

import numpy as np
from math import sqrt, pi
import tables as tb
import yaml
import re

def frange(start, stop, step):
     i = start
     while i < stop:
         yield i
         i += step

class Graph2D0SC(FSVisioTab):
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(Graph2D0SC, self).__init__(title, description, filePrefix, parent)
#        self.varNames = ["Min critical force of mode","Min natural frequency of mode",
#                         "Min normalized critical stress of mode","Min normalized natural frequency of mode",
#                         "Min approximation critical stress of mode","Min natural frequency approximation",
#                         "Critical stress relative error of mode","Natural frequency relative error of mode",
#                         "Critical stress across all modes", "Natural frequency across all modes",
#                         "Approximation critical stress across all modes", "Approximation natural frequency across all modes",
#                         "Critical stress relative error across all modes","Natural frequency relative error across all modes",
#                         "Time of retardation","Effective stress across all modes", "Damage variable across all modes",
#                         "Creep coeficient"]
#        self.varUnits = ["MPa","rad/s","MPa","rad/s","MPa","rad/s","%","%","MPa","rad/s","MPa","rad/s","%","%","s","MPa","",""]
#        self.varNames = Vars.varForcesList + Vars.varCoordsList + Vars.varUserList + Vars.varSpecialList

        self.graph = Graph(self.setStatusBarXY)
        self.contentLayout.addWidget(self.graph)
        self.markers = []

        self.offset1range = None
        self.offset2range = None

        # load data
        cfgdir = "./barbero_creep-iterations/"
        self.filelist = glob.glob( os.path.join(cfgdir, '*.hdf5') )
        self.filelist.sort()
        self.files = []
        for f in self.filelist:
            fname = os.path.basename(f)
            print "Opening file:",fname
            self.files.append(tb.open_file(f))
        self.shortfile = [ re.search(r'(barbero-local-to-global_iteration-[0-9][0-9]-)(.*)(\.hdf5)', os.path.basename(name)).group(2) for name in self.filelist ]
        #self.shortfile = [ re.search(r'(barbero-local-to-global_)(.*)(\.hdf5)', os.path.basename(name)).group(2) for name in self.filelist ]
        self.desc = yaml.load(self.files[0].root.parameter_sweep.modal_composites.attrs.column_descriptions_as_yaml)
        self.units = yaml.load(self.files[0].root.parameter_sweep.modal_composites.attrs.column_units_as_yaml)
        self.hdfvars = self.units.keys()
#        for d in self.desc:
#            print d,':',self.desc[d]

#        a : strip length
#        sigma_cr_approx : critical buckling stress approximated from natural frequency
#        sigma_cr : critical buckling stress
#        t_b : base strip thickness
#        omega : natural frequency
#        omega_approx : natural frequency approximated from critical buckling stress
#        sigma_cr_rel_err : critical buckling stress relative approximation error
#        omega_rel_err : natural frequency relative approximation error
#        m_dominant : dominant mode, modal composite via sigma_cr
        self.thickvalues = [ row['t_b'] for row in self.files[0].root.parameter_sweep.modal_composites.where('(a>149.9)&(a<150.1)') ]
        self.lenvalues = [ row['a'] for row in self.files[0].root.parameter_sweep.modal_composites.where('(t_b>1.99)&(t_b<2.01)') ]
        self.varnames = ['sigma_cr','sigma_cr_approx','omega','omega_approx','m_dominant']
        self.addVar('damage','Damage','')
#    >>> passvalues = [ row['col3'] for row in
#    ...                table.where('(col1 > 0) & (col2 <= 20)', step=5)
#    ...                if your_function(row['col2']) ]
#    >>> print("Values that pass the cuts:", passvalues)

        self.addCheckBox('autoupdate','Auto update graph',True)
        self.addCheckBox('showlegend','Show legend',True)
        self.addLabelEdit("suptitle","Suptitle")
        self.addRadioControl("whichone", "Graph for", self.varnames, colrows=2, byColumns=False)
        self.addLabelSlider("thickness", "Thickness", sync=True, pageStep = 10, singleStep = 1, callback = self.thicknessChanged)
        self.addLabelSlider("length", "Length", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthChanged)
        self.addLabelDoubleSpinBox("sigma","Sigma", 0.01, 10000, 0.01, 2)

#        self.addLabelSlider("lengthn1", "Length 1", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthn1Changed)
#        self.addLabelSlider("lengthn2", "Length 2", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthn2Changed)
#        self.addLabelDoubleSpinBox("phi","Phi", 0.1, 100, 0.01, 2)
#        self.addLabelDoubleSpinBox("OmegaQ","omegaq", 0.1, 10000, 0.01, 2)
#        self.addLabelDoubleSpinBox("wmin","Omega min", 0.1, 100, 0.01, 2)
#        self.addLabelDoubleSpinBox("wmax","Omega max", 0.1, 100, 0.01, 2)

        self.addLabelDoubleSpinBox("scale1","Scaling", 0.1, 100, 0.01, 2, callback = self.scale1Changed)
        self.addLabelDoubleSpinBox("offset1","Offset", -99999, 99999, callback = self.offset1Changed)
        self.addButton("reset1","Reset offset/scale", callback = self.resetOS1)

        self.addSeparator()
        self.addLabelCheckList("fileselect", "Select iterations", self.shortfile)
        self.addSeparator()
        self.addLabelEdit("exportname","Export name", callback=self.dummyCallback)
        self.addButton("export","Export graph",self.exportGraph)

        self.nameControlsTab('Graph 1')

        self.setDefaultParent(self.addControlsTab('Graph 2'))
        self.addCheckBox('graph2','Show graph 2',False)
        self.addCheckBox("twiny","On separate axis",True)
        self.addRadioControl("whichone2", "Graph for", self.varnames, colrows=2, byColumns=False)
        self.addLabelSlider("thickness2", "Thickness", sync=True, pageStep = 10, singleStep = 1, callback = self.thickness2Changed)

        self.addLabelDoubleSpinBox("scale2","Scaling", 0.1, 100, 0.01, 2, callback = self.scale2Changed)
        self.addLabelDoubleSpinBox("offset2","Offset", -99999, 99999, callback = self.offset2Changed)
        self.addButton("reset2","Reset offset/scale", callback = self.resetOS2)

#        self.addLabelSlider("lengthn1", "Length 1", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthn1Changed)
#        self.addLabelSlider("lengthn2", "Length 2", sync=True, pageStep = 10, singleStep = 1, callback = self.lengthn2Changed)
#        self.addLabelDoubleSpinBox("phi","Phi", 0.1, 100, 0.01, 2)
#        self.addLabelDoubleSpinBox("OmegaQ","omegaq", 0.1, 10000, 0.01, 2)
#        self.addLabelDoubleSpinBox("wmin","Omega min", 0.1, 100, 0.01, 2)
#        self.addLabelDoubleSpinBox("wmax","Omega max", 0.1, 100, 0.01, 2)

        self.addSeparator()
        self.addLabelCheckList("fileselect2", "Select iterations", self.shortfile)

        self.setDefaultParent(self.addControlsTab('GraphControls'))
        self.addGraphControls(callback=self.clearMarkers)
        #zasivljenje:
        self.graphSmoothLayout.checkbox.setEnabled(False)
        self.graphColorLinLagVkLayout.checkbox.setVisible(False)
        self.graphSmoothLayout.checkbox.setEnabled(False)
#        self.graphMarkerLayout.setEnabled(False)

        self.scale1 = 1
        self.scale2 = 1

        self.syncTab = False
        self.graphSymbolSize = 10
        self.graphNumberSize = 20
        self.graphTitleSize = 20
        self.graphPenSize = 2
        self.graphLegendScale = 120
        self.graphWidth = 90
        self.graphColor = False


#        self.file1 = fdata.files[0]
#        self.file2 = fdata.files[1]
        self.init()

    def addVar(self, name, description, unit):
        self.varnames.append(name)
        self.desc[name] = description
        self.units[name] = unit

    def thickness2Changed(self, value):
        if value == -1: return
        if not hasattr(self, 'thickness2Layout'): return
        thickness = self.thickvalues[value]
        self.thickness2Layout.label.setText(self.thickness2Layout.labelText+": "+str(thickness)+self.units['t_b'])
        self.update()

    def thicknessChanged(self, value):
        if value == -1: return
        if not hasattr(self, 'thicknessLayout'): return
        thickness = self.thickvalues[value]
        self.thicknessLayout.label.setText(self.thicknessLayout.labelText+": "+str(thickness)+self.units['t_b'])
        self.update()

    def lengthChanged(self, value):
        if value == -1: return
        if not hasattr(self, 'lengthLayout'): return
        length = self.lenvalues[value]
        self.lengthLayout.label.setText(self.lengthLayout.labelText+": "+str(length)+self.units['a'])
        self.update()

#    def lengthn1Changed(self, value):
#        if value == -1: return
#        if not hasattr(self, 'lengthn1Layout'): return
#        length = self.file1.length[value]
#        self.lengthn1Layout.label.setText(self.lengthn1Layout.labelText+": "+str(length)+"mm")
#        self.update()

#    def lengthn2Changed(self, value):
#        if value == -1: return
#        if not hasattr(self, 'lengthn2Layout'): return
#        length = self.file1.length[value]
#        self.lengthn2Layout.label.setText(self.lengthn2Layout.labelText+": "+str(length)+"mm")
#        self.update()

    def addMarker(self):
        return
        if self.marksecond and self.secondg:
            var = self.var2
            pfile = fdata.files[self.file2]
        else:
            var = self.var1
            pfile = fdata.files[self.file1]
        nterm = self.nterm1 - 1
        try:
            index = pfile.length.index(self.markat)
        except:
            return
        if var < 8:
            y = pfile.data[index][nterm].selfvalue[var]
        else:
            y = pfile.composite[var-8][index]
        if abs(y) >= 0.01:
            name = "%0.1f mm: %0.2f %s" % (self.markat,y,self.varUnits[var])
        else:
            name = "%0.1f mm: %0.2E %s" % (self.markat,y,self.varUnits[var])
        if self.markname != "": name = self.markname + ": " + name
        self.markers.append([self.markat, y, name, self.markdata[self.marksym], self.marksecond])
        self.update()

    def offset1Changed(self, value):
        if value == -1: return
        if (value != 0):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.scale1 == 1.0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

    def offset2Changed(self, value):
        if (value == -1) or (self.graph.canvas.axes2 == None): return
        if (value != 0):
            if self.offset2range == None:
                self.offset2range = self.graph.canvas.axes2.get_ylim()
        elif self.scale2 == 1.0:
            self.offset2range = None
        #print "OFFSET2",self.offset2,self.offset2range
        self.update()

    def scale1Changed(self, value):
        if value == -1: return
        if (value != 1):
            if self.offset1range == None:
                self.offset1range = self.graph.canvas.axes.get_ylim()
        elif self.offset1 == 0:
            self.offset1range = None
        #print "OFFSET1",self.offset1,self.offset1range
        self.update()

    def scale2Changed(self, value):
        if (value == -1) or (self.graph.canvas.axes2 == None): return
        if (value != 1):
            if self.offset2range == None:
                self.offset2range = self.graph.canvas.axes2.get_ylim()
        elif self.offset2 == 0:
            self.offset2range = None
        #print "OFFSET2",self.offset2,self.offset2range
        self.update()

    def resetOS1(self):
        self.updateOff()
        self.offset1 = 0.0
        self.scale1 = 1.0
        self.updateOn()

    def resetOS2(self):
        self.updateOff()
        self.offset2 = 0.0
        self.scale2 = 1.0
        self.updateOn()

    def init(self):
        self.thicknessSetMinMax(0,len(self.thickvalues)-1)
        self.thickness2SetMinMax(0,len(self.thickvalues)-1)
        self.thickness = (np.abs(np.array(self.thickvalues) - 6.35)).argmin() #self.thickvalues.index(6.30)+1
        self.thickness2 = self.thickness
        self.thicknessChanged(self.thickness)
        self.lengthSetMinMax(0,len(self.lenvalues)-1)
        self.length = (np.abs(np.array(self.lenvalues) - 2310.0)).argmin() #self.lenvalues.index(2310.0)
        self.fileselectLayout.itemCheck(0,False)
        self.sigma = 0.5
        return

        self.lengthn1SetMinMax(0,self.file1.lengths-1)
        self.lengthn1 = 0
        self.lengthn1Changed(0)
        self.lengthn2SetMinMax(0,self.file1.lengths-1)
        self.lengthn2 = self.file1.lengths-1
        self.lengthn2Changed(0)
        self.phi = 2
        self.omegaq = 1
        w = self.file1.composite[1][self.lengthn1]  # freq elasticno
        wd = self.file2.composite[1][self.lengthn1] # freq visko
        
        wmin = min(self.file1.composite[1])
        wmax = max(self.file1.composite[1])*100
        #self.wminSetMinMaxStepDec(wmin,wmax,0.1,2)
        #self.wmaxSetMinMaxStepDec(wmin,wmax,0.1,2)
        self.wminSetMinMaxStepDec(1,wmax,0.1,2)
        self.wmaxSetMinMaxStepDec(1,wmax,0.1,2)
        self.wmin = wmin
        self.wmax = wmax

    def exportGraph(self):
        if self.exportnameLayout.edit.text() != "":
            title = self.exportnameLayout.edit.text()
        else:
            title = '%s_%s[%f]' % (self.filePrefix, self.varnames[self.whichone], self.thickvalues[self.thickness])
        if self.offset1 != 0:
            title += " (offset %d)" % self.offset1
        self.graph.export("./export/",title)

    def clearMarkers(self):
        markers2 = [x for x in self.markers if x[0] != self.markat]
        if len(markers2) == len(self.markers):
            self.markers = []
        else:
            self.markers = markers2
        self.plot()

    def plot(self):
        if not self.autoupdate: return
        self.setStatusBar("Accessing HDF5 data, please wait...")
        self.graph.titleY = u"$(ω_{D}/ω_{E})^2$"
#        if self.units[self.varnames[self.whichone]] != "":
#            self.graph.titleY = '%s [%s]' % (self.desc[self.varnames[self.whichone]],self.units[self.varnames[self.whichone]])
#        else:
#            self.graph.titleY = self.desc[self.varnames[self.whichone]]
#        if self.units[self.varnames[self.whichone2]] != "":
#            self.graph.titleY2 = '%s [%s]' % (self.desc[self.varnames[self.whichone2]],self.units[self.varnames[self.whichone2]])
#        else:
#            self.graph.titleY2 = self.desc[self.varnames[self.whichone2]]
        self.setGraphAttributes(self.graph)
        self.graph.resetGraph()
        self.graph.showLegend = self.showlegend
        self.graph.logY = False

        selected = self.fileselectGetCheckStates()
        ssc = []
        for f in range(len(self.filelist)):
            if not selected[f]: continue
            condition = '(t_b>%f)&(t_b<%f)&(a>%f)&(a<%f)' % (
                self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01,
                self.lenvalues[self.length]-0.01, self.lenvalues[self.length]+0.01 )
            sigmacr = [ row['sigma_cr'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ][0]
            ssc.append(sigmacr)

        sigma = max(ssc)
        #self.graph.titleX = u"$σ/σ_{cr},σ_{cr}=%.2f$" % sigma
        self.graph.titleX = u"D (%0.1f mm)" % self.lenvalues[self.length]
#        self.sigma = sigma
        #varname = self.varnames[self.whichone]
        xsc = []
        ysc = []
        self.markers = []
        for f in range(len(self.filelist)):
            if not selected[f]: continue
            condition = '(t_b>%f)&(t_b<%f)&(a>%f)&(a<%f)' % (
                self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01,
                self.lenvalues[self.length]-0.01, self.lenvalues[self.length]+0.01 )
            w = [ row['omega'] for row in self.files[0].root.parameter_sweep.modal_composites.where(condition) ][0]   # elastično
            wd = [ row['omega'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ][0]  # viskoelastično
            sigmacr = [ row['sigma_cr'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ][0]
#            xsc.append(sigmacr/sigma)
            xsc.append(1-wd*wd/w/w)
            ysc.append(wd*wd/w/w)
            #print "a:",self.lenvalues[self.length],"F:",f,"W:",w,"Wd:",wd
            name = self.shortfile[f]
            self.graph.addMarker(xsc[-1],ysc[-1],symbol=self.graph.getSymbol(f)[0],name=self.shortfile[f],size=self.graphSymbolSize)
#            if self.graph2: name = "%s-%s" % (varname,name)
#            if varname in self.hdfvars:
#                condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
#                #print "INDEXING:",self.files[f].root.parameter_sweep.modal_composites.will_query_use_indexing(condition)
#                y = [ row[varname] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ]
#                #print "GRAPH:",self.thickness,self.thickvalues[self.thickness]
#                if len(y) == 0:
#                    print "Something wrong with thickness:",self.thickvalues[self.thickness]
#                    return
#            elif varname == 'damage':
#                condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
#                w = np.array([ row['omega'] for row in self.files[0].root.parameter_sweep.modal_composites.where(condition) ])    # elastično
#                wd = np.array([ row['omega'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
#                y = 1-(wd/w)**2
#            else:
#                return
#            self.graph.addCurve(x,y,name,symbol=True,showInLegend=True)
#            #self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)

        line = np.polyfit(xsc, ysc, 1)
        xx = xsc[:]
        xx.extend([0,-line[1]/line[0],1])
        x = np.unique(xx)
        y = np.poly1d(line)(x)
        self.graph.addCurve(x,y,"Linear fit",symbol=True,showInLegend=True)

        if self.graph2:
            varname = self.varnames[self.whichone2]
            selected = self.fileselect2GetCheckStates()
            for f in range(len(self.filelist)):
                if not selected[f]: continue
                x = self.lenvalues
                name = self.shortfile[f]
                name = "%s-%s" % (varname,name)
                if varname in self.hdfvars:
                    condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                    #print "INDEXING:",self.files[f].root.parameter_sweep.modal_composites.will_query_use_indexing(condition)
                    y = [ row[varname] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ]
                    #print "GRAPH:",self.thickness,self.thickvalues[self.thickness]
                    if len(y) == 0:
                        print "Something wrong with thickness:",self.thickvalues[self.thickness]
                        return
                elif varname == 'damage':
                    condition = '(t_b>%f)&(t_b<%f)' % (self.thickvalues[self.thickness]-0.01, self.thickvalues[self.thickness]+0.01)
                    w = np.array([ row['omega'] for row in self.files[0].root.parameter_sweep.modal_composites.where(condition) ])    # elastično
                    wd = np.array([ row['omega'] for row in self.files[f].root.parameter_sweep.modal_composites.where(condition) ])   # viskoelastično
                    y = 1-(wd/w)**2
                else:
                    return
                self.graph.addCurve(x,y,name,symbol=True,showInLegend=True,secondAxe=self.twiny)
                #self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)
            


        #self.graph.canvas.axes.autoscale_view(True,True,True)
        if ((self.offset1 != 0.0) or (self.scale1 != 1.0)) and (self.offset1range != None):
            print "GOFFSET1",self.offset1,self.scale1,self.offset1range
            self.graph.canvas.axes.autoscale_view(True,True,False)
            top = (self.offset1range[1] - self.offset1range[0])*self.scale1 + self.offset1range[0] + self.offset1
            self.graph.canvas.axes.set_ylim(self.offset1range[0] + self.offset1, top)
        else:
            self.graph.canvas.axes.autoscale_view(True,True,True)
        if self.graph.canvas.axes2:
            if ((self.offset2 != 0.0) or (self.scale2 != 1.0)) and (self.offset2range != None):
                print "GOFFSET2",self.offset2,self.scale2,self.offset2range
                self.graph.canvas.axes2.autoscale_view(True,True,False)
                top = (self.offset2range[1] - self.offset2range[0])*self.scale2 + self.offset2range[0] + self.offset2
                self.graph.canvas.axes2.set_ylim(self.offset2range[0] + self.offset2, top)
            else:
                self.graph.canvas.axes2.autoscale_view(True,True,True)

#        #def addMarker(self, x, y, color='k', name="", symbol='o', secondg=False, size=10):
#        for i,(x,y,f) in enumerate(zip(xsc,ysc,self.shortfile)):
#            self.graph.addMarker(x,y,symbol=self.graph.getSymbol(i)[0],name=f,size=self.graphSymbolSize)

        #self.graph.canvas.axes.set_xscale('log')
        #self.graph.showLegend = True
        #if self.graph2:
        #    self.graph.canvas.axes2.set_ylabel()
        self.graph.canvas.figure.suptitle(self.suptitleLayout.edit.text(),fontsize=self.graphTitleSize)
        self.graph.plotGraph()

        self.setStatusBar("Graph shown")

        return

#        length1 = self.file1.length[self.lengthn1]
#        length2 = self.file1.length[self.lengthn2]

#        N = 400
#        x = [0.0]*N
#        y = [0.0]*N
#        fileE = fdata.files[0]  # elastic
#        fileV = fdata.files[1]  # viscoelastic

#        phi = 2 # self.phi
#        mi = phi/(2*(1+phi))
#        Eh = 24098.98
#        Gif = 1
#        k = 1
#        R = -1

#        if self.whichone == 0:  # sigma / a cr
#            if self.lengthn2 <= self.lengthn1:
#                return
#            N = self.lengthn2 - self.lengthn1 + 1
#            x = [0.0]*N
#            y = [0.0]*N
#            wq = self.omegaq
#            for i in range(N):
#                l = i + self.lengthn1
#                sigmaCR = fileV.composite[0][l]    # critical stress
#                w = fileE.composite[1][l]  # freq elasticno
#                wd = fileV.composite[1][l] # freq visko

#                phi12 = (1+phi)*(1+phi)
#                delta = wq/w
#                upper = Eh*Gif*(1+delta*delta)*phi12
#                lower = sigmaCR*(phi12+delta*delta)*2*(1-mi*mi)*pi
#                x[i] = upper/lower
#                y[i] = sigmaCR
#            name = u"$σ_{cr}$"
#            self.graph.titleX = u"$a_{cr}$"
#            self.graph.titleY = name
#            self.graph.logY = True
#            self.graph.canvas.axes.set_yscale('log')
#            self.graph.canvas.axes.get_yaxis().set_major_formatter(ticker.ScalarFormatter())
#            self.graph.canvas.axes.get_yaxis().set_minor_formatter(ticker.ScalarFormatter())
#            self.graph.canvas.axes.ticklabel_format(axis='y', style='plain', which='both')
#        if self.whichone == 1:  # sigma cr / delta
#            w = self.file1.composite[1][self.lengthn1]  # freq elasticno
#            wd = self.file2.composite[1][self.lengthn1] # freq visko
#            sigmaCR = fileV.composite[0][self.lengthn1] # critical stress
#            step = (self.wmax-self.wmin)/(N+1)
#            wq = np.linspace(self.wmin,self.wmax,N) #omegaQ
#            l0 = self.file1.length[self.lengthn1]
#            deltaE = Eh*l0/(pi*k*(1-R)*(1-R))
#            #wq = np.logspace(0.1,5,N) #omegaQ
#            for i in range(N):
#                delta = wq[i]/w
#                deltas = wq[i]/wd
#                upper = ((1+phi)*(1+phi)+delta*delta)*delta
#                lower = deltaE*(1+delta*delta)
#                x[i] = delta
#                y[i] = sigmaCR/sqrt(upper/lower)
#            name = u"$σ_{cr}$"
#            self.graph.titleX = u"$δ$"
#            self.graph.titleY = name
#            self.graph.logY = True
#            self.graph.canvas.axes.set_yscale('log')
#            self.graph.canvas.axes.get_yaxis().set_major_formatter(ticker.ScalarFormatter())
#            self.graph.canvas.axes.get_yaxis().set_minor_formatter(ticker.NullFormatter())
#            self.graph.canvas.axes.ticklabel_format(axis='y', style='plain', which='both')
#            #self.graph.canvas.axes.ticklabel_format(axis='y', style='sci', which='both', scilimits=(-2,2))
#        else:
#            pass
        self.graph.addCurve(x,y,name)
        #self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)
        self.graph.canvas.axes.autoscale_view(True,True,True)
        #self.graph.canvas.axes.set_xscale('log')
        self.graph.plotGraph()
        return

        symbols = self.secondg
        pfile = fdata.files[self.file1]

        # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
        pfile1 = fdata.files[0]
        pfile2 = fdata.files[1]

        nterm = self.nterm1 - 1
        var = self.var1
        x = [0.0]*pfile.lengths
        y = [0.0]*pfile.lengths
        print "VAR:",var

#        a = pfile.length[length]
#        dam_kk = 48*24099000000*0.000003718
#        dam_mm = 0.493*10000*0.002815
#        dam_k = dam_kk/a*1000/a*1000/a*1000
#        dam_m = dam_mm*a/1000
#        dam_wd = pfile.data[length][nterm].selfvalue[3]
##        dam_wd = pfile.composite[1][length]
#        dam_rd = -dam_k+dam_wd*dam_wd*dam_m
#        dam_rdk = -dam_rd/dam_k

        showInLegend = self.secondg and (not self.twiny)
        print "showInLegend",showInLegend,self.secondg,self.twiny

        if var < 8:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.data[i][nterm].selfvalue[var]
        elif var < 14:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = pfile.composite[var-8][i]
        elif var == 14:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                y[i] = 1/pfile.composite[1][i]
#                x[i] = pfile.length[i]
#                k = kk/x[i]*1000
#                m = mm*x[i]/1000
#                wd = pfile.composite[1][i]
#                rd = -k * wd*wd*m
#                d = -rd/k
#                y[i] = d
        # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
        elif var == 15:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                w = pfile1.composite[1][i]  # freq elasticno
                wd = pfile2.composite[1][i] # freq visko
                s = pfile2.composite[0][i]  # napon visko
                y[i] = s * (w/wd)**2
        elif var == 16:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                w = pfile1.composite[1][i]  # freq elasticno
                wd = pfile2.composite[1][i] # freq visko
                s = pfile2.composite[0][i]  # napon visko
                y[i] = 1-(wd/w)**2
        elif var == 17:
            for i in range(pfile.lengths):
                x[i] = pfile.length[i]
                w = pfile1.composite[1][i]  # freq elasticno
                wd = pfile2.composite[1][i] # freq visko
                s = pfile2.composite[0][i]  # napon visko
                D = 1-(wd/w)**2
                y[i] = D/(1-D)

        #procenti
        if var in [6,7,12,13]:
            self.graph.percent1 = True
        else:
            self.graph.percent1 = False

        name = self.varNames[var]
        if name[-4:] == "mode": name += " %s" % (nterm+1)
        if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
#        name = 'Elastic'   # SLIKA 1 i 2
        self.graph.addCurve(x,y,name,symbol=symbols,showInLegend=showInLegend)

        if self.secondg:
            pfile = fdata.files[self.file2]
            nterm = self.nterm2 - 1
            var = self.var2
            x = [0.0]*pfile.lengths
            y = [0.0]*pfile.lengths
            if var < 8:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = pfile.data[i][nterm].selfvalue[var]
            elif var < 14:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = pfile.composite[var-8][i]
            elif var == 14:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    y[i] = 1/pfile.composite[1][i]
            # budzenje za formulu D = 1 - wd^2/w^2, damage = damaged * (w/wd)^2
            elif var == 15:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    w = pfile1.composite[1][i]  # freq elasticno
                    wd = pfile2.composite[1][i] # freq visko
                    s = pfile2.composite[0][i]  # napon visko
                    y[i] = s*w*w/wd/wd
            elif var == 16:
                for i in range(pfile.lengths):
                    x[i] = pfile.length[i]
                    w = pfile1.composite[1][i]  # freq elasticno
                    wd = pfile2.composite[1][i] # freq visko
                    s = pfile2.composite[0][i]  # napon visko
                    y[i] = 1-wd*wd/w/w


            name = self.varNames[var]
            if name[-4:] == "mode": name += " %s" % (nterm+1)
            if self.varUnits[var] != "": name += " [%s]" % self.varUnits[var]
#            name = 'Viscoelastic'  # SLIKA 1 i 2
            #procenti
            if var in [6,7,12,13]:
                self.graph.percent2 = True
            else:
                self.graph.percent2 = False
            self.graph.addCurve(x,y,name,symbol=symbols,secondAxe=self.twiny,showInLegend=showInLegend)
        if ((self.offset1 != 0.0) or (self.scale1 != 1.0)) and (self.offset1range != None):
            #print "GOFFSET1",self.offset1,self.offset1range
            self.graph.canvas.axes.autoscale_view(True,True,False)
            top = (self.offset1range[1] - self.offset1range[0])*self.scale1 + self.offset1range[0] + self.offset1
            self.graph.canvas.axes.set_ylim(self.offset1range[0] + self.offset1, top)
        else:
            self.graph.canvas.axes.autoscale_view(True,True,True)
        if self.graph.canvas.axes2:
            if ((self.offset2 != 0.0) or (self.scale2 != 1.0)) and (self.offset2range != None):
                #print "GOFFSET2",self.offset2,self.offset2range
                self.graph.canvas.axes2.autoscale_view(True,True,False)
                top = (self.offset2range[1] - self.offset2range[0])*self.scale2 + self.offset2range[0] + self.offset2
                self.graph.canvas.axes2.set_ylim(self.offset2range[0] + self.offset2, top)
            else:
                self.graph.canvas.axes2.autoscale_view(True,True,True)

        #dodavanje markera
        for m in self.markers:
            col = 0
            color=(col*0.8+0.1,0.9-col*0.8,0.1,1)
            self.graph.addMarker(m[0],m[1],color,m[2],m[3],m[4])

        #dodavanje suptitle-a
        self.graph.canvas.figure.suptitle(self.suptitle,fontsize=self.graphTitleSize)

        self.graph.plotGraph()
        return

