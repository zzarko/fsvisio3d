#!/usr/bin/python
# -*- coding: UTF-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

from FSVisioTab import *
from FileData import *
from FSVisioGraph3D import *

from math import pi,sin,cos
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import operator

import numpy as np
import tables as tb

from matplotlib.colors import BoundaryNorm
import matplotlib
import matplotlib.colors as mcolors

# https://stackoverflow.com/questions/16834861/create-own-colormap-using-matplotlib-and-plot-color-scale
def make_colormap(seq):
    """Return a LinearSegmentedColormap
    seq: a sequence of floats and RGB-tuples. The floats should be increasing
    and in the interval (0,1).
    """
    seq = [(None,) * 3, 0.0] + list(seq) + [1.0, (None,) * 3]
    cdict = {'red': [], 'green': [], 'blue': []}
    for i, item in enumerate(seq):
        if isinstance(item, float):
            r1, g1, b1 = seq[i - 1]
            r2, g2, b2 = seq[i + 1]
            cdict['red'].append([item, r1, r2])
            cdict['green'].append([item, g1, g2])
            cdict['blue'].append([item, b1, b2])
    return mcolors.LinearSegmentedColormap('CustomMap', cdict)

class Graph3D2(FSVisioTab):
    def __init__(self, title, description="", filePrefix="", parent=None):
        super(Graph3D2, self).__init__(title, description, filePrefix, parent)
#        self.varNames = ["Min critical force of mode","Min natural frequency of mode",
#                         "Min normalized critical stress of mode","Min normalized natural frequency of mode",
#                         "Min approximation critical stress of mode","Min natural frequency approximation",
#                         "Critical stress relative error of mode","Natural frequency relative error of mode",
#                         "Min critical stress of all modes", "Min natural frequency of all modes"]
#        self.varUnits = ["MPa","rad/s","MPa","rad/s","MPa","rad/s","","","MPa","rad/s","MPa","rad/s"]
#        self.varNames = Vars.varForcesList + Vars.varCoordsList + Vars.varUserList + Vars.varSpecialList
#        self.varDamNames = ["u","v",'w','f']

        self.graph = Graph3D(self.setStatusBarXY)
        self.contentLayout.addWidget(self.graph)

        self.lastksiby = -1
        self.diagnames = ["Omega-Sigma","Delta-Sigma","Delta-Damage","Delta-Poisson","Omega-Damage","Delta-t","Omega-SigmaEff"]

        self.addLabelEdit("length","Length")
        self.lengthLayout.edit.returnPressed.connect(self.lengthChanged)
        self.addRadioControl("ksiby", "Show Ksi by", self.diagnames, colrows=2, byColumns=True, callback=self.ksiByChanged)

        self.addLabelEdit("xvaluemax","Omega Q Max")
        self.xvaluemaxLayout.edit.returnPressed.connect(self.update)
        self.addLabelEdit("resolution","Resolution")
        self.resolutionLayout.edit.returnPressed.connect(self.update)

        self.addSeparator()
        self.addLabelSlider("graphalpha", "Graph Alpha", 0, 100, pageStep = 10, singleStep = 1)
        self.addLabelSlider("projalpha", "Projection Alpha", 0, 100, pageStep = 10, singleStep = 1)
        self.addCheckBox("showsideproj","Show side projections",True)
        self.addCheckBox("showbottomproj","Show bottom projection",True)

#        self.addCheckBox("xlogscale","X LOG scale")
#        self.addCheckBox("ylogscale","Y LOG scale")
#        self.addCheckBox("zlogscale","Z LOG scale")

        self.addGraphControls()
        self.addButton("export","Export graph",self.exportGraph)

        self.syncTab = True
        self.init()
        self.graphSymbolSize = 12
        self.graphNumberSize = 10
        self.graphTitleSize = 14

        self.colorbar = None
        self.contour = None
        self.contour2 = None

    def init(self):
        self.lastksiby = 0
        self.length = str(fdata.files[0].length[0])
        self.resolution = "200"
        self.xvaluemax = "150"
        self.omegaqmax = "150"
        self.deltamax = "2"
        self.dammax = "0.99"
        self.poismax = "0.499"
        self.graphalpha = 90
        self.projalpha = 70

    def exportGraph(self):
        title = self.filePrefix + "_L" + self.length + "_" + self.diagnames[self.ksiby] + "_" + self.xvaluemax
        self.graph.export("./export/",title)

    def ksiByChanged(self):
        if self.lastksiby == -1: return
    def ksiByChanged(self):
        if self.lastksiby == -1: return
        if self.lastksiby == 0:
            self.omegaqmax = self.xvaluemax
        elif self.lastksiby == 1:
            self.deltamax = self.xvaluemax
        elif self.lastksiby == 2:
            self.dammax = self.xvaluemax
        elif self.lastksiby == 3:
            self.poismax = self.xvaluemax
        if self.ksiby == 0:
            self.xvaluemax = self.omegaqmax
        elif self.ksiby == 1:
            self.xvaluemax = self.deltamax
        elif self.ksiby == 2:
            self.xvaluemax = self.dammax
        elif self.ksiby == 3:
            self.xvaluemax = self.poismax
        self.lastksiby = self.ksiby
        self.update()

    def lengthChanged(self):
        if len(self.length) == 0: return
        pfile = fdata.files[0]
        try:
            l = pfile.length.index(float(self.length))
        except:
            return
        omegaE = pfile.composite[1][l] # natural frequency - elastic
        omegaE = int(omegaE/5+1)*5

        if self.ksiby == 0:
            self.xvaluemax = str(omegaE)
        self.omegaqmax = str(omegaE)
        self.update()

    def shiftedColorMap(self,cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
        '''
        Function to offset the "center" of a colormap. Useful for
        data with a negative min and positive max and you want the
        middle of the colormap's dynamic range to be at zero

        Input
        -----
          cmap : The matplotlib colormap to be altered
          start : Offset from lowest point in the colormap's range.
              Defaults to 0.0 (no lower ofset). Should be between
              0.0 and `midpoint`.
          midpoint : The new center of the colormap. Defaults to 
              0.5 (no shift). Should be between 0.0 and 1.0. In
              general, this should be  1 - vmax/(vmax + abs(vmin))
              For example if your data range from -15.0 to +5.0 and
              you want the center of the colormap at 0.0, `midpoint`
              should be set to  1 - 5/(5 + 15)) or 0.75
          stop : Offset from highets point in the colormap's range.
              Defaults to 1.0 (no upper ofset). Should be between
              `midpoint` and 1.0.
        '''
        cdict = {
            'red': [],
            'green': [],
            'blue': [],
            'alpha': []
        }

        # regular index to compute the colors
        reg_index = np.linspace(start, stop, 257)

        # shifted index to match the data
        shift_index = np.hstack([
            np.linspace(0.0, midpoint, 128, endpoint=False), 
            np.linspace(midpoint, 1.0, 129, endpoint=True)
        ])

        for ri, si in zip(reg_index, shift_index):
            r, g, b, a = cmap(ri)

            cdict['red'].append((si, r, r))
            cdict['green'].append((si, g, g))
            cdict['blue'].append((si, b, b))
            cdict['alpha'].append((si, a, a))
        newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
        return newcmap

    def clearMarkers(self):
        self.graph.markers = []
        self.plot()

    def plot(self):

        self.graph.azim = self.graph.canvas.axes.azim
        self.graph.elev = self.graph.canvas.axes.elev

        self.graph.clear()

        self.setGraphAttributes(self.graph)
        self.graph.resetGraph()
#        if self.xlogscale:
#            self.graph.canvas.axes.set_xscale('log')
#        else:
#            self.graph.canvas.axes.set_xscale('linear')
#        if self.ylogscale:
#            self.graph.canvas.axes.set_yscale('log')
#        else:
#            self.graph.canvas.axes.set_yscale('linear')
#        if self.zlogscale:
#            self.graph.canvas.axes.set_zscale('log')
#        else:
#            self.graph.canvas.axes.set_zscale('linear')

        pfile = fdata.files[0]
        pfile1 = fdata.files[0]
        pfile2 = fdata.files[1]

        if len(self.length) == 0: return
        try:
            l = pfile.length.index(float(self.length))
        except:
            return
        if len(self.xvaluemax) == 0: return
        try:
            xvaluemax = float(self.xvaluemax)
        except:
            return
        if len(self.resolution) == 0: return
        try:
            N = float(self.resolution)
        except:
            return

        w = pfile1.composite[1][l]  # freq elasticno
        wd = pfile2.composite[1][l] # freq visko
        s = pfile2.composite[0][l]  # napon visko
        D = 1-(wd/w)**2             # damage

        fi = D/(1-D)                # creep coefficient
        sigmaE = pfile.composite[0][l] # critical stress - elastic
        KE = fi/sigmaE
        omegaE = pfile.composite[1][l] # natural frequency - elastic
        TD = 1/omegaE

        if self.ksiby == 0:
            self.graph.titleX = u"$ω_Q [rad/s]$"
            self.graph.titleY = u"σ [MPa]"
            self.graph.titleZ = u"ξ"

            x = np.linspace(0.1, xvaluemax, N)     # omegaQ
            y = np.linspace(0, sigmaE, N)     # sigma
            omegaQ, sigma = np.meshgrid(x, y)
            omegaQE = omegaQ/omegaE
            omegaQE2 = omegaQE*omegaQE
            sigmaKE = sigma*KE
            ksi1 = sigmaKE*omegaQE*(1-omegaQE2*(1+sigmaKE))
            ksi2 = 2*omegaQE*np.sqrt(1+sigmaKE)*(1+omegaQE2+sigmaKE)
            ksi = ksi1/ksi2

        elif self.ksiby == 1:
            self.graph.titleX = u"$δ^*$"
            self.graph.titleY = u"σ [MPa]"
            self.graph.titleZ = u"ξ"

            x = np.linspace(0.1, xvaluemax, N)     # delta
            y = np.linspace(0, sigmaE, N)     # sigma
            delta, sigma = np.meshgrid(x, y)
            sigmaKE = sigma*KE
            omegaQ = delta*omegaE/np.sqrt(1+sigmaKE)
            omegaQE = omegaQ/omegaE
            omegaQE2 = omegaQE*omegaQE

            ksi1 = sigmaKE*omegaQE*(1-delta*delta)
            ksi2 = 2*delta*(1+omegaQE2+sigmaKE)
            ksi = ksi1/ksi2

        elif self.ksiby == 2:
            self.graph.titleX = u"$δ^*$"
            self.graph.titleY = u"D"
            self.graph.titleZ = u"ξ"

            x = np.linspace(0.1, 2, N)     # delta
            y = np.linspace(0, 0.99, N)     # damage
            delta, damage = np.meshgrid(x, y)
            omegaQ = float(self.omegaqmax)
            omegaQE = omegaQ/omegaE
            omegaQE2 = omegaQE*omegaQE
            damage1 = damage/(1-damage)

            ksi1 = damage1*omegaQE*(1-delta*delta)
            ksi2 = 2*delta*(1+omegaQE2+damage1)
            ksi = ksi1/ksi2

        elif self.ksiby == 3:
            self.graph.titleX = u"$δ^*$"
            self.graph.titleY = u"μ"
            self.graph.titleZ = u"ξ"

            x = np.linspace(0.1, 2, N)     # delta
            y = np.linspace(0, 0.499, N)     # poisson
            delta, poisson = np.meshgrid(x, y)
            omegaQ = float(self.omegaqmax)
            omegaQE = omegaQ/omegaE
            omegaQE2 = omegaQE*omegaQE
            poisson1 = 2*poisson/(1-2*poisson)

            ksi1 = poisson1*omegaQE*(1-delta*delta)
            ksi2 = 2*delta*(1+omegaQE2+poisson1)
            ksi = ksi1/ksi2

        elif self.ksiby == 4:
            self.graph.titleX = u"$ω_Q [rad/s]$"
            self.graph.titleY = u"D"
            self.graph.titleZ = u"ξ"

            x = np.linspace(0.1, float(self.omegaqmax), N)     # omegaQ
            y = np.linspace(0, 0.99, N)     # damage
            omegaQ, damage = np.meshgrid(x, y)
            omegaQE = omegaQ/omegaE
            omegaQE2 = omegaQE*omegaQE
            damage1 = damage/(1-damage)

            ksi1 = damage1*(1-omegaQE2*(1+damage1))
            ksi2 = 2*np.sqrt(1+damage1)*(1+omegaQE2+damage1)
            ksi = ksi1/ksi2

        elif self.ksiby == 5:
            self.graph.titleX = u"$δ^*$"
            self.graph.titleY = u" t [s]"
            self.graph.titleZ = u"ξ"

            x = np.linspace(0.1, 2, N)     # delta
            y = np.linspace(0, TD, N)     # poisson
            delta, t = np.meshgrid(x, y)
            omegaQ = float(self.omegaqmax)
            omegaQE = omegaQ/omegaE
            omegaQE2 = omegaQE*omegaQE
            mie = 0.5
            mig = 0.333
            poisson = mie - (mie-mig)*np.exp(-t/TD)
            poisson1 = 2*poisson/(1-2*poisson)

            ksi1 = poisson1*omegaQE*(1-delta*delta)
            ksi2 = 2*delta*(1+omegaQE2+poisson1)
            ksi = ksi1/ksi2

        if self.ksiby == 6:
            self.graph.titleX = u"$ω_Q [rad/s]$"
            self.graph.titleY = u"$σ_{Eff} [MPa]$"
            self.graph.titleZ = u"ξ"

            x = np.linspace(0.1, xvaluemax, N)     # omegaQ
            y = np.linspace(0, sigmaE, N)     # sigma
            omegaQ, sigma = np.meshgrid(x, y)
            omegaQE = omegaQ/omegaE
            omegaQE2 = omegaQE*omegaQE
            sigmaTilda = sigma/(1+fi)
            sigmaTilda = sigmaTilda/(wd*wd/w/w)
            sigmaKE = sigmaTilda*KE
            ksi1 = sigmaKE*omegaQE*(1-omegaQE2*(1+sigmaKE))
            ksi2 = 2*omegaQE*np.sqrt(1+sigmaKE)*(1+omegaQE2+sigmaKE)
            ksi = ksi1/ksi2

        vmin = np.min(ksi)
        vmax = np.max(ksi)
        midp = 1 - vmax/(vmax + abs(vmin))
        #cmap = self.shiftedColorMap(matplotlib.cm.RdBu,midpoint=midp)
        cmap = self.shiftedColorMap(matplotlib.cm.coolwarm_r,midpoint=midp)

        if self.ksiby == 0:
            self.contour = self.graph.canvas.axes.plot_surface(omegaQ,sigma,ksi,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.graphalpha/100.0)
            self.contour2 = self.graph.canvas.axes.contour(omegaQ,sigma,ksi,levels=[0],colors='k')
            if self.showbottomproj:
                self.contour3 = self.graph.canvas.axes.contourf(omegaQ,sigma,ksi,20,zdir='z',offset=vmin,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.projalpha/100.0)
                self.contour33 = self.graph.canvas.axes.contour(omegaQ,sigma,ksi,zdir='z',offset=vmin,levels=[0],colors='k',alpha=self.projalpha/100.0)
            if self.showsideproj:
                self.contour4 = self.graph.canvas.axes.contourf(omegaQ,sigma,ksi,20,zdir='x',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
                self.contour5 = self.graph.canvas.axes.contourf(omegaQ,sigma,ksi,20,zdir='y',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
        elif self.ksiby == 1:
            self.contour = self.graph.canvas.axes.plot_surface(delta,sigma,ksi,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.graphalpha/100.0)
            self.contour2 = self.graph.canvas.axes.contour(delta,sigma,ksi,levels=[0],colors='k')
            if self.showbottomproj:
                self.contour3 = self.graph.canvas.axes.contourf(delta,sigma,ksi,20,zdir='z',offset=vmin,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.projalpha/100.0)
                self.contour33 = self.graph.canvas.axes.contour(delta,sigma,ksi,zdir='z',offset=vmin,levels=[0],colors='k',alpha=self.projalpha/100.0)
            if self.showsideproj:
                self.contour4 = self.graph.canvas.axes.contourf(delta,sigma,ksi,20,zdir='x',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
                self.contour5 = self.graph.canvas.axes.contourf(delta,sigma,ksi,20,zdir='y',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
        elif self.ksiby == 2:
            self.contour = self.graph.canvas.axes.plot_surface(delta,damage,ksi,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.graphalpha/100.0)
            self.contour2 = self.graph.canvas.axes.contour(delta,damage,ksi,levels=[0],colors='k')
            if self.showbottomproj:
                self.contour3 = self.graph.canvas.axes.contourf(delta,damage,ksi,20,zdir='z',offset=vmin,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.projalpha/100.0)
                self.contour33 = self.graph.canvas.axes.contour(delta,damage,ksi,zdir='z',offset=vmin,levels=[0],colors='k',alpha=self.projalpha/100.0)
            if self.showsideproj:
                self.contour4 = self.graph.canvas.axes.contourf(delta,damage,ksi,20,zdir='x',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
                self.contour5 = self.graph.canvas.axes.contourf(delta,damage,ksi,20,zdir='y',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
        elif self.ksiby == 3:
            self.contour = self.graph.canvas.axes.plot_surface(delta,poisson,ksi,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.graphalpha/100.0)
            self.contour2 = self.graph.canvas.axes.contour(delta,poisson,ksi,levels=[0],colors='k')
            if self.showbottomproj:
                self.contour3 = self.graph.canvas.axes.contourf(delta,poisson,ksi,20,zdir='z',offset=vmin,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.projalpha/100.0)
                self.contour33 = self.graph.canvas.axes.contour(delta,poisson,ksi,zdir='z',offset=vmin,levels=[0],colors='k',alpha=self.projalpha/100.0)
            if self.showsideproj:
                self.contour4 = self.graph.canvas.axes.contourf(delta,poisson,ksi,20,zdir='x',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
                self.contour5 = self.graph.canvas.axes.contourf(delta,poisson,ksi,20,zdir='y',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
        elif self.ksiby == 4:
            self.contour = self.graph.canvas.axes.plot_surface(omegaQ,damage,ksi,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.graphalpha/100.0)
            self.contour2 = self.graph.canvas.axes.contour(omegaQ,damage,ksi,levels=[0],colors='k')
            if self.showbottomproj:
                self.contour3 = self.graph.canvas.axes.contourf(omegaQ,damage,ksi,20,zdir='z',offset=vmin,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.projalpha/100.0)
                self.contour33 = self.graph.canvas.axes.contour(omegaQ,damage,ksi,zdir='z',offset=vmin,levels=[0],colors='k',alpha=self.projalpha/100.0)
            if self.showsideproj:
                self.contour4 = self.graph.canvas.axes.contourf(omegaQ,damage,ksi,20,zdir='x',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
                self.contour5 = self.graph.canvas.axes.contourf(omegaQ,damage,ksi,20,zdir='y',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
        elif self.ksiby == 5:
            self.contour = self.graph.canvas.axes.plot_surface(delta,t,ksi,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.graphalpha/100.0)
            self.contour2 = self.graph.canvas.axes.contour(delta,t,ksi,levels=[0],colors='k')
            if self.showbottomproj:
                self.contour3 = self.graph.canvas.axes.contourf(delta,t,ksi,20,zdir='z',offset=vmin,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.projalpha/100.0)
                self.contour33 = self.graph.canvas.axes.contour(delta,t,ksi,zdir='z',offset=vmin,levels=[0],colors='k',alpha=self.projalpha/100.0)
            if self.showsideproj:
                self.contour4 = self.graph.canvas.axes.contourf(delta,t,ksi,20,zdir='x',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
                self.contour5 = self.graph.canvas.axes.contourf(delta,t,ksi,20,zdir='y',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
        elif self.ksiby == 6:
            self.contour = self.graph.canvas.axes.plot_surface(omegaQ,sigma,ksi,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.graphalpha/100.0)
            self.contour2 = self.graph.canvas.axes.contour(omegaQ,sigma,ksi,levels=[0],colors='k')
            if self.showbottomproj:
                self.contour3 = self.graph.canvas.axes.contourf(omegaQ,sigma,ksi,20,zdir='z',offset=vmin,vmin=vmin,vmax=vmax,cmap=cmap,alpha=self.projalpha/100.0)
                self.contour33 = self.graph.canvas.axes.contour(omegaQ,sigma,ksi,zdir='z',offset=vmin,levels=[0],colors='k',alpha=self.projalpha/100.0)
            if self.showsideproj:
                self.contour4 = self.graph.canvas.axes.contourf(omegaQ,sigma,ksi,20,zdir='x',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)
                self.contour5 = self.graph.canvas.axes.contourf(omegaQ,sigma,ksi,20,zdir='y',offset=0,vmin=0,vmax=0,colors='0.7',alpha=self.projalpha/100.0)

        self.colorbar = self.graph.canvas.figure.colorbar(self.contour, shrink=0.5)
        self.colorbar.set_label(u"ξ",rotation=0, fontsize=self.graph.titleFontSize)

        self.graph.canvas.axes.tick_params(which='both',axis='both',direction='out')
        self.graph.canvas.axes.grid(self.graph.showGrid)
        self.graph.canvas.axes.set_xlabel(self.graph.titleX, fontsize=self.graph.titleFontSize)
        self.graph.canvas.axes.set_ylabel(self.graph.titleY, fontsize=self.graph.titleFontSize)
#        self.graph.canvas.axes.xaxis.set_label_coords(0.5,-0.050)

        #dodavanje suptitle-a
        suptitle = "$a=" + self.length + u", ω_e=%.2f" % omegaE + u", σ_e=%.2f" % sigmaE + u", φ=%.2f$" % fi
        self.graph.canvas.figure.suptitle(suptitle,fontsize=self.graphTitleSize)

        self.graph.canvas.draw()

